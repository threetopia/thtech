<?php
/*
 * File name		: inputtools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.7
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 20 Feb 2014
 * File Description	: This file contains inputtools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class InputTools_EngineLibrary extends Systems
{
	/*
	 * @filter
	 * How to : (key,value,filter,title,source)
	 * 	- key = name of the field {array}(name="[username][password][etc]")
	 * 	- value = field value (value="[somevalue]"|option="[someoption]")
	 * 	- filter = filter on field ('unique','email','required',etc)
	 * 	- title = field title ([Username][Password])
	 * 	- source = POST|GET|FILES
	 */
	public function filter($key=NULL,$values=array(),$filter=array(),$title=array(),$method='_POST')
	{
		//var_dump($values);echo'<br><br>';
		$result = array();
		$validation = array();
		$message = array();
		$source = ($method==='_FILES' and !empty($_FILES))?$_FILES:(($method==='_POST' and !empty($_POST))?$_POST:(($method==='_GET' and !empty($_GET))?$_GET:$values));
		$check = (!empty($key))?((is_array($key))?$key:array($key)):array_keys($source);
		if(!empty($check))
		{
			foreach($check as $ck=>$cv)
			{
				$val = (isset($source[$cv]))?$source[$cv]:NULL;
				$filter = (isset($filter[$cv]))?$filter[$cv]:$filter;
				if(!empty($filter))
				{
					foreach($filter as $fk=>$fv)
					{
						$method = 'filter_'.((!is_numeric($fk))?$fk:$fv);
						$filtered = (method_exists($this,$method))?$this->{$method}(((isset($title[$cv]))?$title[$cv]:$cv),$val,((isset($values[$cv]))?$values[$cv]:$values),$filter,$fv,$source):array('val'=>$val,'validation'=>true);
						$val = $filtered['val'];
						$validation[$cv][] = $filtered['validation'];
						$message[$cv] = (!empty($filtered['message']) and empty($message[$cv]))?$filtered['message']:((isset($message[$cv]))?$message[$cv]:'');
					}
					$validation[$cv] = (!in_array(false,$validation[$cv]))?true:false;
				}
				$result[$cv] = $val;
			}
		}
		return array('result'=>$result,'validation'=>$validation,'message'=>$message);
	}
	/*
	 * Below are library function used by filter
	 * How to :
	 * filter_{libraryname}(key,val,values,filter,fv)
	 * 	- key = field key name
	 * 	- val = field current/sended value or output
	 * 	- values = field predifined values such option values in select
	 * 	- filter = applied filter on field
	 * 	- fv = filter value to be used for designed filter
	 * 	- source = object source _POST or _GET
	 * Sample :
	 * filter_range('city','jakarta',array('jakarta','surabaya','semarang','medan'),array('required','range'),array('jakarta'=>array('barat')),$_POST)
	 */
	private function filter_striptags($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		$tags = (isset($filter['striptags']))?$filter['striptags']:'';
		if(!empty($val))
		{
			if(is_array($val))
			{
				foreach($val as $key0=>$val0)
				{
					$val[$key0] = strip_tags($val0,$tags);
				}
			}
			else
			{
				$val = strip_tags($val,$tags);
			}
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_htmlentities($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(!empty($val))
		{
			if(is_array($val))
			{
				foreach($val as $key0=>$val0)
				{
					$val[$key0] = htmlentities($val0);
				}
			}
			else
			{
				$val = htmlentities($val);
			}
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_filetype($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$available_extension = array();
		$validation = true;
		$message = '';
		//echo count($val['name']);
		if(!empty($val['type']) and !empty($val['name'][0]))
		{
			foreach($val['type'] as $ktype=>$vtype)
			{
				$validation = (!in_array($vtype,$fv) and $validation==true)?false:$validation;
			}
			$message = ($validation===false)?$key.' field supposed to be '.implode(', ',$fv):NULL;
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_filemaxsize($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$available_extension = array();
		$validation = true;
		$message = '';
		$fv = str_replace(array('kb','mb','gb'),array('000','000000','000000000'),strtolower($fv));
		if(!empty($val['size']) and !empty($val['name'][0]))
		{
			foreach($val['size'] as $ksize=>$vsize)
			{
				$validation = ($vsize>$fv)?false:$validation;
			}
			$fv = str_replace(array('000000000','000000','000'),array('gb','mb','kb'),strtolower($fv));
			$message = ($validation===false)?$key.' file size supposed to be not more than '.$fv:NULL;
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_fileminsize($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$available_extension = array();
		$validation = true;
		$message = '';
		$fv = str_replace(array('kb','mb','gb'),array('000','000000','000000000'),strtolower($fv));
		if(!empty($val['size']) and !empty($val['name'][0]))
		{
			foreach($val['size'] as $ksize=>$vsize)
			{
				$validation = ($vsize<$fv)?false:$validation;
			}
			$message = ($validation===false)?$key.' file size supposed to be more than '.$fv:NULL;
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_disabled($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(is_array($fv))
		{
			if(is_array($val) and array_intersect($val,$fv))
			{
				$validation = false;
				$message = 'Some of '.$key.' field supposed to be disabled';
			}
			else if(!is_array($val) and in_array($val,$fv))
			{
				$validation = false;
				$message = 'Some of '.$key.' field supposed to be disabled';
			}
		}
		else if(!is_array($fv))
		{
			if(!is_array($val) and !empty($val) and $val!=$values)
			{
				$validation = false;
				$message = $key.' field supposed to be disabled';
			}
			else 
			{
				$val = NULL;
			}
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_required($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(($val=='' or (is_array($val) and (empty($val) or (isset($val['name'][0]) and empty($val['name'][0]))))) and !in_array('disabled',$filter))
		{
			$validation = false;
			$message = $key.' field is required';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}

	private function filter_email($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		$tempval = $val;
		$val = filter_var($val,FILTER_VALIDATE_EMAIL);
		if($val===false)
		{
			$val = $tempval;
			$validation = false;
			$message = $key.' field have an incorrect format';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_range($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		//print_r($val);print_r($values);echo '<br>'.$key.'<br><br>';
		$validation = true;
		$message = '';
		if(is_array($values))
		{
			$values = array_keys(array_filter($values));
			if(((!is_array($val) and !in_array($val,$values)) or (is_array($val) and !array_intersect($val,$values))) and $val!='')
			{
				$validation = false;
				$message = $key.' field value is out of range';
			}
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_date($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		$date = explode('/',$val);
		if(!empty($date) and $date[0]>12 or $date[0]<1 or !is_numeric($date[0]))
		{
			$validation = false;
			$message = 'There is somethi\'n wrong with a month value';
		}
		else if(!empty($date) and $date[1]>((!empty($date[0])?date('t',$date[0]):date('t'))) or $date[1]<1 or !is_numeric($date[0]))
		{
			$validation = false;
			$message = 'There is somethi\'n wrong with a day value';
		}
		else if(!empty($date) and $date[0]>12 or $date[0]<1 or !is_numeric($date[0]))
		{
			$validation = false;
			$message = 'There is somethi\'n wrong with a year value';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_max($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if($val>$fv)
		{
			$validation = false;
			$message = $key.' field must lower than '.$fv;
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_min($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if($val<$fv)
		{
			$validation = false;
			$message = $key.' field must greater than '.$fv;
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_sameas($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(isset($source[$fv]) and $val!=$source[$fv])
		{
			$validation = false;
			$message = $key.' must be same with '.$fv;
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_unique($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		$unique = (!is_array($fv))?array($fv):$fv;
		if(!empty($unique) and !empty($val) and !empty($fv))
		{
			foreach($unique as $k=>$v)
			{
				if(!empty($v) and is_numeric($k))
				{
					$Explode = explode('.',$v); 
					$CountExplode = count($Explode);
					if($CountExplode==2)
					{
						$SQLSelect[] = $Explode[1];
						$SQLFrom[] = $Explode[0];
						$SQLWhere[$Explode[1]] = $val;
					}
					else if($CountExplode==1)
					{
						$SQLWhere[] = $v;
					}
				}
				else if(!empty($v) and !is_numeric($k))
				{
					$SQLWhere[$k] = $v;
				}
			}
			$Execute = $this->_SysEngine->dbtools->Execute(array('SQL'=>array("SQLSelect"=>$SQLSelect,"SQLFrom"=>$SQLFrom,"SQLWhere"=>array("SQLBINARY"=>$SQLWhere))));
			if(!empty($Execute['result']))
			{
				$validation = false;
				$message = $key.' field must be unique';
			}
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}

	private function filter_maxlength($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(strlen($val) > $fv)
		{
			$validation = false;
			$message = $key.' field length is exceed of alowed length ('.$fv.')';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_minlength($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(strlen($val) < $fv)
		{
			$validation = false;
			$message = $key.'field length is exceed of alowed length ('.$fv.')';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_isnumeric($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(!is_array($val) and $val!='' and !is_numeric($val))
		{
			$validation = false;
			$message = $key.' field must be numeric';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
	
	private function filter_recaptcha($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$recaptcha_challenge_field = (isset($source['recaptcha_challenge_field']))?$source['recaptcha_challenge_field']:'';
		$validation = $this->_SysEngine->recaptchatools->checkanswer($recaptcha_challenge_field,$val,NULL)->is_valid;
		$message = '';
		if($validation==false)
		{
			$message = $key.' field is not correct';
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}

	private function filter_lowercase($key=NULL,$val=NULL,$values=NULL,$filter=NULL,$fv=NULL,$source=NULL)
	{
		$validation = true;
		$message = '';
		if(!empty($val))
		{
			$val = strtolower($val);
		}
		return array('val'=>$val,'validation'=>$validation,'message'=>$message);
	}
}
?>