<?php
/*
 * File name		: cookietools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: GPL
 * Create Date		: 11 Mar 2014
 * Modified Date	: 13 Mar 2014
 * File Description	: This file contains cookietools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class CookieTools_EngineLibrary extends Systems
{
	public function process($data=NULL)
	{
		#################################################################################
		$_AppData = (!empty($GLOBALS['_AppData']))?$GLOBALS['_AppData']:((!empty($data['_AppData']))?$data['_AppData']:array());unset($data['_AppData']);
		#################################################################################
		$action = (!empty($data['action']))?$data['action']:'get';unset($data['action']);
		$_CookieExpired = (!empty($data['cookieexpired']))?$data['cookieexpired']:NULL;unset($data['cookieexpired']);
		if(!empty($data))
		{
			foreach($data as $key=>$val)
			{
				$syscookie = $this->syscookie(array('_AppData'=>$_AppData,'action'=>$action,'key'=>$key,'val'=>$val,'cookieexpired'=>$_CookieExpired));
			}
		}
	}
	
	private function syscookie($data=NULL)
	{
		$_COOKIE[$data['key']][$key] = $val;
	}
}
?>