<?php
/*
 * File name		: sessiontools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 26 Mar 2014
 * File Description	: This file contains sessiontools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class SessionTools_EngineLibrary extends Systems
{
	public function __construct()
	{
		@session_start();
	}
	
	public function process($data=NULL)
	{
		#################################################################################
		$_AppData = (!empty($GLOBALS['_AppData']))?$GLOBALS['_AppData']:((!empty($data['_AppData']))?$data['_AppData']:array());unset($data['_AppData']);
		#################################################################################
		$_SysSession = array();
		$action = (!empty($data['action']))?$data['action']:'get';unset($data['action']);
		$_SessionExpired = (!empty($data['_SessionExpired']))?$data['_SessionExpired']:NULL;unset($data['_SessionExpired']);
		$_SysLogged = $_SysSession = array();
		if(!empty($data))
		{
			foreach($data as $key=>$val)
			{
				if($key === '_SysLogged')
				{
					$_SysLogged = $this->syslogged(array('_AppData'=>$_AppData,'action'=>$action,'_SysLogged'=>$val,'_SessionExpired'=>$_SessionExpired));
				}
				else
				{
					$_SysSession = array_merge_recursive($_SysSession,$this->syssession(array('_AppData'=>$_AppData,'action'=>$action,'key'=>$key,'val'=>$val,'_SessionExpired'=>$_SessionExpired)));
				}
			}
		}
		$_SysSession = (!empty($_SysLogged) and !empty($_SysSession))?array_merge_recursive(array('_SysLogged'=>$_SysLogged),$_SysSession):((!empty($_SysLogged))?$_SysLogged:$_SysSession);
		return $_SysSession;
	}

	private function sessionexpired($data=NULL)
	{
		$JSONData = json_decode($data['_SessionExpired'],true);
		$_SessionExpired = (!empty($JSONData))?$JSONData:((!empty($data['_SessionExpired']))?$data['_SessionExpired']:((isset($GLOBALS['_CurProgramConfig']['sessiontools']['sessionexpired']))?$GLOBALS['_CurProgramConfig']['sessiontools']['sessionexpired']:SESSIONEXPIRED));
		$expiredinterval = (!empty($_SessionExpired['expiredinterval']))?$_SessionExpired['expiredinterval']:$_SessionExpired;
		//$expiredtime = (!empty($_SessionExpired['expiredtime']))?$_SessionExpired['expiredtime']:date('Y-m-d H:i:s',time());
		$expiredtime = (!empty($_SessionExpired['expiredtime']))?$_SessionExpired['expiredtime']:time();
		//if(isset($_SESSION[$data['key']]) and date('Y-m-d H:i:s',time())>$expiredtime)
		if(isset($_SESSION[$data['key']]) and time()>$expiredtime)
		{
			unset($_SESSION[$data['key']]);
			$_SessionExpired = true;
			if($data['key']=='_SysLogged')
			{
				$this->_SysEngine->httptools_errorprocess(array('error'=>'419'));# 419 Authentication Timeout
			}
		}
		else if(!empty($JSONData) or $data['action']==='set')
		{
			//$expiredtime = date('Y-m-d H:i:s',strtotime($expiredinterval));
			$expiredtime = strtotime($expiredinterval);
			$_SESSION[$data['key']]['_SessionExpired'] = json_encode(array('expiredtime'=>$expiredtime,'expiredinterval'=>$expiredinterval));
			$_SessionExpired = false;
		}
		else 
		{
			$_SessionExpired = false;
		}
		return $_SessionExpired;
	}
	
	private function syssession($data=NULL)
	{
		#################################################################################
		$_AppData = $data['_AppData'];unset($data['_AppData']);
		#################################################################################
		$SessionData = array();
		if(!empty($data))
		{
			$_SysSession = (isset($_SESSION[$data['key']]))?$_SESSION[$data['key']]:$data['val'];
			$data['_SysSession'] = (!empty($data['val']))?$data['val']:array_keys($_SysSession);
			if($data['action']==='set')
			{
				if(!empty($data['_SysSession']))
				{
					foreach($data['_SysSession'] as $key=>$val)
					{
						$_SESSION[$data['key']][$key] = $val;
					}
				}
			}
			else if($data['action']==='get')
			{
				if(!empty($data['_SysSession']))
				{
					foreach($data['_SysSession'] as $key=>$val)
					{
						if(isset($_SESSION[$data['key']][$val]) and $val!='_SessionExpired')
						{
							$SessionData[$data['key']][$val] = $_SESSION[$data['key']][$val];
						}
					}
				}
			}
			else if($data['action']==='unset')
			{
				if(isset($_SESSION[$data['key']]) and !empty($data['_SysSession']))
				{
					foreach($data['_SysSession'] as $key=>$val)
					{
						if(isset($_SESSION[$data['key']][$val]))
						{
							unset($_SESSION[$data['key']][$val]);
						}
					}
				}
				else if(isset($_SESSION[$data['key']]))
				{
					unset($_SESSION[$data['key']]);
				}
			}
			$_SessionExpired = (isset($_SysSession['_SessionExpired']))?$_SysSession['_SessionExpired']:$data['_SessionExpired'];
			$_SessionExpired = $this->sessionexpired(array('_SessionExpired'=>$_SessionExpired,'key'=>$data['key'],'action'=>$data['action']));
			if($_SessionExpired==true)
			{
				$SessionData = array();
			}
		}
		return $SessionData;
	}
	
	private function syslogged($data=NULL)
	{
		#################################################################################
		$_AppData = $data['_AppData'];unset($data['_AppData']);
		#################################################################################
		$SessionData = array();
		if(!empty($data) and !empty($_AppData))
		{
			$_SysLogged = array('iprgm','uprgm','global');
			$data['_SysLogged'] = (!empty($data['_SysLogged']))?$data['_SysLogged']:$_SysLogged;
			if($data['action']=='set')
			{
				foreach($data['_SysLogged'] as $key=>$val)
				{
					if(in_array($val,$_SysLogged))
					{
						$keyset = ($val!='global')?$_AppData[0][$val.'_name'].'_'.$val.'logged':'globallogged';
						$_SESSION['_SysLogged'][$keyset] = true;
					}
				}
			}
			else if($data['action']=='get')
			{
				foreach($data['_SysLogged'] as $key=>$val)
				{
					if(in_array($val,$_SysLogged))
					{
						$keyget = ($val!='global')?$_AppData[0][$val.'_name'].'_'.$val.'logged':'globallogged';
						if(isset($_SESSION['_SysLogged'][$keyget]))
						{
							$SessionData[$keyget] = $_SESSION['_SysLogged'][$keyget];
						}
						else
						{
							$SessionData[$keyget] = false;
						}
					}
				}
			}
			else if($data['action']=='unset')
			{
				foreach($data['_SysLogged'] as $key=>$val)
				{
					if(in_array($val,$_SysLogged))
					{
						$keyget = ($val!='global')?$_AppData[0][$val.'_name'].'_'.$val.'logged':'globallogged';
						if(isset($_SESSION['_SysLogged'][$keyget]))
						{
							unset($_SESSION['_SysLogged'][$keyget]);
						}
					}
				}
			}
			$_SessionExpired = (isset($_SESSION['_SysLogged']['_SessionExpired']))?$_SESSION['_SysLogged']['_SessionExpired']:$data['_SessionExpired'];
			$_SessionExpired = $this->sessionexpired(array('_SessionExpired'=>$_SessionExpired,'key'=>'_SysLogged','action'=>$data['action']));
			if($_SessionExpired==true)
			{
				$SessionData = array();
			}
		}
		return $SessionData;
	}
}
?>