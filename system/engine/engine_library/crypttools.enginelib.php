<?php
/*
 * File name		: cypttools.enignelib.php
 * Author			: Someone on the Internet (:P)
 * Site				: php.net
 * Framework		: thtech
 * Library type		: Engine Library 
 * Version			: 0
 * License			: NONE
 * Create Date		: 10 Sep 2013
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains crypttools class to be used by the framework and CMS system.
 *
 */
class CryptTools_EngineLibrary
{
	public function aescbc($content=NULL,$password=NULL,$iv=NULL,$decrypt=false)
	{
		$iv = (!empty($iv))?$iv:mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_RAND);
		return ($decrypt===false)?$this->aescbc_encrypt($content,$password,$iv):$this->aescbc_decrypt($content,$password,$iv);
	}
	
	private function aescbc_encrypt($content=NULL,$password=NULL,$iv=NULL)
	{
    	$encrypted = rtrim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $password, $content, MCRYPT_MODE_CBC, $iv)), "\0\3");
		return array('content'=>$encrypted,'iv'=>$iv);
	}
	
	private function aescbc_decrypt($content=NULL,$password=NULL,$iv=NULL)
	{
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $password, base64_decode($content), MCRYPT_MODE_CBC, $iv), "\0\3");
		return array('content'=>$decrypted,'iv'=>$iv);
	}
	
	public function aesecb($content=NULL,$password=NULL,$iv=NULL,$decrypt=false)
	{
		$iv = (!empty($iv))?$iv:mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND);
		return ($decrypt===false)?$this->aesebc_encrypt($content,$password,$iv):$this->aesebc_decrypt($content,$password,$iv);
	}
	
	private function aesecb_encrypt($content=NULL,$password=NULL,$iv=NULL)
	{
    	$encrypted = trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$password,$content,MCRYPT_MODE_ECB,$iv)));
		return array('content'=>$encrypted,'iv'=>$iv);
	}
	
	private function aesecb_decrypt($content=NULL,$password=NULL,$iv=NULL)
	{
		$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$password,base64_decode($content),MCRYPT_MODE_ECB,$iv));
		return array('content'=>$decrypted,'iv'=>$iv);
	}
}
?>