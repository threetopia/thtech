<?php
/*
 * File name		: geoiptools.enignelib.php
 * Author			: Geo Plugin Dot Com
 * Site				: www.geoplugin.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.0
 * License			: GPL
 * Create Date		: 3 Nov 2014
 * Modified Date	: 3 Nov 2014
 * File Description	: This file contains geoiptools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class GeoIPTools_EngineLibrary extends Systems
{
	private $GeoIPData = array();
	public function __construct()
	{
		$this->GeoIPData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
	}
}
?>