<?php
/*
 * File name		: datetools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains datetools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class DateTools_EngineLibrary extends Systems
{
	public function __construct()
	{
		$this->defaulttimezone = ((!empty($timezone))?$timezone:((!empty($this->_LoaderConfig['datetools']['timezone']))?$this->_LoaderConfig['datetools']['timezone']:DEFAULT_TIMEZONE));
	}
	
	private $defaulttimezone;
	public function settimezone($timezone=NULL)
	{
		date_default_timezone_set($this->defaulttimezone);
	}	
	
	/*
	 * Timezone Converter
	 * How to :
	 */
	public function timezoneconverter($data=NULL)
	{
		$fromzone = (isset($data['from']))?$data['from']:$this->defaulttimezone;
		$this->settimezone($fromzone);//date_default_timezone_set($fromzone);//set from time zone
		$date = new DateTime(((!empty($data['datetime']))?$data['datetime']:date('d-m-Y H:i:s')),new DateTimeZone($fromzone));
		$fromdate = $date->format((!empty($data['fromdateformat']))?$data['fromdateformat']:((!empty($data['dateformat']))?$data['dateformat']:'Y-m-d'));
		$fromtime = $date->format((!empty($data['fromtimeformat']))?$data['fromtimeformat']:((!empty($data['timeformat']))?$data['timeformat']:'H:i:s'));
		$fromunixtime = mktime($date->format('H'),$date->format('i'),$date->format('s'),$date->format('m'),$date->format('d'),$date->format('Y'));
		
		$tozone = (!empty($data['to']))?$data['to']:$defaulttimezone;
		$this->settimezone($tozone);//date_default_timezone_set($tozone);//set to timezone
		$date->setTimeZone(new DateTimeZone($tozone));
		$todate = $date->format((!empty($data['todateformat']))?$data['todateformat']:((!empty($data['dateformat']))?$data['dateformat']:'Y-m-d'));
		$totime = $date->format((!empty($data['totimeformat']))?$data['totimeformat']:((!empty($data['timeformat']))?$data['timeformat']:'H:i:s'));
		$tounixtime = mktime($date->format('H'),$date->format('i'),$date->format('s'),$date->format('m'),$date->format('d'),$date->format('Y'));
		//echo $date->format('d-m-Y H:i:s P e');
		date_default_timezone_set($this->defaulttimezone);//backit again to default system timezone
		return array('fromzone'=>$fromzone,'fromdate'=>$fromdate,'fromtime'=>$fromtime,'fromunixtime'=>$fromunixtime,'tozone'=>$tozone,'todate'=>$todate,'totime'=>$totime,'tounixtime'=>$fromunixtime);
	}

	public function datediff($data=NULL)
	{
		$s = (!empty($data['startdate']))?$data['startdate']:array();
		$startdate = NULL;
		$e = (!empty($data['enddate']))?$data['enddate']:array();
		$enddate = NULL;
		$diff = NULL;
		if(!empty($s) and !empty($e))
		{
			//$startdate = new DateTime($s[5].'-'.$s[3].'-'.$s[4].' '.$s[0].':'.$s[1].':'.$s[2]);
			//$enddate = new DateTime($e[5].'-'.$e[3].'-'.$e[4].' '.$e[0].':'.$e[1].':'.$e[2]);
			$startdate = new DateTime(date('Y',$s).'-'.date('m',$s).'-'.date('d',$s).' '.date('H',$s).':'.date('i',$s).':'.date('s',$s));
			$enddate = new DateTime(date('Y',$e).'-'.date('m',$e).'-'.date('d',$e).' '.date('H',$e).':'.date('i',$e).':'.date('s',$e));
			$diff = $startdate->diff($enddate);
		}
		$dayslength = ((!empty($diff))?$diff->days:((!empty($data['dayslength']))?$data['dayslength']:0));
		return array('startdate'=>$startdate,'enddate'=>$enddate,'diff'=>$diff,'dayslength'=>$dayslength);     
	}
}
?>