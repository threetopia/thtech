<?php
class ReCaptchaTools_EngineLibrary extends Systems
{
	public function __construct($data=NULL)
	{
		$this->LoadRecaptcha();
	}
	
	private function LoadRecaptcha($data=NULL)
	{
		include_once(dirname(__FILE__).'/recaptcha/recaptchalib.php');
	}
	
	private $RecaptchaConfig;
	
	private function GetRecaptchaConfig()
	{
		$RecaptchaConfig = array();
		$RecaptchaConfig = $this->_SysEngine->configtools->recaptchatools;
		$RecaptchaConfig = (isset($this->_LoaderConfig['recaptchatools']))?$this->_LoaderConfig['recaptchatools']:$RecaptchaConfig;
		$RecaptchaConfig = (isset($this->_CurProgramConfig['recaptchatools']))?$this->_CurProgramConfig['recaptchatools']:$RecaptchaConfig;
		return $RecaptchaConfig;
	}
	
	public function gethtml($publickey=NULL,$error=NULL,$https=NULL)
	{
		$RecaptchaConfig = $this->GetRecaptchaConfig();
		$https = ($https!=NULL)?$https:(($this->_URLDecode['URLProtocol']=='https')?true:false);
		$publickey = ($publickey!=NULL)?$publickey:$RecaptchaConfig['publickey'];
		return recaptcha_get_html($publickey,$error,$https);
	}
	
	public function checkanswer($recaptcha_challenge_field=NULL,$recaptcha_response_field=NULL,$privatekey=NULL)
	{
		$RecaptchaConfig = $this->GetRecaptchaConfig();
		$privatekey = ($privatekey!=NULL)?$privatekey:$RecaptchaConfig['privatekey'];
		return recaptcha_check_answer($privatekey,$_SERVER["REMOTE_ADDR"],$recaptcha_challenge_field,$recaptcha_response_field);
	}
}
?>