<?php
class MySQL_DBToolsLibrary
{
	public function connect($data=NULL)
	{
		$DBConnect = mysqli_connect($data['DBConfig']['DBHost'],$data['DBConfig']['DBUser'],$data['DBConfig']['DBPassword']) or mysqli_error($DBConnect);
		return $DBConnect;
	}
	
	private function queryData($data=NULL)
	{
		if(!empty($data['DBData']['DBConfig']['DBName']))
		{
			mysqli_select_db($data['DBData']['DBSource'],$data['DBData']['DBConfig']['DBName']) or false;
		}
		$query = @mysqli_query($data['DBData']['DBSource'],$data['SQL']);
		return $query;
	}
	
	public function getData($data=NULL)
	{
		$query = $this->queryData($data);
		if($query)
		{
			$data = array();
			for($a=0; $row = mysqli_fetch_array($query,MYSQL_ASSOC); $a++)
			{
				$data[$a] = $row;
			}
			mysqli_free_result($query);
			return $data;
		}
		return false;
	}
	
	public function executeData($data=NULL)
	{
		$SQLAction = (!empty($data['SQLAction']))?$data['SQLAction']:NULL;
		$query = $this->queryData($data);
		if($query)
		{
			return (in_array(strtoupper($SQLAction),array('INSERT')))?@mysqli_insert_id($data['DBData']['DBSource']):@mysqli_affected_rows($data['DBData']['DBSource']);
		}
		return false;
	}
	
	public function close($data=NULL)
	{
		if(is_object($data['DBSource']))
		{
			mysqli_close($data['DBSource']);
		}
	}
	
	public function BuildSQLS($data=NULL,$deep=0,$loop=0)
	{
		$SQL = "";
		if(!empty($data['SQL']))
		{
			foreach($data['SQL'] as $key=>$val)
			{
				if(!empty($val) and method_exists($this,$key))
				{
					$SQL .= $this->$key($val,$deep,$loop);
				}
			}
		}
		if($deep>0)
		{
			$SQL = "(".$SQL.")";
		}
		return $SQL;
	}
}
?>