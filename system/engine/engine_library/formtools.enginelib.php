<?php
/*
 * File name		: formtool.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.6
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains formtools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class FormTools_EngineLibrary extends Systems
{
	/*
	 * Form creation and validation method
	 */
	public $result = '';

	private function formvalidation_process($val=NULL,$key=NULL,$method=NULL)
	{
		/*
		 *	Validation for form
		 */
		$validation = (isset($val['validation']))?$val['validation']:array();
		$isvalid = array();
		$message = '';
		$output = '';
		$result = false;
		$typeuserange = array('select','radio','checkbox');
		$method = ($val['type']=='file')?'_FILES':$method;
		$source = ($method==='_FILES' and !empty($_FILES))?$_FILES:(($method==='_POST' and !empty($_POST))?$_POST:(($method==='_GET' and !empty($_GET))?$_GET:array()));
		$value = (isset($val['option']))?$val['option']:((!empty($val['value']))?$val['value']:NULL);
		if(!isset($validation['isvalid']))
		{
			if(isset($val) and $val['type']=='email')
			{
				$validation[] = 'email';
			}
			if(isset($val) and $val['type']=='date')
			{
				$validation[] = 'date';
			}
			if(isset($val['disabled']))
			{
				if(is_array($val['disabled']))
				{
					$validation['disabled'] = $val['disabled'];
				}
				else
				{
					$validation[] = 'disabled';
				}
			}
			if(isset($val) and $val['type']=='recaptcha')
			{
				$validation[] = 'required';
				$validation[] = 'recaptcha';
				$val['name'] = 'recaptcha_response_field';
			}
			if(isset($val) and in_array($val['type'],$typeuserange) and ((empty($val['checked']) and empty($val['selected'])) and empty($_POST[$val['name']])))
			{
				$value = array();
			}
			if(isset($val) and in_array($val['type'],$typeuserange) and !empty($_POST[$val['name']]))
			{
				$validation[] = 'range';
			}
			$maxvalue = (!empty($val['max']))?$val['max']:((!empty($validation['max']))?$validation['max']:NULL);
			if(!empty($maxvalue) and !empty($_POST[$val['name']]))
			{
				$validation['max'] = $maxvalue;
			}
			$minvalue = (!empty($val['min']))?$val['min']:((!empty($validation['min']))?$validation['min']:NULL);
			if(!empty($minvalue) and !empty($_POST[$val['name']]))
			{
				$validation['min'] = $minvalue;
			}
			if(isset($val['maxlength']))
			{
				$validation['maxlength'] = $val['maxlength'];
			}
			if(isset($val['minlength']))
			{
				$validation['minlength'] = $val['minlength'];
			}
			$filtered = $this->_SysEngine->inputtools->filter($val['name'],array($val['name']=>$value),$validation,array($val['name']=>$key),$method);
			$isvalid = (isset($filtered['validation'][$val['name']]))?array($filtered['validation'][$val['name']]):array(true);
			$message = (isset($filtered['message'][$val['name']]))?$filtered['message'][$val['name']]:'';
			$result = (!empty($source) and isset($filtered['result'][$val['name']]))?$filtered['result'][$val['name']]:NULL;
		}
		else
		{
			$isvalid[] = $validation['isvalid'];//overwrite by manual value provide from input
			if($validation['isvalid']==false and !empty($validation['invalid_message']))
			{
				$message = $validation['invalid_message'];
			}
			else
			{
				$message = ((!is_numeric($key))?$key:$val['name']).'is invalid';
			}
		}
		/*
		 * Validation end here
		 */
		$isvalid = (is_array($isvalid) and in_array(false,$isvalid))?false:true;
		return array('isvalid'=>$isvalid,'result'=>$result,'message'=>$message);	
	}
	
	public function createform($data=NULL)
	{
		$_SysEngine = $this->_SysEngine;
		$_AppData = $this->_AppData;
		$_URLDecode = $this->_URLDecode;
		$validation = (!empty($data['validation']))?$data['validation']:array();
		$result = (!empty($data['result']))?$data['result']:array();
		$rowdeep = (!empty($data['rowdeep']))?$data['rowdeep']:1;
		$method = (!empty($data['form']['method']))?'_'.strtoupper($data['form']['method']):'_POST';
		$hash = array();
		$html = '';
		if(!empty($data['form']))
		{
			$html .= '<form';
			foreach($data['form'] as $key=>$val)
			{
				$key = strtolower($key);
				if($key=='action' and empty($val))
				{
					$val = $this->_SysEngine->urltools->createurl(array('URLApp'=>$_AppData[0]['uprgm_name'],'URLLink'=>$_URLDecode['URLLink'],'URLString'=>$_URLDecode['URLString']));
				}
				$html .= ' '.$key.'="'.$val.'" ';
			}
			$html .= '>';
		}
		$html .= '<table';
		if(!empty($data['table']))
		{
			foreach($data['table'] as $key=>$val)
			{
				$key = strtolower($key);
				$html .= ' '.$key.'="'.$val.'"'.' ';
			}
		}
		$html .= '>';
		if($rowdeep>1)
		{
			$html .= '<tr>';
		}
		if(!empty($data['input']))
		{
			foreach($data['input'] as $key=>$val)
			{
				if(isset($val['form']))
				{
					$formdata = (!empty($data['form']))?$data['form']:NULL;
				}
				if($rowdeep<=1)
				{
					$html .= '<tr>';
				}
				$html .= '	<td>'.((!is_numeric($key) and !empty($key))?$key:'').'</td>';
				$html .= '	<td>'.((!is_numeric($key) and !empty($key))?'&nbsp;:&nbsp;':'').'</td>';
				$html .= '	<td>';
				if(is_array($val) and (is_array($val[key($val)]) and is_numeric(key($val))))
				{
					$tabledata = (isset($val['tabledata']))?$val['tabledata']:array('class'=>'table_subform');unset($val['tabledata']);
					$formcreate_formrow = $this->createform(array('input'=>$val,'validation'=>$validation,'result'=>$result,'table'=>$tabledata,'rowdeep'=>$rowdeep+1));
					$validation = array_merge($validation,$formcreate_formrow['validation']);
					$result = array_merge($result,$formcreate_formrow['result']);
					$html .= $formcreate_formrow['html'];
					$hash = array_merge($hash,$formcreate_formrow['hash']);
				}
				elseif(is_array($val) and isset($val['name']))
				{
					$validationdata = $this->formvalidation_process($val,$key,$method/*array('val'=>$val,'key'=>$key,'method'=>$method)*/);
					$val['validationdata'][$val['name']] = $validationdata;
					$validation[$val['name']] = $validationdata['isvalid'];
					($validationdata['result']===false)?NULL:$result[$val['name']] = $validationdata['result'];
					$html .= '	'.$this->formcreate_formattrib($val);
					$hash[] = $val['name'];
				}
				else
				{
					$html .= '	'.$this->formcreate_formattrib($val);
				}
				$html .= '	</td>';
				if($rowdeep<=1)
				{
					$html .= '</tr>';
				}
			}
		}
		if($rowdeep>1)
		{
			$html .= '</tr>';
		}
		else
		{
			$time = time();
			if(in_array('recaptcha',$hash))
			{
				$hash[] = 'recaptcha_challenge_field';
				$hash[] = 'recaptcha_response_field';
				unset($hash['recaptcha']);
			}
			asort($hash);
			$input = $_SysEngine->inputtools->filter(NULL,array(),array(),array(),$method);
			$inputkeys = array_keys($input['result']);
			asort($inputkeys);
			//print_r($inputkeys);echo '<br>';print_r($hash);echo '<br>';
			$resulthash = hash('sha1',(implode('',$inputkeys).SYSTEMHASH.$time));
			$hash = hash('sha1',(implode('',array_intersect($inputkeys,$hash)).SYSTEMHASH.$time));
			//$html .= '<input type="hidden" name="_formhash" value="'.$hash.'">';
			$validation['_formhash'] = ($resulthash===$hash)?true:false;
		} 
		$html .= '</table>';
		if(isset($data['form']))
		{
			$html .= '</form>';
		}
		return array('html'=>$html,'validation'=>$validation,'result'=>$result,'hash'=>$hash);
	}
	
	private function formcreate_formattrib($data=NULL)
	{
		$InputType = array(/*'button','checkbox',*/'color',/*'date','datetime','file',*/'datetime-local','email','hidden','image','month','number','password',/*'radio',*/'range','reset','search'/*,'submit'*/,'tel','text','time','url','week');
		$html  = '';
		$method = (isset($data['type']))?'formcreate_'.$data['type']:'';
		if(!is_array($data))
		{
			$html = $data;
		}
		else if(isset($data['type']) and in_array($data['type'],$InputType))
		{
			ksort($data);
			$html = $this->formcreate_input($data);
		}
		else if($method!='' and method_exists($this,$method))
		{
			ksort($data);
			$html = $this->$method($data);
		}
		return $html;
	}
	
	private function formcreate_submit($data=NULL)
	{
		return $this->formcreate_button($data);
	}
	
	private function formcreate_button($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$data['value'] = (isset($data['value']))?$data['value']:NULL;
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		unset($data['validation'],$data['validationdata'],$data['back_title'],$data['clear']);
		$html  = '';
		if(!empty($data))
		{
			$title = (array_key_exists('title', $data))?$data['title']:$data['value'];
			$html .= '<div class="buttonform input'.$data['name'].'">';
			$html .= 	'<button';
			foreach($data as $key=>$val)
			{
				$key = strtolower($key);
				if($key!='separator' and $key!='title')
				{
					(isset($validationdata['result']) and $key=='value' and $clear!='clear')?$val = $validationdata['result']:NULL;
					$html .= ' '.$key.'="'.$val.'"';
				}
			}
			$html .= 	'/>';
			$html .= $title;
			$html .= '</button>';
			$html .= '</div>';
			if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
			{
				$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
			}
		}
		return $html;
	}
	
	private function formcreate_radio($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$value = (isset($data['value']))?$data['value']:NULL;
		$checked = (isset($data['checked']))?$data['checked']:NULL;
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		unset($data['validation'],$data['validationdata'],$data['back_title'],$data['value'],$data['checked'],$data['clear']);
		$html  = '';
		if(!empty($data))
		{
			$this->_SysEngine->addhtmlhead(array('style'=>array('.radioform{width:13px;}')));
			$loop = 0;
			$html .= '<div class="radioform radio'.$data['name'].'">';
			$html .= '	<ul>';
			foreach($value as $key0=>$val0)
			{
				$html .= '	<li>';
				if(!empty($val0))
				{
					$data['class'] = 'radioform'.((!empty($data['class']))?' '.$data['class']:'');
					$html .= '<input';
					foreach($data as $key1=>$val1)
					{
						$key1 = strtolower($key1);
						$html .= ' '.$key1.'="'.$val1.'"';
					}
					$html .= ' value="'.$key0.'" ';
					$html .= ((!empty($checked) and !isset($validationdata['result']) and $clear!='clear')?(in_array($key0,$checked)):((isset($validationdata['result']) and $clear!='clear')?$validationdata['result']==$key0:false))?'checked="checked"':'';
					$html .= '/>';
				}
				$html .= ' '.$val0.' ';
				$html .= '	</li>';
				$loop++;
			}
			$html .= '	</ul>';
			$html .= '</div>';
			if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
			{
				$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
			}
		}
		return $html;
	}
	
	private function formcreate_checkbox($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$value = (isset($data['value']))?$data['value']:NULL;
		$checked = (isset($data['checked']))?$data['checked']:NULL;
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		$class = (isset($data['class']))?$data['class']:NULL;
		$id = (isset($data['id']))?$data['id']:NULL;
		unset($data['validation'],$data['validationdata'],$data['back_title'],$data['value'],$data['checked'],$data['clear']);
		$html  = '';
		$validationdata['result'] = (isset($validationdata['result']))?$validationdata['result']:false;
		if(!empty($value))
		{
			$loop1=1;
			$this->_SysEngine->addhtmlhead(array('style'=>array('.checkboxinput{width:13px;}')));
			$html .= '<div class="checkboxform checkbox'.$data['name'].'">';
			$html .= '	<ul>';
			foreach($value as $key0=>$val0)
			{
				$html .= '<li>';
				if(!empty($data))
				{
					$label = '';
					$data['class'] = 'checkboxinput'.((!empty($data['class']))?' '.$class:'');
					$data['id'] = ((!empty($id))?$id.' ':'').$data['name'].'_checkbox';
					$html .= '<input';
					foreach($data as $key1=>$val1)
					{
						$key1 = strtolower($key1);
						if($key1=='name')
						{
							$val1 = $val1.'[]';
						}
						if($key1=='id')
						{
							$val1 = $val1.$loop1;
							$label = $val1;
						}
						$val1 = ((is_array($val1) and in_array($key0,$val1)))?$key1:$val1;
						$html .= (!is_array($val1))?' '.$key1.'="'.$val1.'" ':'';
					}
					$html .= ' value="'.$key0.'" ';
					$html .= (($validationdata['result']!==false and is_array($validationdata['result']) and $clear!='clear')?(in_array($key0,$validationdata['result'])):(($validationdata['result']!==false and !is_array($validationdata['result']) and $clear!='clear')?($validationdata['result']==$key0):(($validationdata['result']===false and !empty($checked) and $clear!='clear')?(in_array($key0,$checked)):false)))?' checked="checked"':'';
					$html .= '/>';
					$loop1++;
				}
				$html .= '<label for="'.$label.'">'.$val0.'</label>';
				$html .= '</li>';
			}
			$html .= '	</ul>';
			$html .= '</div>';
			if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
			{
				//$html .= '<tr>';
				$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
				//$html .= '</tr>';
			}
		}
		return $html;
	}
	
	private function formcreate_input($data=NULL)
	{
		//print_r($data);
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$data['value'] = (isset($data['value']))?$data['value']:NULL;
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		unset($data['validation'],$data['validationdata'],$data['back_title'],$data['clear']);
		$html  = '';
		if(!empty($data))
		{
			$html .= '<div class="inputform input'.$data['name'].'">';
			$html .= 	'<input';
			foreach($data as $key=>$val)
			{
				$key = strtolower($key);
				if($key!='separator' and $key!='title')
				{
					(!empty($validationdata['result']) and $key=='value' and $clear!='clear')?$val = $validationdata['result']:NULL;
					$html .= ' '.$key.'="'.$val.'" ';
				}
			}
			$html .= 	'/>';
			$html .= '</div>';
			if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
			{
				$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
			}
		}
		return $html;
	}

	private function formcreate_date($data=NULL)
	{
		$browser = $this->_SysEngine->browsertools->getinfo();
		if(in_array($browser['browser'],array('Firefox','Internet Explorer')))
		{
			$addhtmlhead = array(
								"link"=>array(
											array('rel'=>"stylesheet",'href'=>"http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"),
											array('rel'=>"stylesheet",'href'=>"http://jqueryui.com/jquery-wp-content/themes/jqueryui.com/style.css")
											),
								"script"=>array(
												array("src"=>"http://code.jquery.com/jquery-1.11.0.min.js"),
										   		array("src"=>"http://code.jquery.com/ui/1.11.2/jquery-ui.js"),
										   		'$(function(){$("#'.((!empty($data['id']))?$data['id']:$data['name']).'").datepicker();});'
												)
								);
			$this->_SysEngine->addhtmlhead(array('html_head'=>$addhtmlhead));
			$data['id'] = ((!empty($data['id']))?$data['id']:$data['name']);
		}
		return $this->formcreate_input($data);
	}

	private function formcreate_file($data=NULL)
	{
		$html  = '';
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$multiple = (isset($data['multiple']))?true:false;
		unset($data['multiple']);
		$data['name'] = (strpos($data['name'],'[]')===false)?$data['name'].'[]':$data['name'];
		$input = $this->formcreate_input($data);
		for($i=0;$i<((!empty($validationdata['result']['name']))?count($validationdata['result']['name']):1);$i++)
		{
			$html .= $input;
		}
		if($multiple===true)
		{
			$html .= $this->formcreate_button(array('name'=>'addmorefiles','class'=>'add_more_files','value'=>'Add More Files'));
			$addhtmlhead = array(
								"script"=>array(
										   		'$(document).ready(function(){$(\'.add_more_files\').click(function(e){e.preventDefault();$(this).before(\''.$input.'\');});});'
												)
								);
			$this->_SysEngine->addhtmlhead(array('html_head'=>$addhtmlhead));
		}
		if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
		{
			$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
		}
		return $html;
	}
	
	private function formcreate_select($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$option = $data['option'];
		$selected = (isset($data['selected']))?$data['selected']:NULL;
		$disabled = NULL;
		if(!empty($data['disabled']) and is_array($data['disabled']))
		{
			$disabled = $data['disabled'];
			unset($data['disabled']);
		}
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		unset($data['validation'],$data['selected'],$data['option'],$data['validationdata'],$data['clear']);
		$html  = '';
		if(!empty($data))
		{
			$html .= '<div class="selectform select'.$data['name'].'">';
			$html .= 	'<select';
			foreach($data as $key=>$val)
			{
				$key = strtolower($key);
				if($key!='separator' and $key!='option')
				{
					if($key=='name' and isset($data['multiple']))
					{
						$val = $val.'[]';
					}
					$html .= ' '.$key.'="'.$val.'" ';
				}
			}
			$html .= 	'>';
			if(!empty($option))
			{
				foreach($option as $key=>$val)
				{
					$html .= '<option value="'.$key.'"';
					//$html .= ((!isset($_POST[$data['name']]) and !empty($selected))?in_array($key,$selected):((isset($_POST[$data['name']]) and is_array($_POST[$data['name']]))?in_array($key,$_POST[$data['name']]):((isset($_POST[$data['name']]))?$_POST[$data['name']]==$key:false)))?' selected="selected"':'';
					$html .= ((isset($validationdata['result']) and is_array($validationdata['result']) and $clear!='clear')?in_array($key,$validationdata['result']):((isset($validationdata['result']) and $clear!='clear')?$validationdata['result']==$key:((!isset($validationdata['result']) and !empty($selected) and $clear!='clear')?in_array($key,$selected):false)))?' selected="selected"':'';
					$html .= (is_array($disabled) and in_array($key,$disabled))?' disabled="disabled"':'';
					$html .= '>';
					$html .= ''.$val.'';
					$html .= '</option>';
				}
			}
			$html .= 	'</select>';
			$html .= '</div>';
			if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
			{
				$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
			}
		}
		return $html;
	}
	
	private function formcreate_textarea($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		$value = ((!empty($validationdata['result']) and $clear!='clear')?$validationdata['result']:((isset($data['value']) and !empty($data['value']) and $clear!='clear')?$data['value']:NULL));
		unset($data['value'],$data['validation'],$data['type'],$data['validationdata'],$data['clear']);
		$html  = '';
		if(!empty($data))
		{
			$html .= '<div class="inputform input'.$data['name'].'">';
			$html .= 	'<textarea';
			foreach($data as $key=>$val)
			{
				$key = strtolower($key);
				$html .= ' '.$key.'="'.$val.'" ';
			}
			$html .= 	'/>';
			$html .= 	$value;
			$html .= 	'</textarea>';
			$html .= '</div>';
		}
		if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
		{
			$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
		}
		return $html;
	}

	private function formcreate_ckeditor($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$data['id'] = (!empty($data['id']))?$data['id']:$data['name'];
		$version = ((!empty($data['version']))?$data['version']:'basic');
		unset($data['version']);
		$html  = $this->formcreate_textarea($data);
		$html .= '<script>CKEDITOR.replace(\''.$data['name'].'\');</script>';
		$addhtmlhead = array(
						   "script"=>array(
						   				array("src"=>"http://cdn.ckeditor.com/4.4.4/".$version."/ckeditor.js")
										)
						 	);
		$this->_SysEngine->addhtmlhead(array('html_head'=>$addhtmlhead));
		return $html;
	}
	
	private function formcreate_recaptcha($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$data['value'] = (isset($data['value']))?$data['value']:NULL;
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		$html  = '';
		if(!empty($data))
		{
			$html .= $this->_SysEngine->recaptchatools->gethtml();
			if(isset($validationdata['isvalid']) and $validationdata['isvalid']==false)
			{
				$html .= '<span class="input_error" id="'.$data['name'].'_input_error">'.$validationdata['message'].'</span>';
			}
		}
		return $html;
	}
	
	
	
	private function formcreate_autocomplete($data=NULL)
	{
		$validationdata = (!empty($data['validationdata'][$data['name']]))?$data['validationdata'][$data['name']]:array();
		$data['value'] = (isset($validationdata[$data['name']]['result']))?$validationdata[$data['name']]['result']:((!empty($data['value']))?$data['value']:NULL);
		$clear = (isset($data['clear']))?$data['clear']:NULL;
		$acoption = (!empty($data['acoption']))?$data['acoption']:"";
		unset($data['acoption']);
		$html  = '';
		if(!empty($data))
		{
			$data['type'] = 'text';
			$data['id'] = (!empty($data['id']))?$data['id']:'autocomplete'.$data['name'];
			$this->_SysEngine->addhtmlhead(
										   array(
										   		'script'=>array(
										   						array("src"=>"http://code.jquery.com/jquery-1.11.0.min.js"),
										   						file_get_contents(dirname(__FILE__).'/formtools/autocomplete/jquery.autocomplete.js'),
										   						"var options, a;jQuery(function(){options = {".$acoption."};a = $('#".$data['id']."').autocomplete(options);});"
																),
												'style'=>array(
															   file_get_contents(dirname(__FILE__).'/formtools/autocomplete/style.css')
															   )
												)
										  );
			$html .= $this->formcreate_formattrib($data);
		}
		return $html;
	}
	/*
	 * End of form creation method
	 */
	 
	/*
	 * Form creation and validation method
	 */
	public function createlist($data=NULL)
	{
		$caption = (!empty($data['caption']))?$data['caption']:array();
		$thead = (!empty($data['thead']))?$data['thead']:((!empty($data['input']))?$data['input']:array());
		$tbody = (!empty($data['tbody']))?$data['tbody']:((!empty($data['rawdata']))?$data['rawdata']:array());
		$tfoot = (!empty($data['tfoot']))?$data['tfoot']:array();
		$html  = '';
		if(!empty($data['form']))
		{
			$html .= '<form';
			foreach($data['form'] as $key=>$val)
			{
				$val = ($key=='action' and is_array($val))?$this->_SysEngine->urltools->createurl($val):$val;
				$html .= ' '.$key.'="'.$val.'"';
			}
			$html .= '>';
		}
		$html .= '<table';
		if(!empty($data['table']))
		{
			foreach($data['table'] as $key=>$val)
			{
				$html .= ' '.$key.'="'.$val.'" ';
			}
		}
		$html .= '>';
		if(!empty($tbody) and !empty($thead))
		{
			$loop = 0;
			$html .= '<thead>';
			$html .= '	<tr class="listhead">';
			foreach($thead as $k_head=>$v_head)
			{
				$html .= '<td>';
				$html .= $k_head;
				$html .= '</td>';
			}
			$html .= '	</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			foreach($tbody as $k_body=>$v_body)
			{
				$loop++;
				$rowmodulo = $loop%2;
				$html .= '<tr class="listcontent listcontent_'.$rowmodulo.'">';
				foreach($thead as $k_rowhead=>$v_rowhead)
				{
					$class = str_replace(' ','',strtolower($k_rowhead));
					$html .= '<td class="list_'.$class.' list_'.$class.'_'.$rowmodulo.'">';
					$html .= $this->createlist_replacefilter($v_body,$v_rowhead);
					$html .= '</td>';
				}
				$html .= '</tr>';
			}
			$html .= '</tbody>';
		}
		if(!empty($tfoot))
		{
			$html .= '<thead>';
			$html .= '	<tr class="listfoot">';
			foreach($tfoot as $k_foot=>$v_foot)
			{
				$html .= '<td>';
				$html .= $k_foot;
				$html .= '</td>';
			}
			$html .= '	</tr>';
			$html .= '</thead>';
		}
		$html .= '</table>';
		if(!empty($data['form']))
		{
			$html .= '</form>';
		}
		$html = html_entity_decode($html);
		return array('html'=>$html);
	}

	private $replacefilter_list = array('input','urllink','anchor');
	
	private function createlist_replacefilter($v_body=array(),$v_rowhead='')
	{
		$replace = (array_key_exists($v_rowhead, $v_body))?$v_body[$v_rowhead]:str_replace(array_keys($v_body),array_values($v_body),$v_rowhead);
		$html = '';
		foreach($this->replacefilter_list as $filter)
		{
			$pattern = "/<$filter>(.*?)<\/$filter>/";
			preg_match_all($pattern, $replace, $matches);
			if(!empty($matches[1]))
			{
				for($i=0;$i<count($matches[1]);$i++)
				{
					$val = ($filter=='input')?$this->formcreate_formattrib(json_decode($matches[1][$i],true)):(($filter=='anchor')?$this->_SysEngine->urltools->createanchor(json_decode($matches[1][$i],true)):'');
					$html .= str_replace("<$filter>".$matches[1][$i]."</$filter>",$val,$replace);
				}
			}
		}
		return (!empty($html))?$html:$replace;
	}
	/*
	 * End of form creation method
	 */
}
?>