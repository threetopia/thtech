<?php
/*
 * File name		: httptools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.2
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 04 Apr 2014
 * File Description	: This file contains httptools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class HttpTools_EngineLibrary extends Systems
{
	public function __construct($data=NULL)
	{
		//$_SysEngine = $GLOBALS['_SysEngine'];
		//$this->httperror_template = (isset($_SysEngine))?$_SysEngine:'';
	}
	
	public $iserror = false;
	
	public $errormessage = NULL;
	
	public $httperror_template = false;
	
	public $httperror_data = NULL;
	
	public $httperror_output = '';
	
	public $errortype = '';
	
	public function errorprocess($data=NULL)
	{
		(!empty($data['message']) and empty($this->iserror) and empty($this->errormessage))?$this->errormessage = $data['message']:NULL;
		if(!empty($data['tmpl_name']))
		{
			$this->iserror = $data['tmpl_name'];
			$this->httperror_template = $data['tmpl_name'];
			if(!empty($data['data']))
			{
				$this->httperror_data = $data['data'];
			}
		}
		else if(empty($this->iserror))
		{
			$this->iserror = $data['error'];
		}
	}
	
	public function errorrender($data=NULL)
	{
		unset($GLOBALS['_AppData'],$GLOBALS['_ProgramData']);
		$this->_SysEngine->staging_html_head = array();
		$output = '';
		$errordata = $this->geterrordata($data);
		$output = $this->httperror_template($errordata);
		return $output;
	}
	
	private function geterrordata($data=NULL)
	{
		$message = '';
		$title = '';
		if($this->iserror!==false)
		{
			switch($this->iserror)
			{
				case '401':
					$message = 'Sorry your requested page need authentication.';
					$title = 'Error 401 need authentication';
					//throw new Exception($message);
				break;
				case '404':
					$message = 'Sorry your requested page seems doesn\'t exists.';
					$title = 'Error 404 page not found';
					//throw new Exception($message);
				break;
				case '419':
					$message = 'Your authentication is timeout. Please kindly try to login again.';
					$title = 'Error 419 authentication timeout';
				break;
				case '500':
					$message = 'There something error in the system.';
					$title = 'Error 500 internal server error';
				break;
				default:
					$message = 'There was an error.';
					$title = 'Error '.$this->iserror.'';
				break;
			}
			$homeurl = $this->_SysEngine->urltools->createanchor(array('title'=>'click here to go back to home','href'=>array('URLApp'=>'','URLLink'=>'','URLExtPath'=>array(),'URLString'=>array())));
			$message = ((!empty($this->errormessage))?$this->errormessage:$message).'<br>'.$homeurl;
		}
		return array('message'=>$message,'title'=>$title);
	}
	
	private function httperror_template($data=NULL)
	{
		$addhtmlhead = array(
							   "meta"=>array(
							   				 array("name"=>"description","content"=>"THTech HTTP Error Handler. ".$data['title']),
							   				 array("http-equiv"=>"refresh","content"=>"3;url=".$this->_SysEngine->urltools->createurl(array('URLApp'=>'','URLLink'=>'','URLExtPath'=>array(),'URLString'=>array())))
											 ),
							   "style"=>array(
											  "html{background:none;}body{margin:0 auto !important;font-size:20px !important;font-family:arial !important;background:none !important;}#errorpage{background:white;margin:0 auto;width:800px;text-align:center;}.message{border:1px solid grey;margin:100px 0px 0px 0px;padding:100px 0px 100px 0px;}.errortitle{font-size:33px;}"
											  ),
							   "title"=>array($data['title'],"delimiter"=>" :: ","Powered by THTech Framework by trihartanto.com")
							 	);
		
		$this->_SysEngine->addhtmlhead(array('html_head'=>$addhtmlhead));
		$html  = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$html .= '<html  xmlns="http://www.w3.org/1999/xhtml">';
		$html .= 	'<head>';
		$html .= 		''.$this->_SysEngine->render(array('program_type'=>'html_head')).'';
		$html .= 	'</head>';
		$html .= 	'<body>';
		$html .= 		'<div id="errorpage">';
		$html .= 			'<div class="message"><span class="errortitle">'.$data['title'].'</span><br>'.$data['message'].'<br>redirect in 3 seconds</div>';
		$html .= 		'</div>';
		$html .= 	'</body>';	
		$html .= '</html>';
		return $html;
	}
}
?>