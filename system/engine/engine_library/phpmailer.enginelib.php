<?php
/*
 * File name		: phpmailer.enignelib.php
 * Author			: Marcus Bointon (PHP Mailer), Tri Hartanto (This files)
 * Site				: https://github.com/PHPMailer/PHPMailer, trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: LGPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains direcotorytools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class PHPMailer_EngineLibrary extends Systems
{
	private $PHPMailer;
	
	public function __construct($data=NULL)
	{
		$this->LoadPHPMailerClass();
	}
	
	private function LoadPHPMailerClass($data=NULL)
	{
		$ClassFile = dirname(__FILE__).'/phpmailer/class.phpmailer.php';
		if(file_exists($ClassFile))
		{
			include($ClassFile);
			$this->PHPMailer = new phpmailer();
		}
	}
	
	public function sendmail($data=NULL)
	{
		$_MailConfig   = (isset($this->_CurProgramConfig['email']) and !empty($this->_CurProgramConfig['email']))?$this->_CurProgramConfig['email']:(NULL);
		//print_r($_ProgramConfig);
		$mail             =  $this->PHPMailer;
		$body             =  ((!empty($data['emailBody']))?$data['emailBody']:((!empty($_MailConfig['emailBody']))?$_MailConfig['emailBody']:'Email'));
		
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = ((!empty($data['emailHost']))?$data['emailHost']:((!empty($_MailConfig['emailHost']))?$_MailConfig['emailHost']:EMAIL_HOST));				// SMTP server
		//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;				   // enable SMTP authentication
		$mail->Port       = ((!empty($data['emailPort']))?$data['emailPort']:((!empty($_MailConfig['emailPort']))?$_MailConfig['emailPort']:EMAIL_PORT));				// set the SMTP port for the GMAIL server
		$mail->Username   = ((!empty($data['emailUsername']))?$data['emailUsername']:((!empty($_MailConfig['emailUsername']))?$_MailConfig['emailUsername']:EMAIL_USERNAME));	// SMTP account username
		$mail->Password   = ((!empty($data['emailPassword']))?$data['emailPassword']:((!empty($_ProgramConfig['emailPassword']))?$_ProgramConfig['emailPassword']:EMAIL_PASSWORD));	// SMTP account password
		
		$CheckEmail = explode('@',((!empty($data['emailSentTo']['address']))?$data['emailSentTo']['address']:((!empty($_MailConfig['emailSentTo']['address']))?$_MailConfig['emailSentTo']['address']:'')));
		if(!empty($CheckEmail[1]))
		{
			$CheckEmailDomain = explode('.',$CheckEmail[1]);
		}
		
		$mail->SetFrom(((!empty($data['emailSentFrom']['address']))?$data['emailSentFrom']['address']:((!empty($_MailConfig['emailSentFrom']['address']))?$_MailConfig['emailSentFrom']['address']:EMAIL_ADDRESS)), ((!empty($data['emailSentFrom']['title']))?$data['emailSentFrom']['title']:((!empty($_MailConfig['emailSentFrom']['title']))?$_MailConfig['emailSentFrom']['title']:DEFAULT_EMAIL_TITLE)));
		
		$mail->AddReplyTo(((!empty($data['emailReplyTo']['address']))?$data['emailReplyTo']['address']:((!empty($_MailConfig['emailReplyTo']['address']))?$_MailConfig['emailReplyTo']['address']:EMAIL_ADDRESS)),((!empty($data['emailReplyTo']['title']))?$data['emailReplyTo']['title']:((!empty($_MailConfig['emailReplyTo']['title']))?$_MailConfig['emailReplyTo']['title']:DEFAULT_EMAIL_TITLE)));
		
		$mail->Subject    =  (!empty($data['emailSubject'])?$data['emailSubject']:((!empty($_MailConfig['emailSubject']))?$_MailConfig['emailSubject']:NULL));
		
		//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		
		$mail->MsgHTML($body);
		//echo (!empty($data['emailSentTo']['address']))?$data['emailSentTo']['address']:((!empty($_ProgramConfig['emailSentTo']['address']))?$_ProgramConfig['emailSentTo']['address']:NULL);
		$mail->AddAddress(((!empty($data['emailSentTo']['address']))?$data['emailSentTo']['address']:((!empty($_MailConfig['emailSentTo']['address']))?$_MailConfig['emailSentTo']['address']:NULL)),((!empty($data['emailSentTo']['title']))?$data['emailSentTo']['title']:((!empty($_ProgramConfig['emailSentTo']['title']))?$_MailConfig['emailSentTo']['title']:NULL)));
		$mail->AddBCC(((!empty($data['emailBCC']['address']))?$data['emailBCC']['address']:((!empty($_MailConfig['emailBCC']['address']))?$_MailConfig['emailBCC']['address']:NULL)),((!empty($data['emailBCC']['title']))?$data['emailBCC']['title']:((!empty($_MailConfig['emailBCC']['title']))?$_MailConfig['emailBCC']['title']:NULL)));
		if(!$mail->Send() and (empty($CheckEmail[1]) and empty($CheckEmailDomain[1]))) 
		{
			$error = "Mailer Error: ".$mail->ErrorInfo;
			$state = false;
		}
		else
		{
			$error = false;
			$state = true;
		}
		return array('emailstatus'=>$state,'error'=>$error);
	}
}
?>