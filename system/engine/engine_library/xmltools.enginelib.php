<?php
/*
 * File name		: xmltools.enginelib.php
 * Author			: user1398287 (http://stackoverflow.com/users/1398287/user1398287)
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.0
 * License			: GPL
 * Create Date		: 25 Apr 2014
 * Modified Date	: 25 Apr 2014
 * File Description	: This file contains xmltools class to be used by the framework and CMS system.
 * 					  This file contains any code made by someone http://stackoverflow.com/questions/6578832/how-to-convert-xml-into-array-in-php/20431742#20431742
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class XMLTools_EngineLibrary extends Systems
{
	public function decode($xmlstring=NULL,$toarray=true)
	{
		$xml = simplexml_load_string($xmlstring);
		$json = json_encode($xml);
		return json_decode($json,$toarray);
	}
	
	public function encode($content=NULL)
	{
		
	}
}
?>