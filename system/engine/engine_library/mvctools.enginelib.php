<?php
/*
 * File name		: mvctools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.5
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 04 Apr 2014
 * File Description	: This file contains MVC (Model View Controller) class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class MVCTools_EngineLibrary extends Systems
{
	public $staging_mvc_view = NULL;
	public $returndata = array();
	
	public function mainprocess($data=NULL)
	{
		$output = '';
		$GetConfig = $this->_SysEngine->configtools->getconfig('mvctools');
		$ActiveProgram = (!empty($GetConfig['mvc_class']))?$GetConfig['mvc_class']:((!empty($this->_CurProgramData['uprgm_name']))?$this->_CurProgramData['uprgm_name']:$this->_CurProgramData['iprgm_name']);
		//$ActiveProgram = (!empty($this->_NavData[0]['nav_mvc_class']) and $this->_CurProgramData['iprgm_type']=='app')?$this->_NavData[0]['nav_mvc_class']:(!empty($this->_CurProgramData['uprgm_mvc_class']))?$this->_CurProgramData['uprgm_mvc_class']:((!empty($this->_CurProgramData['uprgm_name']))?$this->_CurProgramData['uprgm_name']:$this->_CurProgramData['iprgm_name']);
		/*
		 * Load Controller Class
		 */
		$ControllerProgram = $ActiveProgram.'_c';
		$autoload[] = array('sourcedir'=>$this->_CurSystemProgramDir.'/controller','file'=>$ControllerProgram.'.php','class'=>NULL,'alias'=>NULL,'loaderstate'=>NULL);
		/*
		 * Load Model Class
		 */
		$ModelProgram = $ActiveProgram.'_m';
		$autoload[] = array('sourcedir'=>$this->_CurSystemProgramDir.'/model','file'=>$ModelProgram.'.php','class'=>NULL,'alias'=>NULL,'loaderstate'=>NULL);	
		/*
		 * Load View Class
		 */
		$ViewProgram = $ActiveProgram.'_v';
		$autoload[] = array('sourcedir'=>$this->_CurSystemProgramDir.'/view','file'=>$ViewProgram.'.php','class'=>NULL,'alias'=>NULL,'loaderstate'=>NULL);	
		/*
		 * Class Loader
		 */
		$this->class_loader(array('sourcedir'=>NULL,'autoload'=>$autoload,'loaderstate'=>'dynamic'));
		/*
		 * Call predifined class in autoloader
		 */
		${$ControllerProgram} = (!is_null($this->$ControllerProgram))?$this->$ControllerProgram:new StdClass;
		${$ViewProgram} = (!is_null($this->$ViewProgram))?$this->$ViewProgram:new StdClass;
		if($this->_CurProgramData['iprgm_type']=='app')
		{
			$PageMethod = (!empty($GetConfig['mvc_method']) and method_exists(${$ControllerProgram},$GetConfig['mvc_method']))?$GetConfig['mvc_method']:((!empty($this->_NavData[0]['nav_name']) and method_exists(${$ControllerProgram},$this->_NavData[0]['nav_name']))?$this->_NavData[0]['nav_name']:((isset($this->_URLDecode['URLLink']) and method_exists(${$ControllerProgram},$this->_URLDecode['URLLink']))?$this->_URLDecode['URLLink']:((!isset($this->_URLDecode['URLLink']) and empty($this->_NavData[0]['nav_name']) and method_exists(${$ControllerProgram},'index'))?'index':'')));
			//$PageMethod = (!empty($this->_NavData[0]['nav_mvc_method']) and method_exists(${$ControllerProgram},$this->_NavData[0]['nav_mvc_method']))?$this->_NavData[0]['nav_mvc_method']:((!empty($this->_CurProgramData['uprgm_mvc_method']) and method_exists(${$ControllerProgram},$this->_CurProgramData['uprgm_mvc_method']))?$this->_CurProgramData['uprgm_mvc_method']:((!empty($this->_NavData[0]['nav_name']) and method_exists(${$ControllerProgram},$this->_NavData[0]['nav_name']))?$this->_NavData[0]['nav_name']:((isset($this->_URLDecode['URLLink']) and method_exists(${$ControllerProgram},$this->_URLDecode['URLLink']))?$this->_URLDecode['URLLink']:((!isset($this->_URLDecode['URLLink']) and empty($this->_NavData[0]['nav_name']) and method_exists(${$ControllerProgram},'index'))?'index':''))));
		}
		else
		{
			$PageMethod = (!empty($GetConfig['mvc_method']) and method_exists(${$ControllerProgram},$GetConfig['mvc_method']))?$GetConfig['mvc_method']:((!empty($this->_AppData[0]['uprgm_name']) and method_exists(${$ControllerProgram},$this->_AppData[0]['uprgm_name']))?$this->_AppData[0]['uprgm_name']:((method_exists(${$ControllerProgram},'index'))?'index':''));
			//$PageMethod = (!empty($this->_CurProgramData['uprgm_mvc_method']) and method_exists(${$ControllerProgram},$this->_CurProgramData['uprgm_mvc_method']))?$this->_CurProgramData['uprgm_mvc_method']:((!empty($this->_AppData[0]['uprgm_name']) and method_exists(${$ControllerProgram},$this->_AppData[0]['uprgm_name']))?$this->_AppData[0]['uprgm_name']:((method_exists(${$ControllerProgram},'index'))?'index':''));
		}
		if(!empty($PageMethod))
		{
			$func_get_args = func_get_args();
			$returndata = call_user_func_array(array(${$ControllerProgram},$PageMethod),$func_get_args);
			$this->returndata = (!empty($returndata))?$returndata:array();
			if(method_exists(${$ViewProgram},$PageMethod))
			{
				$output .= call_user_func_array(array(${$ViewProgram},$PageMethod),$this->returndata);
			}
			else if(file_exists($this->_CurSystemProgramDir.'/view/'.$ActiveProgram.'/'.$PageMethod.'.php'))
			{
				$output .= $this->viewprocess(array('file'=>''.$PageMethod.'.php','data'=>$this->returndata,'sourcedir'=>$this->_CurSystemProgramDir.'/view/'.$ActiveProgram.'/','grab'=>true));
			}
		}
		else if(empty($PageMethod) and $this->_CurProgramData['iprgm_type']=='app')
		{
			$this->_SysEngine->httptools->errorprocess(array('error'=>'404','message'=>'MVC tools can\'t found any match controller or method on Application to render the page.'));# 404 Page not found
		}
		$output .= $this->staging_mvc_view;
		$this->staging_mvc_view = NULL;
		return $output;
	}
	
	public function viewprocess($data=NULL)
	{
		$ViewFile = ((!empty($data['sourcedir']))?$data['sourcedir']:$this->_CurSystemProgramDir.'/view/').((!empty($data['file']))?$data['file']:'');
		$ViewData = (isset($data['data']))?$data['data']:NULL;
		$Grab = (isset($data['grab']))?$data['grab']:false;
		unset($data);
		if(file_exists($ViewFile))
		{
			(!empty($ViewData))?extract($ViewData):NULL;
			@ob_start();
			include($ViewFile);
			$output = ob_get_contents();
			@ob_end_clean();
			if($Grab===false)
			{
				$this->staging_mvc_view .= $output;
			}
			else
			{
				return $output;
			}
		}
	}
}
?>