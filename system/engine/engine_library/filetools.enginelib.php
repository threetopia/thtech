<?php
/*
 * File name		: filetools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains filetools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class FileTools_EngineLibrary
{
	public function upload($UploadData=array(),$mkdir=false)
	{
		$status = array();
		$NewFileName = '';
		if(!empty($_FILES) and is_array($_FILES))
		{
			foreach($_FILES as $key=>$val)
			{
				//$UploadData['overwrite'] = (!empty($UploadData['overwrite']))?true:false;
				//$UploadData['overwrite'][$key] = (!empty($UploadData['overwrite'][$key]))?true:false;
				$CountFiles[$key] = count($val['name']);
				if(is_array($val['name']) and !empty($_FILES[$key]["tmp_name"]))
				{
					for($a=0;$a<$CountFiles[$key];$a++)
					{
						$NewFileName[$key][$a] = (((!empty($UploadData['filename'][$a]) and is_array($UploadData['filename'])) or (!empty($UploadData['filename']) and !is_array($UploadData['filename'])) or (!empty($UploadData['filename'][$key][$a]) and is_array($UploadData['filename'][$key])) or (!empty($UploadData['filename'][$key]) and !is_array($UploadData['filename'][$key])))?((!empty($UploadData['filename'][$key][$a]) and is_array($UploadData['filename'][$key]))?$UploadData['filename'][$key][$a]:((!empty($UploadData['filename'][$key]) and !is_array($UploadData['filename'][$key]))?$UploadData['filename'][$key]:((is_array($UploadData['filename']))?$UploadData['filename'][$a]:$UploadData['filename']))):$_FILES[$key]["name"][$a]);					
						$NewFilePath[$key][$a] = (((!empty($UploadData['filepath'][$a]) and is_array($UploadData['filepath'])) or (!empty($UploadData['filepath']) and !is_array($UploadData['filepath'])) or (!empty($UploadData['filepath'][$key][$a]) and is_array($UploadData['filepath'][$key])) or (!empty($UploadData['filepath'][$key]) and !is_array($UploadData['filepath'][$key])))?((!empty($UploadData['filepath'][$key][$a]) and is_array($UploadData['filepath'][$key]))?$UploadData['filepath'][$key][$a]:((!empty($UploadData['filepath'][$key]) and !is_array($UploadData['filepath'][$key]))?$UploadData['filepath'][$key]:((is_array($UploadData['filepath']))?$UploadData['filepath'][$a]:$UploadData['filepath']))):SYSTEM_TEMPORARY_DIR.'/');
						$NewFileFullPath[$key][$a] = (((!empty($UploadData['filefullpath'][$a]) and is_array($UploadData['filefullpath'])) or (!empty($UploadData['filefullpath']) and !is_array($UploadData['filefullpath'])) or (!empty($UploadData['filefullpath'][$key][$a]) and is_array($UploadData['filefullpath'][$key])) or (!empty($UploadData['filefullpath'][$key]) and !is_array($UploadData['filefullpath'][$key])))?((!empty($UploadData['filefullpath'][$key][$a]) and is_array($UploadData['filefullpath'][$key]))?$UploadData['filefullpath'][$key][$a]:((!empty($UploadData['filefullpath'][$key]) and !is_array($UploadData['filefullpath'][$key]))?$UploadData['filefullpath'][$key]:((is_array($UploadData['filefullpath']))?$UploadData['filefullpath'][$a]:$UploadData['filefullpath']))):$NewFilePath[$key][$a].$NewFileName[$key][$a]);
						$createpath = $this->createpath($NewFilePath[$key][$a],$mkdir);
						//if(((((!empty($UploadData['overwrite'][$key][$a]))?$UploadData['overwrite'][$key][$a]:((!empty($UploadData['overwrite'][$key]))?$UploadData['overwrite'][$key]:$UploadData['overwrite']))==true)?true:!file_exists($NewFileFullPath[$key][$a])) and ((!empty($UploadData['type'][$key]) and is_array($UploadData['type'][$key][$a]))?in_array($_FILES[$key]["type"][$a],$UploadData['type'][$key][$a]):((!empty($UploadData['type'][$key]) and is_array($UploadData['type'][$key]))?in_array($_FILES[$key]["type"][$a],$UploadData['type'][$key]):((!empty($UploadData['type']) and is_array($UploadData['type']))?in_array($_FILES[$key]["type"][$a],$UploadData['type']):true))) and ((!empty($UploadData['size'][$key][$a]))?($_FILES[$key]["size"][$a] <= $UploadData['size'][$key][$a]):((!empty($UploadData['size'][$key]))?($_FILES[$key]["size"][$a] <= $UploadData['size'][$key]):((!empty($UploadData['size']))?($_FILES[$key]["size"][$a] <= $UploadData['size']):true))) and !empty($_FILES[$key]["tmp_name"][$a]))
						if(file_exists($_FILES[$key]["tmp_name"][$a]) and ((file_exists($NewFileFullPath[$key][$a]) and !empty($UploadData['overwrite']))?((!empty($UploadData['overwrite'][$key][$a]))?$UploadData['overwrite'][$key][$a]:((!empty($UploadData['overwrite'][$key]))?$UploadData['overwrite'][$key]:$UploadData['overwrite'])):!file_exists($NewFileFullPath[$key][$a])))
						{
							//echo $a.').'.$_FILES[$key]["tmp_name"][$a].'<=>'.$NewFileFullPath[$key][$a].'<br>';
							move_uploaded_file($_FILES[$key]["tmp_name"][$a],$NewFileFullPath[$key][$a]);
							$status[$key][$a] = true;
						}
						else
						{
							$status[$key][$a] = false;
						}
						//unset($FileName,$FilePath,$FullPath);
					}
				}
				else if(!is_array($val['name']) and !empty($_FILES[$key]["tmp_name"]))
				{
					$NewFileName[$key] = ((!empty($UploadData['filename'][$key]) and !is_array($UploadData['filename'][$key]))?$UploadData['filename'][$key]:((!empty($UploadData['filename']) and !is_array($UploadData['filename']))?$UploadData['filename']:((!empty($_POST['filename']))?$_POST['filename']:$_FILES[$key]["name"])));
					$NewFilePath[$key] = ((!empty($UploadData['filepath'][$key]) and !is_array($UploadData['filepath'][$key]))?$UploadData['filepath'][$key]:((!empty($UploadData['filepath']) and !is_array($UploadData['filepath']))?$UploadData['filepath']:((!empty($_POST['filepath']))?$_POST['filepath']:SYSTEM_TEMPORARY_DIR.'/')));
					$NewFileFullPath[$key] = ((!empty($UploadData['filefullpath'][$key]) and !is_array($UploadData['filefullpath'][$key]))?$UploadData['filefullpath'][$key]:((!empty($UploadData['filefullpath']) and !is_array($UploadData['filefullpath']))?$UploadData['filefullpath']:((!empty($_POST['filefullpath']))?$_POST['filefullpath']:$NewFilePath[$key].$NewFileName[$key])));
					//if(((((!empty($UploadData['overwrite'][$key]))?$UploadData['overwrite'][$key]:$UploadData['overwrite'])==true)?true:!file_exists($NewFileFullPath[$key])) and ((!empty($UploadData['type'][$key]) and is_array($UploadData['type'][$key]))?in_array($_FILES[$key]["type"],$UploadData['type'][$key]):((!empty($UploadData['type']) and is_array($UploadData['type']))?in_array($_FILES[$key]["type"],$UploadData['type']):true)) and (!empty($UploadData['size'])?($_FILES[$key]["size"] <= $UploadData['size']):true) and !empty($_FILES[$key]["tmp_name"]))
					if(file_exists($_FILES[$key]["tmp_name"]) and ((file_exists($NewFileFullPath[$key]) and !empty($UploadData['overwrite']))?((!empty($UploadData['overwrite'][$key]))?$UploadData['overwrite'][$key]:$UploadData['overwrite']):!file_exists($NewFileFullPath[$key])))
					{
						//echo $key.').'.$_FILES[$key]["tmp_name"].'<=>'.$NewFileFullPath.'<br>';
						move_uploaded_file($_FILES[$key]["tmp_name"],$NewFileFullPath);
						$status[$key] = true;
					}
					else
					{
						$status[$key] = false;
					}
				}
			}
		}
		$DataArray = array('FileName'=>((!empty($NewFileName))?$NewFileName:NULL),'FilePath'=>((!empty($NewFilePath))?$NewFilePath:NULL),'FileFullPath'=>((!empty($NewFileFullPath))?$NewFileFullPath:NULL),'FileCount'=>((!empty($CountFiles))?$CountFiles:NULL),'status'=>$status);
		return $DataArray;
	}

	public function createpath($path='',$recursive=false,$permission=0755)
	{
		if(!empty($path) and !file_exists($path))
		{
			mkdir($path,$permission,$recursive);
		}
		if(file_exists($path))
		{
			return true;
		}
		return false;
	}
	
	public function upload_old($data=NULL)
	{
		$UploadData = $data['UploadData'];
		$status = array();
		$NewFileName = '';
		if(!empty($_FILES) and is_array($_FILES))
		{
			foreach($_FILES as $key=>$val)
			{
				$UploadData['overwrite'][$key] = (!empty($UploadData['overwrite'][$key]))?true:false;
				$UploadData['overwrite'] = (!empty($UploadData['overwrite']))?true:false;
				$CountFiles[$key] = count($val['name']);
				if(is_array($val['name']) and !empty($_FILES[$key]["tmp_name"]))
				{
					for($a=0;$a<$CountFiles[$key];$a++)
					{
						$NewFileName[$key][$a] = (((!empty($UploadData['filename'][$a]) and is_array($UploadData['filename'])) or (!empty($UploadData['filename']) and !is_array($UploadData['filename'])) or (!empty($UploadData['filename'][$key][$a]) and is_array($UploadData['filename'][$key])) or (!empty($UploadData['filename'][$key]) and !is_array($UploadData['filename'][$key])))?((!empty($UploadData['filename'][$key][$a]) and is_array($UploadData['filename'][$key]))?$UploadData['filename'][$key][$a]:((!empty($UploadData['filename'][$key]) and !is_array($UploadData['filename'][$key]))?$UploadData['filename'][$key]:((is_array($UploadData['filename']))?$UploadData['filename'][$a]:$UploadData['filename']))):$_FILES[$key]["name"][$a]);					
						$NewFilePath[$key][$a] = (((!empty($UploadData['filepath'][$a]) and is_array($UploadData['filepath'])) or (!empty($UploadData['filepath']) and !is_array($UploadData['filepath'])) or (!empty($UploadData['filepath'][$key][$a]) and is_array($UploadData['filepath'][$key])) or (!empty($UploadData['filepath'][$key]) and !is_array($UploadData['filepath'][$key])))?((!empty($UploadData['filepath'][$key][$a]) and is_array($UploadData['filepath'][$key]))?$UploadData['filepath'][$key][$a]:((!empty($UploadData['filepath'][$key]) and !is_array($UploadData['filepath'][$key]))?$UploadData['filepath'][$key]:((is_array($UploadData['filepath']))?$UploadData['filepath'][$a]:$UploadData['filepath']))):SYSTEM_TEMPORARY_DIR.'/');
						$NewFileFullPath[$key][$a] = (((!empty($UploadData['filefullpath'][$a]) and is_array($UploadData['filefullpath'])) or (!empty($UploadData['filefullpath']) and !is_array($UploadData['filefullpath'])) or (!empty($UploadData['filefullpath'][$key][$a]) and is_array($UploadData['filefullpath'][$key])) or (!empty($UploadData['filefullpath'][$key]) and !is_array($UploadData['filefullpath'][$key])))?((!empty($UploadData['filefullpath'][$key][$a]) and is_array($UploadData['filefullpath'][$key]))?$UploadData['filefullpath'][$key][$a]:((!empty($UploadData['filefullpath'][$key]) and !is_array($UploadData['filefullpath'][$key]))?$UploadData['filefullpath'][$key]:((is_array($UploadData['filefullpath']))?$UploadData['filefullpath'][$a]:$UploadData['filefullpath']))):$NewFilePath[$key][$a].$NewFileName[$key][$a]);
						if(((((!empty($UploadData['overwrite'][$key][$a]))?$UploadData['overwrite'][$key][$a]:((!empty($UploadData['overwrite'][$key]))?$UploadData['overwrite'][$key]:$UploadData['overwrite']))==true)?true:!file_exists($NewFileFullPath[$key][$a])) and ((!empty($UploadData['type'][$key]) and is_array($UploadData['type'][$key][$a]))?in_array($_FILES[$key]["type"][$a],$UploadData['type'][$key][$a]):((!empty($UploadData['type'][$key]) and is_array($UploadData['type'][$key]))?in_array($_FILES[$key]["type"][$a],$UploadData['type'][$key]):((!empty($UploadData['type']) and is_array($UploadData['type']))?in_array($_FILES[$key]["type"][$a],$UploadData['type']):true))) and ((!empty($UploadData['size'][$key][$a]))?($_FILES[$key]["size"][$a] <= $UploadData['size'][$key][$a]):((!empty($UploadData['size'][$key]))?($_FILES[$key]["size"][$a] <= $UploadData['size'][$key]):((!empty($UploadData['size']))?($_FILES[$key]["size"][$a] <= $UploadData['size']):true))) and !empty($_FILES[$key]["tmp_name"][$a]))
						{
							echo $a.').'.$_FILES[$key]["tmp_name"][$a].'<=>'.$NewFileFullPath[$key][$a].'<br>';
							//move_uploaded_file($_FILES[$key]["tmp_name"][$a],$NewFileFullPath[$a]);
							$status[$key][$a] = true;
						}
						else
						{
							$status[$key][$a] = false;
						}
						//unset($FileName,$FilePath,$FullPath);
					}
				}
				else if(!is_array($val['name']) and !empty($_FILES[$key]["tmp_name"]))
				{
					$NewFileName[$key] = ((!empty($UploadData['filename'][$key]) and !is_array($UploadData['filename'][$key]))?$UploadData['filename'][$key]:((!empty($UploadData['filename']) and !is_array($UploadData['filename']))?$UploadData['filename']:((!empty($_POST['filename']))?$_POST['filename']:$_FILES[$key]["name"])));
					$NewFilePath[$key] = ((!empty($UploadData['filepath'][$key]) and !is_array($UploadData['filepath'][$key]))?$UploadData['filepath'][$key]:((!empty($UploadData['filepath']) and !is_array($UploadData['filepath']))?$UploadData['filepath']:((!empty($_POST['filepath']))?$_POST['filepath']:SYSTEM_TEMPORARY_DIR.'/')));
					$NewFileFullPath[$key] = ((!empty($UploadData['filefullpath'][$key]) and !is_array($UploadData['filefullpath'][$key]))?$UploadData['filefullpath'][$key]:((!empty($UploadData['filefullpath']) and !is_array($UploadData['filefullpath']))?$UploadData['filefullpath']:((!empty($_POST['filefullpath']))?$_POST['filefullpath']:$NewFilePath[$key].$NewFileName[$key])));
					if(((((!empty($UploadData['overwrite'][$key]))?$UploadData['overwrite'][$key]:$UploadData['overwrite'])==true)?true:!file_exists($NewFileFullPath[$key])) and ((!empty($UploadData['type'][$key]) and is_array($UploadData['type'][$key]))?in_array($_FILES[$key]["type"],$UploadData['type'][$key]):((!empty($UploadData['type']) and is_array($UploadData['type']))?in_array($_FILES[$key]["type"],$UploadData['type']):true)) and (!empty($UploadData['size'])?($_FILES[$key]["size"] <= $UploadData['size']):true) and !empty($_FILES[$key]["tmp_name"]))
					{
						//echo $key.').'.$_FILES[$key]["tmp_name"].'<=>'.$NewFileFullPath.'<br>';
						//move_uploaded_file($_FILES[$key]["tmp_name"],$NewFileFullPath);
						$status[$key] = true;
					}
					else
					{
						$status[$key] = false;
					}
				}
			}
		}
		$DataArray = array('FileName'=>((!empty($NewFileName))?$NewFileName:NULL),'FilePath'=>((!empty($NewFilePath))?$NewFilePath:NULL),'FileFullPath'=>((!empty($NewFileFullPath))?$NewFileFullPath:NULL),'FileCount'=>((!empty($CountFiles))?$CountFiles:NULL),'status'=>$status);
		return $DataArray;
	}

	public function readwrite($content=NULL,$file='',$overwrite=false)
	{
		$file = $this->check_path($file);
		if((($content==NULL and !empty($file) and $overwrite===false) or ($content!=NULL and !empty($file) and $overwrite===false)) and file_exists($file))
		{
			//echo 'masuk';
			//$tempcontent .= $content;
			$content .= file_get_contents($file);
		}
		if(!empty($content) and !empty($file))
		{
			file_put_contents($file, $content);
		}
		return $content;
	}
	
	private function check_path($file='')
	{
		$filedir = substr($file, 0, strrpos($file, '/'));
		if(!file_exists($filedir))
		{
			mkdir($filedir,0775,true);
		}
		return ((file_exists($filedir))?$file:false);
	}
}
?>