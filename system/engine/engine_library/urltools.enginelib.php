<?php
/*
 * File name		: urltools.enginelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 3.5
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 26 Okt 2014
 * File Description	: This file contains inputtools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class URLTools_EngineLibrary extends Systems
{
	public function __construct()
	{
		$this->URLDelimiter = (isset($this->_LoaderConfig['URLTools']['urldelimiter']))?$this->_LoaderConfig['URLTools']['urldelimiter']:'/';
	}
	
	private $URLDelimiter;
	private $URLSubtitute = array('/'=>'+','+'=>'/','-'=>'_','_'=>'-');
	/*
	 * URLRules
	 * How to :
	 * array([urlpattern]=>[overwritepattern]) use <{var}> to make pattern as variable
	 * example
	 * array('/app/<var>/some/path'=>'/home/') when user go to /app/result/some/path it would redirect to home and generate array('var'=>'result)
	 */
	private function url_decode_rules($PATH_INFO=NULL)
	{
		$URLOverwrite = false;
		$URLPath = array_values(array_filter(explode($this->URLDelimiter,$PATH_INFO)));
		$URLConfig = $this->_SysEngine->configtools->getconfig('urltools');
		$URLRules = (!empty($URLConfig['URLRules']))?$URLConfig['URLRules']:array();
		if(!empty($URLRules) and !empty($URLPath))
		{
			foreach($URLRules as $search=>$overwrite)
			{
				preg_match_all('/<(\w+)>/', $search, $matches);
				$search = array_values(array_filter(explode($this->URLDelimiter,$search)));
				$overwrite = array_values(array_filter(explode($this->URLDelimiter,$overwrite)));
				$SearchMatchIntersect = array_intersect($search,$matches[0]);# extract intersction between search and $matches[0]
				if(count($URLPath)>count($search))
				{
					$URLPathDiff = array_diff_assoc(array_diff_key($URLPath,$SearchMatchIntersect),array_diff_key($search,$SearchMatchIntersect));# Colide URLPath SearchMatchIntersect and URLSearchIntersect return empty if there have exact match
				}
				else 
				{
					$URLPathDiff = array_diff_assoc(array_diff_key($search,$SearchMatchIntersect),array_diff_key($URLPath,$SearchMatchIntersect));# Colide URLPath SearchMatchIntersect and URLSearchIntersect return empty if there have exact match
				}
				if(empty($URLPathDiff))
				{
					$kloop = 0;
					foreach($URLPath as $kpath=>$vpath)
					{
						$URLPath[$kpath] = '';
						if(isset($SearchMatchIntersect[$kpath]))
						{
							unset($URLPath[$kpath]);
							$kpath = str_replace(array('<','>'),array('',''),$SearchMatchIntersect[$kpath]);
							$URLPath[$kpath] = $vpath;
							$URLOverwrite = true;
						}
						if(isset($overwrite[$kloop]))
						{
							$URLPath[$kloop] = $overwrite[$kloop];
							$URLOverwrite = true;
						}
						$kloop++;
					}
					break;
				}
			}
		}
		$URLPath = array_filter($URLPath);
		$URLApp = (array_key_exists(0,$URLPath))?$URLPath[0]:NULL;unset($URLPath[0]);
		$URLLink = (array_key_exists(1,$URLPath))?$URLPath[1]:NULL;unset($URLPath[1]);
		if($URLOverwrite===false)
		{
			$URLPath = array_values($URLPath);
		}
		return array('URLApp'=>$URLApp,'URLLink'=>$URLLink,'URLExtPath'=>$URLPath);
	}

	private function PATH_INFO($request_uri='',$script_name='')
	{
		$request_uri = explode('/',str_replace('?'.$_SERVER['QUERY_STRING'],'',substr(strip_tags(html_entity_decode($request_uri)),1)));
		$script_name = explode('/',substr(strip_tags(html_entity_decode($script_name)),1));
		return implode('/',array_diff($request_uri,array_filter(array_intersect($request_uri,$script_name))));
	}
	
	public function url_decode($data=NULL)
	{
		/*
		 * Start URLProtocol
		 */
		$URLProtocol = ((!empty($_SERVER['HTTPS'])?$_SERVER['HTTPS']:NULL)=='on')?'https':'http';
		/*
		 * Start URLDomain
		 */
		$URLDomain = array_reverse(explode('.',$_SERVER['HTTP_HOST']));
		krsort($URLDomain);
		/*
		 * Start URLApp, URLLink, URLExtPath
		 */
		$PATH_INFO = $this->PATH_INFO($_SERVER['REQUEST_URI'],$_SERVER['SCRIPT_NAME']);
		$URLExtension = explode('.',$PATH_INFO);
		$URLRules = $this->url_decode_rules(((array_key_exists(0,$URLExtension))?$URLExtension[0]:NULL));
		/*
		 * Start URLRoot, URLIndex
		 */
		$URLRoot = $_SERVER['SCRIPT_NAME'];
		$URLRoot = explode('/',$URLRoot);
		$URLIndex = str_replace('.php','',end($URLRoot));
		array_splice($URLRoot,0,1);
		array_pop($URLRoot);
		$URLRoot = implode('/',$URLRoot);
		return array('URLProtocol'=>$URLProtocol,'URLDomain'=>$URLDomain,'URLRoot'=>$URLRoot,'URLIndex'=>$URLIndex,'URLApp'=>$URLRules['URLApp'],'URLLink'=>$URLRules['URLLink'],'URLExtPath'=>$URLRules['URLExtPath'],'URLDelimiter'=>$this->URLDelimiter,'URLExtension'=>((array_key_exists(1,$URLExtension))?$URLExtension[1]:NULL),'URLString'=>$_GET);
	}

	public function createanchor($data=NULL)
	{
		$html  = '';
		if(!empty($data))
		{
			$link_title = (array_key_exists('linktitle', $data))?$data['linktitle']:((array_key_exists('title',$data))?$data['title']:'');
			krsort($data);
			$html .= '<a ';
			foreach($data as $key=>$val)
			{
				$html .= ($key=='href')?((is_array($val))?" ".$key.'="'.$this->createurl($val).'"':" ".$key.'="'.$val.'"'):((!in_array($key,array('linktitle')))?" ".$key.'="'.$val.'"':'');
			}
			$html .= '>';
			$html .= $link_title;
			$html .= '</a>';
		}
		return $html;
	}
	
	public function redirect($data=NULL,$refresh=0)
	{
		$html = '';
		$location = $this->createurl($data);
		if(!empty($location))
		{
			$html = header('Refresh: '.$refresh.';url='.html_entity_decode($location).'');
		}
		return $html;
	}
	
	public function createurl($data=NULL)
	{
		$URLData = (!empty($data['URLData']))?$data['URLData']:$data;
		#################################################################################
		$URLProtocol = (array_key_exists('URLProtocol', $URLData))?$URLData['URLProtocol']:$this->_URLDecode['URLProtocol'];
		$URLDomain = (array_key_exists('URLDomain', $URLData))?array_reverse($URLData['URLDomain']):$this->_URLDecode['URLDomain'];
		$URLRoot = (array_key_exists('URLRoot', $URLData))?$URLData['URLRoot']:$this->_URLDecode['URLRoot'];
		$URLApp = (array_key_exists('URLApp', $URLData))?$URLData['URLApp']:((!empty($this->_AppData[0]['uprgm_name']))?$this->_AppData[0]['uprgm_name']:((!empty($this->_URLDecode['URLApp']))?$this->_URLDecode['URLApp']:NULL));
		$URLLink = (array_key_exists('URLLink', $URLData))?$URLData['URLLink']:((!empty($this->_NavData[0]['nav_name']))?$this->_NavData[0]['nav_name']:((!empty($this->_URLDecode['URLLink']))?$this->_URLDecode['URLLink']:NULL));
		$URLExtPath = (array_key_exists('URLExtPath', $URLData))?$URLData['URLExtPath']:((!empty($this->_NavData[0]['nav_extpath']))?$this->_NavData[0]['nav_extpath']:((!empty($this->_URLDecode['URLExtPath']))?$this->_URLDecode['URLExtPath']:NULL));
		$URLString = (array_key_exists('URLString', $URLData))?$URLData['URLString']:((!empty($this->_NavData[0]['nav_string']))?$this->_NavData[0]['nav_string']:((!empty($this->_URLDecode['URLString']))?$this->_URLDecode['URLString']:NULL));
		$URLIndex = (array_key_exists('URLIndex', $URLData))?$URLData['URLIndex']:((!empty($this->_AppData[0]['loader_index']))?$this->_AppData[0]['loader_index']:((!empty($this->_URLDecode['URLIndex']))?$this->_URLDecode['URLIndex']:NULL));
		$URLFragment = (array_key_exists('URLFragment', $URLData))?'#'.$URLData['URLFragment']:(!empty($this->_NavData[0]) and (array_key_exists('nav_fragment',$this->_NavData[0]))?'#'.$this->_NavData[0]['nav_fragment']:((!empty($this->_URLDecode) and array_key_exists('URLFragment',$this->_URLDecode))?'#'.$this->_URLDecode['URLFragment']:NULL));
		$URLExtension = (array_key_exists('URLExtension', $URLData))?$URLData['URLExtension']:((!empty($this->_NavData[0]['URLExtension']))?$this->_NavData[0]['URLExtension']:((!empty($this->_URLDecode['URLExtension']))?$this->_URLDecode['URLExtension']:NULL));
		$html = $URLProtocol.'://';
		if(!empty($URLDomain))
		{
			krsort($URLDomain);
			$URLDomain = (!is_array($URLDomain))?json_decode($URLDomain,true):$URLDomain;
			$html .= implode('.',$URLDomain);
		}
		$html .= ($URLRoot!=NULL)?$this->URLDelimiter.$URLRoot:NULL;
		$html .= ((FRIENDLYURLINDEX===true or !empty($URLData['URLIndex'])) and (!empty($URLIndex)))?$this->URLDelimiter.$URLIndex:NULL;
		$html .= ($URLApp!=NULL)?$this->URLDelimiter.$URLApp:NULL;
		$html .= ($URLLink!=NULL)?$this->URLDelimiter.$URLLink:NULL;
		if(!empty($URLExtPath))
		{
			$URLExtPath = (!is_array($URLExtPath))?json_decode($URLExtPath,true):$URLExtPath;
			$html .= $this->URLDelimiter.str_replace($this->URLDelimiter,$this->URLSubtitute[$this->URLDelimiter],implode($this->URLDelimiter,$URLExtPath));
		}
		$html .= ($URLExtension!=NULL)?'.'.$URLExtension:NULL;
		if(!empty($URLString))
		{
			$loop = 0;
			$URLString = (!is_array($URLString))?json_decode($URLString,true):$URLString;
			$html .= '?'.http_build_query($URLString,'','&amp;');
		}
		$html .= $URLFragment;
		return $html;
	}
}
?>