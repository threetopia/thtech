<?php
/*
 * File name		: configtools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: GPL
 * Create Date		: 11 Mar 2014
 * Modified Date	: 13 Mar 2014
 * File Description	: This file contains configuration tools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class ConfigTools_EngineLibrary extends Systems
{
	public function system_configuration($data=NULL)
	{
		# Begin of PHP statement to overwrite the server configuration
		
		# System definition start here
		define('DEFAULTSESSIONEXPIRED','+5 seconds');//can use strtotimeformat eg. +1 minute,+12 minutes (if int use, in sec time metrics) can be overwrite from loader using 'sessiontimeout' parameter
		define('DEFAULTSYSTEMHASH',hash('sha1','ini kata rahasia'.hash('md5',$_SERVER['REMOTE_ADDR'].'+'.$_SERVER['SERVER_ADDR']).'+'.$_SERVER['SERVER_PORT']));
		
		# URL definition start here
		define('DEFAULTFRIENDLYURLINDEX',true);//set to true if want use a friendly url with {indexfile}.php/app/link/string add to htaccess the exclude index page (set to true if you want using multiple index eg. home.php .etc with friendlyurl set to true)
		
		# Site definition start here
		define('DEFAULTTITLEDELIMITER',' - ');//[Apps Title][TITLESPARATOR][Nav Title]; Can be overwrite through Program or Nav custom head
		
		# Database definition start here
		define('DEFAULTDBPERREQUEST', false);
		define('DBPREFIX', '');
		define('DEFAULTDBTYPE','Oracle');//can be overwrite through DB config
		
		# Default email setting
		define('DEFAULT_EMAIL_HOST','ssl://smtp.gmail.com');//Default SMTP host name
		define('DEFAULT_EMAIL_PORT',465);//Default SMTP host port
		define('DEFAULT_EMAIL_ADDRESS','tri@trihartanto.com');//Default email address
		define('DEFAULT_EMAIL_TITLE','Tri Hartanto');//Default email Title
		define('DEFAULT_EMAIL_USERNAME','tri@trihartanto.com');//Default SMTP user name
		define('DEFAULT_EMAIL_PASSWORD','');//Default SMTP password
		
		# Default TimeZone
		define('DEFAULT_TIMEZONE','Asia/Jakarta');
	}
	
	public $recaptchatools = array('publickey'=>'6LffJ-8SAAAAAGt7JVv5NuvkDR2h_F0cHos0XXri','privatekey'=>'6LffJ-8SAAAAAPuWcYQEQBw1VE6RM9k39mywA_pB');
	
	public function system_attribute($data=NULL)
	{
		//$sysconfig_html_head['title'] 	= array('firsttitle'=>((!empty($GLOBALS['_AppData']))?$GLOBALS['_AppData'][0]['uprgm_title']:'THTech System'),'secondtitle'=>((!empty($GLOBALS['_NavData']))?$GLOBALS['_NavData'][0]['nav_title']:((empty($GLOBALS['_AppData']))?'By trihartanto.com':NULL)),'separator'=>'::');
		$sysconfig_html_head['title'] 	= array(((!empty($this->_NavData[0]))?$this->_NavData[0]['nav_title']:((empty($this->_AppData[0]))?'By trihartanto.com':NULL)),((!empty($this->_AppData[0]))?$this->_AppData[0]['uprgm_title']:'THTech System'));
		$sysconfig_html_head['meta'] 	= array(
											  array('http-equiv'=>'Content-Type','content'=>'text/html; charset=utf-8'),
											  array('name'=>'description','content'=>'THTech System'),
											  array('name'=>'keywords','content'=>'THTech,System'),
											  array('name'=>'generator','content'=>'THTech System ver.5 by trihartanto.com')
											  );
		return array(
					  'sysconfig_html_head'=>$sysconfig_html_head
					  );
	}
	
	public function db_config($data=NULL)
	{
		$_dbUser = 'roots';
		$_dbPasswd = '';
		$_dbHost = 'localhost';
		$_dbName = 'thtech';
		$_dbType = 'MySQL';
		$DBConfig = array('DBUser'=>$_dbUser,'DBPassword'=>$_dbPasswd,'DBHost'=>$_dbHost,'DBName'=>$_dbName,'DBType'=>(!empty($_dbType)?$_dbType:((defined(DEFAULTDBTYPE))?DEFAULTDBTYPE:NULL)));// use this as the prototype
		return $DBConfig;
	}
	
	public function getconfig($key=NULL)
	{
		return (!empty($this->_CurProgramConfig[$key]))?$this->_CurProgramConfig[$key]:((!empty($this->_LoaderConfig[$key]))?$this->_LoaderConfig[$key]:((!empty($this->{$key}))?$this->{$key}:array()));
	}
}
?>