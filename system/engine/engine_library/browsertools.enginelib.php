<?php
class BrowserTools_EngineLibrary extends Systems
{
	private $knownPlatform = array('Linux'=>'linux','Windows'=>'windows|win32','Mac'=>'macintosh|mac os x','Unix'=>'unix');
	private $knownBrowser = array('Firefox'=>'Firefox','Opera'=>'Opera','Navigator'=>'Navigator','Internet Explorer'=>'MSIE','Chrome'=>'chrome|chromium','MAXTHON'=>'MAXTHONE','NetScape'=>'netscape','Safari'=>'safari');
	private $knownOS = array('Ubuntu'=>'ubuntu','XP'=>'xp','Vista'=>'vista','Seven'=>'seven','Macintosh'=>'macintosh|mac os','Oracle'=>'oracle');
	private $info = array();
	
	public function __construct()
	{
		$this->getPlatform($_SERVER['HTTP_USER_AGENT']);
		$this->getOS($_SERVER['HTTP_USER_AGENT']);
		$this->getBrowser($_SERVER['HTTP_USER_AGENT']);
	}
	
	public function getinfo()
	{
		return ($this->info);
	}
	
	private function getPlatform($HTTP_USER_AGENT=NULL)
	{
		foreach($this->knownPlatform as $key=>$val)
		{
			$val = (is_array($val))?implode('|',$val):$val;
			if(preg_match('/'.$val.'/i', $HTTP_USER_AGENT))
			{
				$this->info = array_merge($this->info,array('platform'=>$key));
				break;
			}
		}
	}
	
	private function getBrowser($HTTP_USER_AGENT=NULL)
	{
		foreach($this->knownBrowser as $key=>$val)
		{
			$val = (is_array($val))?implode('|',$val):$val;
			if(preg_match('/'.$val.'(.*)/i', $HTTP_USER_AGENT))
			{
				$this->info = array_merge($this->info,array('browser'=>$key),array('version'=>$this->getVersion($key,'/'.$val.'(.*)/i',$HTTP_USER_AGENT)));
				break;
			}
		}
	}
	
	private function getVersion($browser=NULL,$search=NULL,$HTTP_USER_AGENT=NULL)
	{
        $version = "";
        preg_match_all($search,$HTTP_USER_AGENT,$match);
		$version = explode(' ',$match[0][0]);
		if(!empty($version))
		{
			foreach($version as $val)
			{
				$val = explode('/',$val);
				if(!empty($val[0]) and $val[0]==$browser)
				{
					return $val[1];
				}
			}
		}
	}
	
	private function getOS($HTTP_USER_AGENT=NULL)
	{
		foreach($this->knownOS as $key=>$val)
		{
			$val = (is_array($val))?implode('|',$val):$val;
			if(preg_match('/'.$val.'(.*)/i', $HTTP_USER_AGENT))
			{
				$this->info = array_merge($this->info,array('os'=>$key));
				break;
			}
		}
	}
}
?>