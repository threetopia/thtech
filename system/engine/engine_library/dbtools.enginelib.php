<?php
/*
 * File name		: database.class.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Database
 * Version			: 4
 * License			: GPL
 * Create Date		: 23 February 2011
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains database class to used by the framework and CMS system.
 *
 * For more license information please kindly open and read LICENSE.txt file
 */
class DBTools_EngineLibrary extends Systems
{
	private $DBToolsLibrary = array();
	private function loadDBLibrary($dbtype=NULL)
	{
		$libraryFile = dirname(__FILE__).'/dbtools_library/'.strtolower($dbtype).'.dbtoolslib.php';
		if(!isset($this->DBToolsLibrary[$dbtype]) and file_exists($libraryFile))
		{
			include_once($libraryFile);
			$libraryClass = $dbtype.'_DBToolsLibrary';
			$this->DBToolsLibrary[$dbtype] = new $libraryClass();
		}
		if(isset($this->DBToolsLibrary[$dbtype]))
		{
			return $this->DBToolsLibrary[$dbtype];
		}
		$this->_SysEngine->httptools->errorprocess(array('error'=>'500','message'=>'Can\'t open '.$dbtype.' database library.'));
		return false;
	}
	
	private function getDBData($data=NULL)
	{
		$DBConfig = (isset($data['DBConfig']))?$data['DBConfig']:NULL;
		$DBPerRequest = false;
		/*
		 * Build database source and config
		 */
		if(!empty($DBConfig))
		{
			$DBInstance = '_DBPerRequest';
			$DBSource = $DBInstance;
			$DBConfig = $DBConfig;
			$DBPerRequest = true;
		}
		else if(!empty($this->_CurProgramConfig['DBConfig']))
		{
			$DBInstance = $this->_CurProgramData['uprgm_name'];
			if(isset($this->_SysDBData[$this->_CurProgramData['uprgm_name']]))
			{
				$DBSource = $this->_SysDBData[$DBInstance]['DBSource'];
				$DBConfig = $this->_SysDBData[$DBInstance]['DBConfig'];
			}
			else
			{
				$DBSource = $DBInstance;
				$DBConfig = $this->_CurProgramConfig['DBConfig'];
			}
			$DBPerRequest = (!empty($DBConfig['DBPerRequest']))?$DBConfig['DBPerRequest']:false;
		}
		else
		{
			$DBInstance = '_DBToolsDefaultConnection';
			if(isset($this->_SysDBData[$DBInstance]))
			{
				$DBSource = $this->_SysDBData[$DBInstance]['DBSource'];
				$DBConfig = $this->_SysDBData[$DBInstance]['DBConfig'];
			}
			else
			{
				$DBSource = $DBInstance;
				$DBConfig = (!empty($this->_LoaderConfig['DBConfig']))?$this->_LoaderConfig['DBConfig']:$this->_SysEngine->configtools->db_config(array('SelectConfig'=>'auto'));
			}
		}
		$DBData = array('DBInstance'=>$DBInstance,'DBSource'=>$DBSource,'DBConfig'=>$DBConfig,'DBPerRequest'=>$DBPerRequest);
		/*
		 * Initiate connection (This mean the connection never been made and stored (Per Request,First init))
		 */
		if(!isset($this->_SysDBData[$DBInstance]))
		{
			$DBConnect = ($DBLibrary = $this->loadDBLibrary($DBConfig['DBType']))?$DBLibrary->connect($DBData):false;
			if($DBConnect!=false)
			{
				$DBData['DBSource'] = $DBConnect;
			}
			else
			{
				$this->_SysEngine->httptools->errorprocess(array('error'=>'500','message'=>'Can\'t connect to database.'));
			}
			/*
			 * Then write DBData to GLOBALS variable under their own instance
			 */
			if($DBPerRequest===false)
			{
				$GLOBALS['_SysDBData'][$DBInstance] = $DBData;
			}
		}
		return $DBData;
	}
	
	public function Execute($data=NULL)
	{
		$data['DBData'] = $this->getDBData($data);
		if((isset($data['SQL']['SQLSelect']) or isset($data['SQL']['SQLInsert']) or isset($data['SQL']['SQLUpdate']) or isset($data['SQL']['SQLDelete'])) and is_array($data['SQL']))
		{
		   $BuildSQL = $this->BuildSQL($data);
		   $data['SQL'] = $BuildSQL['SQL'];
		   $data['SQLAction'] = $BuildSQL['SQLAction'];
		}
		if(strtoupper($data['SQLAction']) == 'SELECT')
		{
		   $result = $this->getData($data);
		}
		else if(in_array(strtoupper($data['SQLAction']),array('INSERT','DELETE','UPDATE')))
		{
		   $result = $this->executeData($data);
		}
		if($data['DBData']['DBPerRequest']===true)
		{
			$CloseDB = $this->Close($data);
			unset($data['DBData']['DBSource']);
		}
		if($result===false)
		{
			//$this->_SysEngine->httptools->errorprocess(array('error'=>'500','message'=>'Could not execute to your database.'));
		}
		return array('DBData'=>$data['DBData'],"SQL"=>$BuildSQL['SQL'],'result'=>$result);
	}
	
	private function getData($data=NULL)
	{
		/*
		 * Query SQL to Database library
		 */
	   	if(array_key_exists($data['DBData']['DBConfig']['DBType'],$this->DBToolsLibrary))
		{
			return $this->DBToolsLibrary[$data['DBData']['DBConfig']['DBType']]->getData($data);
		}
		return false;
	}
	
	private function executeData($data=NULL)
	{
	   	/*
		 * Execute SQL Query to Database library
		 */
		if(array_key_exists($data['DBData']['DBConfig']['DBType'],$this->DBToolsLibrary))
		{
			return $this->DBToolsLibrary[$data['DBData']['DBConfig']['DBType']]->executeData($data);
		}
		return false;
	}
	
	public function Close($data=NULL)
	{
		$DBInstance = $data['DBData']['DBInstance'];
		$_SysDBData = (array_key_exists($DBInstance,$this->_SysDBData))?$this->_SysDBData[$DBInstance]:$data['DBData'];
		$DBType = (!empty($_SysDBData['DBConfig']))?$_SysDBData['DBConfig']['DBType']:NULL;
		if(array_key_exists($DBType,$this->DBToolsLibrary))
		{
			$this->DBToolsLibrary[$DBType]->close(array('DBSource'=>$_SysDBData['DBSource']));
			unset($GLOBALS['_SysDBData'][$DBInstance]);
		}
	}
	
	public function BuildSQL($data=NULL)
	{
		$DBData = (!empty($data['DBData']))?$data['DBData']:((!empty($this->_SysDBData['_DBToolsDefaultConnection']))?$this->_SysDBData['_DBToolsDefaultConnection']:NULL);
		$DBInstance = (!empty($DBData['DBInstance']))?$DBData['DBInstance']:NULL;
		$DBType = (!empty($DBData['DBConfig']['DBType']))?$DBData['DBConfig']['DBType']:NULL;
		$SQLAction = strtoupper(str_replace('SQL','',current(array_keys($data['SQL']))));
		$SQL = false;
		if(array_key_exists($DBType,$this->DBToolsLibrary) and method_exists($this->DBToolsLibrary[$DBType],'BuildSQL'))
		{
			$SQL = $this->DBToolsLibrary[$DBType]->BuildSQL($data);
		}
		else
		{
			$SQL = $this->defaultBuildSQL($data);
		}
		return array('SQL'=>$SQL,'SQLAction'=>$SQLAction);
	}
	
	private function defaultBuildSQL($data=NULL,$deep=0,$loop=0)
	{
		$SQL = "";
		if(!empty($data['SQL']))
		{
			foreach($data['SQL'] as $key=>$val)
			{
				if(!empty($val) and method_exists($this,$key))
				{
					$SQL .= $this->$key($val,$deep,$loop);
				}
			}
		}
		if($deep>0)
		{
			$SQL = "(".$SQL.")";
		}
		return $SQL;
	}
	
	private function SQLSelect($SQL=array(),$deep=0,$loop=0)
	{
		$SQLSelect = '';
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLSelect .= ($loop==0)?"SELECT ":",";
					if(!is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAS";
						$val = array($key1=>$val);
					}
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLSelect .= $this->$key($val,$deep,0);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLSelect .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val);
						$val = (!empty($JSONVal))?$val:((!is_numeric($key))?addslashes($val):$val);
						$SQLSelect .= $val;
					}
					$loop++;
				}
			}
		}
		return $SQLSelect;
	}
	
	private function SQLAS($SQL=array(),$deep=0,$loop=0)
	{
		$SQLAS = '';
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLAS .= ($loop==0)?"":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLAS .= $this->$key($val,$deep,$loop);
					}
					else if(is_array($val))
					{
						$deep++;
						$SQLAS .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$SQLAS .= $val;
					}
					$SQLAS .= " AS `".$key."`";
					$loop++;
				}
			}
		}
		return $SQLAS;
	}
	
	private function SQLFrom($SQL=array(),$deep=0,$loop=0)
	{
		$SQLFrom  = '';
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLFrom .= ($loop==0)?" FROM ":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLFrom .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLFrom .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$SQLFrom .= ((is_string($key))?"`".$val."` ".$key."":"".$val."");
					}
					$loop++;
				}
			}
		}
		return $SQLFrom;
	}
	
	private function SQLJoin($SQL=array(),$deep=0,$loop=0)
	{
		$SQLJoin  = '';
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(is_array($val))
				{
					$loop1=0;
					foreach($val as $key1=>$val1)
					{
						if(!empty($val1))
						{
							$loop1++;
							$SQLJoin .= (!empty($val1))?(" ".$key." JOIN "):"";
							if(!is_numeric($key1) and method_exists($this,$key1))
							{
								$SQLJoin .= $this->$key1($val1);
							}
							else if(is_numeric($key1) and is_array($val1))
							{
								$deep++;
								$SQLJoin .= $this->defaultBuildSQL(array('SQL'=>$val1),$deep);
							}	
							else if(is_array($val1))
							{
								foreach($val1 as $key2=>$val2)
								{
									if(!empty($val2))
									{
										if(!is_numeric($key2) and method_exists($this,$key2))
										{
											$SQLJoin .= $key1." ".$this->$key2($val2);
										}
										else if(is_numeric($key2) and is_array($val2))
										{
											$deep++;
											$SQLJoin .= $this->defaultBuildSQL(array('SQL'=>$val2),$deep);
										}
										else
										{
											$SQLJoin .= ((is_string($key2))?"`".$key2."` ":"").$val2."";
										} 
									}
								}
							}				
							else
							{
								$SQLJoin .= ((!is_numeric($key1))?$key1.' ON ':'').$val1;
							}
						}
					}
				}
				else
				{
					$ExplodeVal = explode("'",$val);
					if(empty($ExplodeVal[0]) and !empty($val))
					{
						$val = substr($val,1,-1);
					}
					$JSONVal = json_decode($val,true);
					$val = (!empty($JSONVal))?$val:addslashes($val);
					$SQLJoin .= $val;
				}
				$loop++;
			}
		}
		return $SQLJoin;
	}
	
	private function SQLON($SQL=array(),$deep=0,$loop=0)
	{
		$SQLON = "";
		if(!empty($SQL))
		{
			$inloop=0;
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					if($inloop>0 and !is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAND";
						$val = array($key1=>$val);
					}
					$SQLON .= ($loop==0)?" ON ":"";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLON .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLON .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLON .= ((is_string($key))?"".$key."=":"").$val."";
					}
					$loop++;
					$inloop++;
				}
			}
		}
		return $SQLON;
	}
	
	private function SQLAND($SQL=array(),$deep=0,$loop=0)
	{
		$SQLAND = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLAND .= ($loop==0 or $key==='SQLOR')?"":" AND ";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLAND .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLAND .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLAND .= ((is_string($key))?" ".$key."=":"").$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLAND;
	}
	
	private function SQLOR($SQL=array(),$deep=0,$loop=0)
	{
		$SQLOR = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLOR .= ($loop==0)?"":" OR ";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLOR .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLOR .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLOR .= ((!is_numeric($key))?"".$key."='".$val."'":$val);
					}
					$loop++;
				}
			}
		}
		return $SQLOR;
	}
	
	private function SQLIN($SQL=array(),$deep=0,$loop=0)
	{
		$SQLIN = "";
		if(!empty($SQL))
		{
			$inloop = 0;
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					if($inloop>0 and !is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAND";
						$val = array("SQLIN"=>array($key1=>$val));
					}
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLIN .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLIN .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$SQLIN .= ((!method_exists($this,$key))?$key:"")." IN ";
						$loop1 = 0;
						$SQLIN .= "(";
						foreach($val as $key1=>$val1)
						{
							if(!is_numeric($key1) and method_exists($this,$key1))
							{
								$SQLIN .= $this->$key1($val1,$deep,$loop);
							}
							else if(is_numeric($key1) and is_array($val1))
							{
								$SQLIN .= $this->defaultBuildSQL(array('SQL'=>$val1),$deep);
							}
							else
							{
								$SQLIN .= (($loop1>0)?",":"");
								$ExplodeVal = explode("'",$val1);
								if(empty($ExplodeVal[0]) and !empty($val))
								{
									$val1 = substr($val1,1,-1);
								}
								$JSONVal = json_decode($val1,true);
								$val1 = (!empty($JSONVal))?$val1:addslashes($val1);
								$SQLIN .= "'".$val1."'";
								$loop1++;
							}
						}
						$SQLIN .= ")";
					}
					$loop++;
					$inloop++;
				}
			}
		}
		return $SQLIN;
	}
	
	private function SQLNOTIN($SQL=array(),$deep=0,$loop=0)
	{
		$SQLNOTIN  = "";
		if(!empty($SQL))
		{
			$inloop = 0;
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					if($inloop>0 and !is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAND";
						$val = array("SQLNOTIN"=>array($key1=>$val));
						//echo $key,"|";print_r($val);echo "<br><br>";
					}
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLNOTIN .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLNOTIN .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$SQLNOTIN .= ((!method_exists($this,$key))?$key:"")." NOT IN ";
						$loop1 = 0;
						$SQLNOTIN .= "(";
						foreach($val as $key1=>$val1)
						{
							$SQLNOTIN .= (($loop1>0)?",":"");
							$ExplodeVal = explode("'",$val1);
							if(empty($ExplodeVal[0]) and !empty($val))
							{
								$val1 = substr($val1,1,-1);
							}
							$JSONVal = json_decode($val1,true);
							$val1 = (!empty($JSONVal))?$val1:addslashes($val1);
							$SQLNOTIN .= "'".$val1."'";
							$loop1++;
						}
						$SQLNOTIN .= ")";
					}
					$loop++;
					$inloop++;
				}
			}
		}
		return $SQLNOTIN;
	}

	private function SQLISNOT($SQL=array(),$deep=0,$loop=0)
	{
		$SQLISNOT  = "";
		if(!empty($SQL))
		{
			$inloop = 0;
			foreach($SQL as $key=>$val)
			{
				if($val!='')
				{
					if($inloop>0 and !is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAND";
						$val = array("SQLISNOT"=>array($key1=>$val));
					}
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLISNOT .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLISNOT .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$SQLISNOT .= ((!empty($key))?"".$key."":"")." IS NOT ".$val;
					}
					$loop++;
					$inloop++;
				}
			}
		}
		return $SQLISNOT;
	}
	
	private function SQLIS($SQL=array(),$deep=0,$loop=0)
	{
		$SQLIS = "";
		if(!empty($SQL))
		{
			$inloop = 0;
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					if($inloop>0 and !is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAND";
						$val = array("SQLIS"=>array($key1=>$val));
					}
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLIS .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLIS .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$SQLIS .= ((!empty($key))?"".$key."":"")." IS ".$val;
					}
					$loop++;
					$inloop++;
				}
			}
		}
		return $SQLIS;
	}
	
	private function SQLIF($SQL=array(),$deep=0,$loop=0)
	{
		$SQLIF = "";
		if(!empty($SQL))
		{
			$outloop = 0;
			foreach($SQL as $key0=>$val0)
			{
				if(!is_numeric($key0) and method_exists($this,$key0))
				{
					$SQLIF .= $this->$key0($val0,$deep,$loop);
				}
				else if($outloop>=1)
				{
					$key0 = "SQLAND"; 
					$SQLIF .= $this->$key0(array("SQLIF"=>array($val0)),$deep,$loop);
				}
				else if(!empty($val0))
				{
					$inloop = 0;
					$count = count($val0);
					foreach($val0 as $key1=>$val1)
					{
						$SQLIF .= ($inloop==0)?"IF(":",";
						if(!is_numeric($key1) and method_exists($this,$key1))
						{
							$SQLIF .= $this->$key1($val1,$deep,$loop);
						}
						else if(is_numeric($key1) and is_array($val1))
						{
							$deep++;
							$SQLIF .= $this->defaultBuildSQL(array('SQL'=>$val1),$deep);
						}
						else
						{
							$ExplodeVal1 = explode("'",$val1);
							if(empty($ExplodeVal1[0]) and !empty($val1))
							{
								$val1 = substr($val1,1,-1);
							}
							$JSONVal1 = json_decode($val1,true);
							$val1 = (!empty($JSONVal1))?$val1:addslashes($val1);
							$val1 = ((strpos($val1,'=')===false)?"'".$val1."'":"".$val1."");
							$SQLIF .= ((!is_numeric($key1))?"".$key1."=".$val1."":"".$val1."");
						}
						$SQLIF .= ($inloop+1==$count)?")":"";
						$inloop++;
					}
				}
				$loop++;
				$outloop++;
			}
		}
		return $SQLIF;
	}
		
	private function SQLLIKE($SQL=array(),$deep=0,$loop=0)
	{
		$SQLLIKE  = "";
		if(!empty($SQL))
		{
			$inloop = 0;
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					if($inloop>0 and !is_numeric($key) and !method_exists($this,$key))
					{
						$key1 = $key;
						$key = "SQLAND";
						$val = array("SQLLIKE"=>array($key1=>$val));
					}
					$SQLLIKE .= ((!is_numeric($key) and !method_exists($this,$key))?"".$key." LIKE ":"");
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLLIKE .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLLIKE .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLLIKE .= "'".$val."'";
					} 
					$loop++;
					$inloop++;
				}
			}
		}
		return $SQLLIKE;
	}

	private function SQLBINARY($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLBINARY  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLBINARY .= ($loop==0 and !empty($val))?"":(($key==='SQLOR')?"":" AND ");
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLBINARY .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLBINARY .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLBINARY .= " BINARY ".((!is_numeric($key))?"".$key."='".$val."'":$val);
					}
					$loop++;
				}
			}
		}
		return $SQLBINARY;
	}
	
	private function SQLWhere($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLWhere  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLWhere .= ($loop==0 and !empty($val))?" WHERE ":(($key==='SQLOR')?"":" AND ");
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLWhere .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLWhere .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLWhere .= ((!is_numeric($key))?"".$key."='".$val."'":$val);
					}
					$loop++;
				}
			}
		}
		return $SQLWhere;
	}
	
	private function SQLOPERAND($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLOPERAND = "";
		if(!empty($SQL))
		{
			$inloop = 0;
			foreach($SQL as $key0=>$val0)
			{
				if(!empty($val0))
				{
					foreach($val0 as $key1=>$val1)
					{
						if($inloop>0 and !is_numeric($key1) and !method_exists($this,$key1))
						{
							$key2 = $key0;
							$val1 = array("SQLOPERAND"=>array($key2=>array($key1=>$val1)));
							$key1 = "SQLAND";
						}
						//$SQLOPERAND .= ((!is_numeric($key) and !method_exists($this,$key))?"".$key." LIKE ":"");
						if(!is_numeric($key1) and method_exists($this,$key1))
						{
							$SQLOPERAND .= $this->$key1($val1,$deep,$loop);
						}
						else if(is_numeric($key1) and is_array($val1))
						{
							$deep++;
							$SQLOPERAND .= $this->defaultBuildSQL(array('SQL'=>$val1),$deep);
						}
						else
						{
							$SQLOPERAND .= $key1.' '.$key0.' '.$val1;
						} 
						$loop++;
						$inloop++;
					}
				}
			}
		}
		return $SQLOPERAND;
	}
	
	private function SQLDelete($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLDelete  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLDelete .= ($loop==0 and !empty($val))?" DELETE FROM ":"";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLDelete .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLDelete .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLDelete .= "".$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLDelete;
	}
	
	private function SQLUpdate($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLUpdate = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLUpdate .= ($loop==0 and !empty($val))?" UPDATE ":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLUpdate .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLUpdate .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else 
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLUpdate .= "".$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLUpdate;
	}
	
	private function SQLInsert($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLInsert  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLInsert .= ($loop==0 and !empty($val))?" INSERT INTO ":"";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLInsert .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLInsert .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLInsert .= "".$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLInsert;
	}
	
	private function SQLSet($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLSet  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLSet .= ($loop==0)?" SET ":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLSet .= $this->$key($val,$deep,$loop);
					}
					else if(is_array($val) and method_exists($this,end(array_keys($val))))
					{
						$SQLSet .= ((!is_numeric($key))?"".$key."=":"");
						$key = end(array_keys($val));
						$val = end(array_values($val));
						$SQLSet .= $this->$key($val,$deep,$loop);
					}
					else if(is_array($val) and method_exists($this,key(array_keys($val))))
					{
						$key = key(array_keys($val));
						$SQLSet .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLSet .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLSet .= ((!is_numeric($key))?"".$key."=":"").(($val!==NULL)?"'".$val."'":'NULL');
					}
					$loop++;
				}
			}
		}
		return $SQLSet;
	}
	
	private function SQLValues($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLValues  = "";
		if(!empty($SQL))
		{
			$countColumn = count($SQL);
			$countValue = ((count($SQL,COUNT_RECURSIVE))-($countColumn))/$countColumn;
			$SQLColumn = "";
			$SQLValue = "";
			for($loop=0;$loop<$countValue;$loop++)
			{
				$columnloop = 0;
				$SQLValue .= ($loop==0)?"":",";
				foreach($SQL as $key=>$val)
				{
					$columnloop++;
					if($loop==0)
					{
						$SQLColumn .= ($columnloop==1)?"":",";
						$SQLColumn .= $key;
					}
					$SQLValue .= ($columnloop==1)?"(":",";
					$ExplodeVal = explode("'",$val[$loop]);
					if(empty($ExplodeVal[0]) and isset($val[$loop]) and !is_numeric($val[$loop]))
					{
						$val[$loop] = substr($val[$loop],1,-1);
					}
					$JSONVal = json_decode($val[$loop],true);
					$val[$loop] = (!empty($JSONVal))?$val[$loop]:addslashes($val[$loop]);
					//$SQLValue .= ($val[$loop]!==NULL)?((is_numeric($val[$loop]) and !empty($ExplodeVal[0]))?"".$val[$loop]."":"'".$val[$loop]."'"):'NULL';
					$SQLValue .= ($val[$loop]!==NULL)?"'".$val[$loop]."'":'NULL';
					$SQLValue .= ($columnloop==$countColumn)?")":"";
				}
			}
			$SQLValues .= " (";
			$SQLValues .= $SQLColumn;
			$SQLValues .= ")";
			$SQLValues .= " VALUES ";
			$SQLValues .=  $SQLValue;
		}
		return $SQLValues;
	}
	
	private function SQLGroup($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLGroup  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLGroup .= ($loop==0 and !empty($val))?" GROUP BY ":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLGroup .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLGroup .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLGroup .= "".$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLGroup;
	}
	
	private function SQLOrder($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLOrder  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLOrder .= ($loop==0 and !empty($val))?" ORDER BY ":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLOrder .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLOrder .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLOrder .= ((!is_numeric($key))?"".$key." ":"")."".$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLOrder;
	}
	
	private function SQLHaving($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLHaving  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLHaving .= ($loop==0 and !empty($val))?" HAVING ":" AND ";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLHaving .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLHaving .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLHaving .= ((!is_numeric($key))?"".$key."='".$val."'":$val);
					}
					$loop++;
				}
			}
		}
		return $SQLHaving;
	}
	
	private function SQLLimit($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLLimit  = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if(!empty($val))
				{
					$SQLLimit .= ($loop==0 and !empty($val))?" LIMIT ":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLLimit .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLLimit .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLLimit .= ((!empty($key))?"".$key.",":"")."".$val."";
					}
					$loop++;
				}
			}
		}
		return $SQLLimit;
	}
	
	private function SQLCONCAT($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLCONCAT = "";
		if(!empty($SQL))
		{
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLCONCAT .= ($loop==0)?" CONCAT(":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLCONCAT .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLCONCAT .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLCONCAT .= ((is_string($key))?" ".$key."=":"").$val."";
					}
					$loop++;
					$SQLCONCAT .= ($loop==count($SQL))?")":"";
				}
			}
		}
		return $SQLCONCAT;
	}
	
	private function SQLGROUPCONCAT($SQL=NULL,$deep=0,$loop=0)
	{
		$SQLGROUPCONCAT = "";
		if(!empty($SQL))
		{
			$SEPARATOR = (!empty($SQL['SEPARATOR']))?$SQL['SEPARATOR']:"";
			unset($SQL['SEPARATOR']);
			foreach($SQL as $key=>$val)
			{
				if($val!==NULL)
				{
					$SQLGROUPCONCAT .= ($loop==0)?" GROUP_CONCAT(":",";
					if(!is_numeric($key) and method_exists($this,$key))
					{
						$SQLGROUPCONCAT .= $this->$key($val,$deep,$loop);
					}
					else if(is_numeric($key) and is_array($val))
					{
						$deep++;
						$SQLGROUPCONCAT .= $this->defaultBuildSQL(array('SQL'=>$val),$deep);
					}
					else
					{
						$ExplodeVal = explode("'",$val);
						if(empty($ExplodeVal[0]) and !empty($val))
						{
							$val = substr($val,1,-1);
						}
						$JSONVal = json_decode($val,true);
						$val = (!empty($JSONVal))?$val:addslashes($val);
						$SQLGROUPCONCAT .= ((is_string($key))?" ".$key."=":"").$val."";
					}
					$loop++;
					$SQLGROUPCONCAT .= ($loop==count($SQL))?" SEPARATOR '".$SEPARATOR."' )":"";
				}
			}
		}
		return $SQLGROUPCONCAT;
	}
}
?>