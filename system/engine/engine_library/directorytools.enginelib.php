<?php
/*
 * File name		: directorytools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 29 Jan 2014
 * File Description	: This file contains direcotorytools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class DirectoryTools_EngineLibrary
{
	public function scandirectory($ScanDir=NULL,$Exclude=array())
	{
		$ScanDirResult = array();
		$ScanDir = scandir($ScanDir);
		$DefaultExclude = array('.','..');
		$Exclude = array_merge($DefaultExclude,$Exclude);
		if(!empty($ScanDir) and is_array($ScanDir))
		{
			foreach($ScanDir as $key=>$val)
			{
				if(!in_array($val,$Exclude))
				{
					$ScanDirResult[] = $val;
				}
			}
		}
		return $ScanDirResult;
	}
	
	public function scanfiles($ScanFiles=NULL,$Exclude=array())
	{
		$ScanFilesResult = glob($ScanFiles);
		$DefaultExclude = array();
		$Exclude = array_merge($DefaultExclude,$Exclude);
		return $ScanFilesResult;
	}
}
?>