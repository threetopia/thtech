<?php
/*
 * File name		: imagetools.enignelib.php
 * Author			: Jarrod Oberto
 * Site				: http://phpimagemagician.jarrodoberto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 1.0
 * License			: (Creative Commons Attribution 3.0 Unported License) http://creativecommons.org/licenses/by/3.0/deed.en_US
 * Create Date		: 29 Nov 2014
 * Modified Date	: 29 Nov 2014
 * File Description	: This file contains image tools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class GeoIPTools_EngineLibrary extends Systems
{
	public function __construct()
	{
		//require_once(dirname(__FILE__).'/image_magician/php_image_magician.php');
	}
	
	public function image($file='',$param=array())
	{
		$magicianObj = new imageLib($file);
		if(!empty($param))
		{
			foreach($param as $key=>$val)
			{
				if(method_exists($magicianObj,$key))
				{
					call_user_method_array(array($magicianObj,$key), $val);
				}
			}
		}
		return $magicianObj;
	}
}
?>