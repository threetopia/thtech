<?php
/*
 * File name		: sessiontools.enignelib.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Library type		: Engine Library
 * Version			: 2.2
 * License			: GPL
 * Create Date		: 11 Dec 2013
 * Modified Date	: 26 Mar 2014
 * File Description	: This file contains sessiontools class to be used by the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class SessionTools_EngineLibrary extends Systems
{
	public function __construct()
	{
		@session_start();
	}
	
	public function process($data=NULL)
	{
		#################################################################################
		$_AppData = (!empty($GLOBALS['_AppData']))?$GLOBALS['_AppData']:((!empty($data['_AppData']))?$data['_AppData']:array());unset($data['_AppData']);
		#################################################################################
		$_SysSession = array();
		$action = (!empty($data['action']))?$data['action']:'get';unset($data['action']);
		$_sessionexpired = (!empty($data['_sessionexpired']))?$data['_sessionexpired']:NULL;unset($data['_sessionexpired']);
		if(!empty($data))
		{
			foreach($data as $key=>$val)
			{
                if($key=='_SysLogged' and !empty($val))
                {
                	$guest = array_search('guest',$val,true);
					if($guest!==false)
					{
						$_SysSession['guest_logged'] = true;
						unset($val[$guest]);#clear guest since we dont need it in here
					}
					$logged = array_search('logged',$val,true);
					if($logged!==false)
					{
						unset($val[$logged]);
						if(count($val)>1 or $action=='set')
						{
							$val = array_merge($val,array('uprgm','iprgm'));
						}
					}
					if(!empty($val))
					{
						foreach($val as $newkey)
	                    {
	                        $newval[((in_array($newkey, array('uprgm','iprgm')))?$_AppData[0][$newkey.'_name'].'_'.$newkey.'logged':$newkey.'_logged')]=true;
	                    }
	                    $val = (in_array($action,array('unset','get')))?array_keys($newval):$newval;
					} 
                }
                $val['_sessionexpired'] = (isset($val['_sessionexpired']))?$val['_sessionexpired']:$_sessionexpired;
                $val['action'] = (isset($val['action']))?$val['action']:$action;
				$_SysSession = array_merge_recursive($_SysSession,$this->syssession(array('_AppData'=>$_AppData,'key'=>$key,'val'=>$val)));
			}
		}
		return $_SysSession;
	}

	private function sessionexpired_old(&$data=NULL)
	{
		$JSONData = json_decode($data['_sessionexpired'],true);
		$_sessionexpired = (!empty($JSONData))?$JSONData:((!empty($data['val']['_sessionexpired']))?$data['val']['_sessionexpired']:((isset($this->_CurProgramConfig['sessiontools']['sessionexpired']))?$this->_CurProgramConfig['sessiontools']['sessionexpired']:SESSIONEXPIRED));
		$_SessionExpired = (isset($_SESSION[$data['key']]['_SessionExpired']))?json_decode($_SESSION[$data['key']]['_SessionExpired'],true):'';
		$expiredinterval = (!empty($_SessionExpired['expiredinterval']))?$_SessionExpired['expiredinterval']:$_sessionexpired;
        if(!empty($_SessionExpired) and $_SessionExpired['expiredtime'] < time())
        {
            $data['action'] = 'unset';
			$data['_SysSession'] = array();
			//$this->_SysEngine->httptools->errorprocess(array('error'=>'419'));
        }
        else if(!empty($data['_SysSession']) and !empty($_SESSION[$data['key']]))
        {
        	$expiredtime = strtotime($expiredinterval);
          //  $_SESSION[$data['key']]['_SessionExpired'] = json_encode(array('expiredtime'=>$expiredtime,'expiredinterval'=>$expiredinterval));
        }
	}

	private function sessionexpired(&$data=NULL)
	{
		$JSONData = json_decode($data['_sessionexpired'],true);
		$_sessionexpired = (!empty($JSONData))?$JSONData:((!empty($data['val']['_sessionexpired']))?$data['val']['_sessionexpired']:((isset($this->_CurProgramConfig['sessiontools']['sessionexpired']))?$this->_CurProgramConfig['sessiontools']['sessionexpired']:SESSIONEXPIRED));
		$_SessionExpired = (isset($_SESSION[$data['key']]['_SessionExpired']))?json_decode($_SESSION[$data['key']]['_SessionExpired'],true):'';
		$expiredinterval = (!empty($_SessionExpired['expiredinterval']))?$_SessionExpired['expiredinterval']:$_sessionexpired;
       	$expiredtime = strtotime($expiredinterval);
		$ExpiredData = json_encode(array('expiredtime'=>$expiredtime,'expiredinterval'=>$expiredinterval));
	    if(!empty($_SessionExpired) and $_SessionExpired['expiredtime'] < time())
        {
            $data['action'] = 'unset';
			$data['_SysSession'] = array();
			$this->_SysEngine->httptools->errorprocess(array('error'=>'419'));
        }
        else if((!empty($data['_SysSession']) and !empty($_SESSION[$data['key']])) or $data['action']=='set')
        {
            $_SESSION[$data['key']]['_SessionExpired'] = $ExpiredData;
        }
	}
	
	private function syssession($data=NULL)
	{
		#################################################################################
		$_AppData = $data['_AppData'];unset($data['_AppData']);
		#################################################################################
		$SessionData = array();
		if(!empty($data))
		{
		    $data['action'] = $data['val']['action'];
			$data['_sessionexpired'] = $data['val']['_sessionexpired'];
            unset($data['val']['_sessionexpired'],$data['val']['action']);
			$_SysSession = (isset($_SESSION[$data['key']]))?$_SESSION[$data['key']]:$data['val'];
			$data['_SysSession'] = (!empty($data['val']))?$data['val']:array_keys($_SysSession);
            $this->sessionexpired($data);
			if($data['action']==='set')
			{
				if(!empty($data['_SysSession']))
				{
					foreach($data['_SysSession'] as $key=>$val)
					{
						$_SESSION[$data['key']][$key] = $val;
					}
                    $SessionData = $_SESSION[$data['key']];
				}
			}
			else if($data['action']==='get')
			{
				if(!empty($data['_SysSession']))
				{
					foreach($data['_SysSession'] as $key=>$val)
					{
						if(array_key_exists($data['key'],$_SESSION) and array_key_exists($val,$_SESSION[$data['key']]) and $val!='_SessionExpired')
						{
							$SessionData[$data['key']][$val] = $_SESSION[$data['key']][$val];
						}
					}
				}
			}
			else if($data['action']==='unset')
			{
				if(array_key_exists($data['key'], $_SESSION) and !empty($data['_SysSession']))
				{
					foreach($data['_SysSession'] as $key=>$val)
					{
						if(array_key_exists($val, $_SESSION[$data['key']]))
						{
							unset($_SESSION[$data['key']][$val]);
						}
					}
				}
				else if(array_key_exists($data['key'], $_SESSION))
				{
					unset($_SESSION[$data['key']]);
				}
			}
		}
		return $SessionData;
	}
}
?>