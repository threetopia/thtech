<?php
/* بسم الله الرحمن الرحيم
 * File name		: engine.class.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Engine type		: Program type 
 * Version			: 6
 * License			: GPL
 * Create Date		: 23 February 2011
 * Modified Date	: 04 Apr 2014
 * File Description	: This file contains engine class to process the framework and CMS system.
 * 
 * For more license information please kindly open and read LICENSE.txt file
 */
class System_Engine_Class
{
	public function __get($libraryName=NULL)# use to call any available object within SysEngine library
	{
		return $this->loadSysEngineLibrary($libraryName,NULL);
	}
	/*
	 * Use to load any available SysEngine library class
	 * How to :
	 * [class name]->[object or method]([arguments when used on method])
	 * Example : libraryclass->method($data); libraryclass->getobject;
	 */
	private $SysEngineLibrary = array();
	private function loadSysEngineLibrary($libraryName=NULL,$libraryArguments=NULL)
	{
		$libraryFile = dirname(__FILE__).'/../engine_library/'.$libraryName.'.enginelib.php';
		$libraryClass = ucfirst($libraryName).'_EngineLibrary';
		if(!isset($this->SysEngineLibrary[$libraryClass]) and file_exists($libraryFile))
		{
			include_once($libraryFile);
			if(class_exists($libraryClass))
			{
				$this->SysEngineLibrary[$libraryClass] = new $libraryClass();
			}
		}
		if(array_key_exists($libraryClass,$this->SysEngineLibrary))
		{
			return $this->SysEngineLibrary[$libraryClass];
		}
		return false;
	}
	
	/*
	 * Set RenderData for load_program
	 */
	private $RenderData = array();
	
	/*
	 * Set autoload
	 */
	public $static_loaded_class = array();
	public $dynamic_loaded_class = array();
	
	/*
	 * Loader custom head
	 */
	public $staging_html_head = array();
	
	/*
	 * mainprocess method
	 * @params (array)$data()
	 */
	public function mainprocess($data=NULL)
	{
		####################### Load and store loader configuration #########################
		$this->configtools->system_configuration();
		$GLOBALS['_LoaderConfig'] = (!empty($data['loader_configuration']))?$data['loader_configuration']:array();
		$GLOBALS['_LoaderData'] = $data;
		####################### Load _URLDecode pattern used for CMS (This should be loaded first) #######################
		if(empty($_URLDecode))
		{
			$_URLDecode = $this->urltools->url_decode();//decode the url pattern for $URLString, $URLApp , and $URLLink
			$GLOBALS['_URLDecode'] = $_URLDecode;
		}
		###################################################################################
		#########################################################################
		//Init system running statement
		define('THTECH','system is ready');
		//Load ROOT first
		define('ROOT',$_URLDecode['URLRoot']);
		define('URL_PROTOCOL',$_URLDecode['URLProtocol']);
		define('DOMAIN',$_SERVER['HTTP_HOST']);
		define('URL_DOMAIN',URL_PROTOCOL.'://'.DOMAIN);
		//Begin of HTML defined links
		define('URL_ROOT_DIR',URL_DOMAIN.((ROOT!='')?'/'.ROOT:''));
		define('URL_PROGRAM_DIR',URL_ROOT_DIR.'/program');
		define('URL_APPS_DIR',URL_ROOT_DIR.'/program/apps');
		define('URL_COMPONENTS_DIR',URL_ROOT_DIR.'/program/components');
		define('URL_MODULES_DIR',URL_ROOT_DIR.'/program/modules');
		define('URL_TEMPLATES_DIR',URL_ROOT_DIR.'/program/templates');
		define('URL_TEMPORARY_DIR',URL_ROOT_DIR.'/temp');
		define('URL_LIBRARY_DIR',URL_ROOT_DIR.'/library');
		//Begin of systems defined links
		define('SYSTEM_ROOT_DIR',str_replace('\\','/',realpath('')));
		define('SYSTEM_PROGRAM_DIR',SYSTEM_ROOT_DIR.'/program');
		define('SYSTEM_APPS_DIR',SYSTEM_ROOT_DIR.'/program/apps');
		define('SYSTEM_COMPONENTS_DIR',SYSTEM_ROOT_DIR.'/program/components');
		define('SYSTEM_MODULES_DIR',SYSTEM_ROOT_DIR.'/program/modules');
		define('SYSTEM_TEMPLATES_DIR',SYSTEM_ROOT_DIR.'/program/templates');
		define('SYSTEM_TEMPORARY_DIR',SYSTEM_ROOT_DIR.'/temp');
		define('SYSTEM_LIBRARY_DIR',SYSTEM_ROOT_DIR.'/library');
		#########################################################################
		########################### Init autoload #####################
		if(!empty($GLOBALS['_LoaderConfig']['autoload']))
		{
			$loaddata = array('sourcedir'=>SYSTEM_ROOT_DIR,'autoload'=>$GLOBALS['_LoaderConfig']['autoload'],'loaderstate'=>'static');
			$this->class_loader($loaddata);
			unset($GLOBALS['_LoaderConfig']['autoload']);
		}
		############################ Loader Definitier ###########################
		//define('LOADER_NAME',$data['loader_name']);
		//define('LOADER_INDEX',$_URLDecode['URLIndex']);
		define('SESSIONEXPIRED',(isset($data['_SessionExpired']))?$data['_SessionExpired']:DEFAULTSESSIONEXPIRED);
		define('SYSTEMHASH',sha1(((isset($data['systemhash']))?$data['systemhash']:DEFAULTSYSTEMHASH).THTECH));
		define('FRIENDLYURLINDEX',((isset($data['friendlyurlindex']))?$data['friendlyurlindex']:DEFAULTFRIENDLYURLINDEX));
		###################### Set Current Default System TimeZone ########################
		$this->datetools->settimezone();
		###################################################################################
		#################### Get _ProgramData used to load all program ####################
		$_GetProgramData = $this->engine_get_program();
		###################################################################################
		################################ Load programdata #################################
		$_LoadProgramData = $this->load_program();
		###################################################################################
		######################### clean up all static autoload ######################
		if(!empty($this->static_loaded_class))
		{
			$this->static_loaded_class = array();
		}
		######################################################################
		####################### Close default database connection #########################
		if(isset($GLOBALS['_SysDBData']['_DBToolsDefaultConnection']))
		{
			$this->dbtools->close(array('DBData'=>array('DBInstance'=>'_DBToolsDefaultConnection')));
		}
		###################################################################################
		##################### Render template and HTTPError handler #######################
		$output = ($this->httptools->iserror!==false)?$this->httptools->errorrender():$this->render(array('program_type'=>'template','position'=>'template'));
		print($output);
		###################################################################################
	}
	
	private function load_program($data=NULL)
	{
		$_SysEngine = $this;
		################# Get _AppData used for program location ################
		$_AppData = $GLOBALS['_AppData'];
		$_ProgramData = $GLOBALS['_ProgramData'];
		$_TemplateData = $GLOBALS['_TemplateData'];
		$_URLDecode = $GLOBALS['_URLDecode'];
		#################################################################################
		################# Get _NavData used for program location ################
		$_NavData = $GLOBALS['_NavData'];
		######################################################################
		################################ Load programdata #################################
		if(!empty($_ProgramData) and !empty($_AppData[0]) and !empty($_TemplateData[0]))
		{
			foreach($_ProgramData as $_CurProgramKey=>$_CurProgramData)
			{
				$_CurSystemProgramDir = $_CurProgramData['_SystemProgramDir'];
				$_CurURLProgramDir = $_CurProgramData['_URLProgramDir'];
				if(file_exists($_CurSystemProgramDir."/index.php"))
				{
					$GLOBALS['_CurSystemProgramDir'] = $_CurSystemProgramDir;
					$GLOBALS['_CurURLProgramDir'] = $_CurURLProgramDir;
					###################################################################
					# Load configuration data
					###################################################################
					$_CurProgramConfig = array_merge_recursive(((!empty($_CurProgramData['iprgm_configuration']))?$_CurProgramData['iprgm_configuration']:array()),((!empty($_CurProgramData['uprgm_configuration']))?$_CurProgramData['uprgm_configuration']:array()),((!empty($_NavData[0]['nav_configuration']) and $_CurProgramData['iprgm_type']=='app')?$_NavData[0]['nav_configuration']:array()));
					if(!empty($_CurProgramConfig['autoload']))
					{
						$this->class_loader(array('sourcedir'=>$_CurSystemProgramDir,'autoload'=>$_CurProgramConfig['autoload'],'loaderstate'=>'dynamic'));
					}
					$_CurProgramData['_ProgramConfig'] = $_CurProgramConfig;
					$GLOBALS['_CurProgramConfig'] = $_CurProgramConfig;
					$GLOBALS['_CurProgramData'] = $_CurProgramData;
					@ob_start();
					include($_CurSystemProgramDir."/index.php");
					$output = ob_get_contents();
					@ob_end_clean();
					################# Render template ################
					if($_CurProgramData['iprgm_type']!='template')
					{
						$template = $this->template_path();
						if(!empty($template['templatefile']))
						{
							$template_attribute  = '';
							$tmpl_attribute = (!empty($_NavData[0]['nav_template_attribute']) and $_CurProgramData['iprgm_type']=='app')?$_NavData[0]['nav_template_attribute']:((!empty($_CurProgramData['uprgm_template_attribute']))?$_CurProgramData['uprgm_template_attribute']:((!empty($_CurProgramData['iprgm_template_attribute']))?$_CurProgramData['iprgm_template_attribute']:((!empty($GLOBALS['_LoaderConfig']['loader_template_attribute']))?$GLOBALS['_LoaderConfig']['loader_template_attribute']:'')));
							if(!empty($tmpl_attribute))
							{
								$loop=0;
								foreach($tmpl_attribute as $key=>$val)
								{
									$template_attribute .= (($loop==0)?'':' ').$key.'="'.$val.'"';
									$loop++;
								}
							}
							@ob_start();
							include($template['templatefile']);
							$output_tmpl = ob_get_contents();
							@ob_end_clean();
							$output = (!empty($output_tmpl))?$output_tmpl:$output;
						}
					}
					######################################################################
					$_CurProgramData['uprgm_position'] = ($_CurProgramData['uprgm_position']=='component')?'app':$_CurProgramData['uprgm_position'];
					$_CurProgramData['iprgm_type'] = ($_CurProgramData['iprgm_type']=='component')?'app':$_CurProgramData['iprgm_type'];
					if(!empty($output) and $_CurProgramData['iprgm_type']!='app')//modules
					{
						(!isset($this->RenderData[$_CurProgramData['iprgm_type']][$_CurProgramData['uprgm_position']][$_CurProgramData['uprgm_order']]))?$this->RenderData[$_CurProgramData['iprgm_type']][$_CurProgramData['uprgm_position']][$_CurProgramData['uprgm_order']]='':NULL;
						$this->RenderData[$_CurProgramData['iprgm_type']][$_CurProgramData['uprgm_position']][$_CurProgramData['uprgm_order']] .= $output;
					}
					else if(!empty($output) and $_CurProgramData['iprgm_type']=='app')//apps, components
					{
						$this->RenderData[$_CurProgramData['iprgm_type']][$_CurProgramData['uprgm_position']][$_CurProgramData['uprgm_order']] = $output;
					}
					######################################################################
					######################### clean up all dynamic autoload ######################
					if(!empty($this->dynamic_loaded_class))
					{
						$this->dynamic_loaded_class = array();
					}
					######################################################################
					if(isset($GLOBALS['_SysDBData'][$_CurProgramData['uprgm_name']]))
					{
						$this->dbtools->close(array('DBData'=>array('DBInstance'=>$_CurProgramData['uprgm_name'])));
					}
					$GLOBALS['_CurProgramData'] = array();
					$GLOBALS['_CurProgramConfig'] = array();
					$GLOBALS['_CurSystemProgramDir'] = array();
					$GLOBALS['_CurURLProgramDir'] = array();
				}
			}
			unset($GLOBALS['_CurProgramData'],$GLOBALS['_CurProgramData'],$GLOBALS['_CurProgramConfig'],$GLOBALS['_CurURLProgramDir']);
		}
		###################################################################################
		return $this->RenderData;
	}
	
	/*
	 * Class Loader
	 * How to :
	 * array('sourcedir'=>(source directory),'file'=>(filename.ext | directory/filename.ext),'class'=>(class name),'alias'=>(alias for class name),'loaderstate'=>(static,dynamic,GLOBALS));
	 * key are used if setted even if the key value is NULL or empty, if you don't want to use it (use system default) just leave it or not set the key
	 */
	public function class_loader($data=NULL)
	{
		if(isset($data['autoload']))
		{
			foreach($data['autoload'] as $key=>$val)
			{
				$sourcedir = (!empty($val['sourcedir']))?$val['sourcedir']:((!empty($data['sourcedir']))?$data['sourcedir']:((!empty($GLOBALS['_CurSystemProgramDir']))?$GLOBALS['_CurSystemProgramDir']:SYSTEM_ROOT_DIR));
				$FileDir = (strstr($val['file'],'/',true)==="")?$val['file']:str_replace('\\','/',$sourcedir.'/'.$val['file']);
				$LoaderState = (!empty($val['loaderstate']))?$val['loaderstate']:((!empty($data['loaderstate']))?$data['loaderstate']:'GLOBALS');
				if(strpos($val['file'],'*')===false and file_exists($FileDir))
				{
					include_once($FileDir);
					$Class = ((empty($val['class'])))?basename($FileDir, ".php"):$val['class'];
					$ClassAlias = (!empty($val['alias']))?$val['alias']:$Class;
					if(class_exists($Class) and !array_key_exists($ClassAlias, $this->static_loaded_class) and !array_key_exists($ClassAlias, $this->dynamic_loaded_class) and !array_key_exists($ClassAlias, $GLOBALS))
					{
						$val['data'] = (isset($val['data']) and !empty($val['data']))?$val['data']:NULL;
						${$ClassAlias} = new $Class($val['data']);
						if($LoaderState == 'GLOBALS')#deprecated move to static load instead
						{
							$GLOBALS[$ClassAlias] = ${$ClassAlias};
						}
						else if($LoaderState == 'static')
						{
							$this->static_loaded_class[$ClassAlias] = ${$ClassAlias};
						}
						else if($LoaderState == 'dynamic')
						{
							$this->dynamic_loaded_class[$ClassAlias] = ${$ClassAlias};
						}
					}
				}
				else if(strpos($val['file'],'*')!==false)
				{
					foreach(glob($FileDir) as $filekey=>$filename)
					{
						$filename = str_replace($sourcedir.'/','',$filename);
						$autoload[] = array('sourcedir'=>((!empty($val['sourcedir']))?$val['sourcedir']:NULL),'file'=>$filename,'class'=>((!empty($val['class']))?$val['class']:NULL),'alias'=>((!empty($val['alias']))?$val['alias']:NULL),'loaderstate'=>((!empty($val['loaderstate']))?$val['loaderstate']:NULL));
						$this->class_loader(array('sourcedir'=>((array_key_exists('sourcedir',$data))?$data['sourcedir']:''),'autoload'=>$autoload,'loaderstate'=>$LoaderState));
					}
				}
			}
		}
		return true;
	}
	
	public function render($data=NULL)
	{
		##################### Get and initiate RenderData ####################
		$RenderData = $this->RenderData;
		######################################################################
		################# Render the html head first!!! THIS IS IMPORTANT ################
		$output = '';
		if($data['program_type']=='html_head')
		{
			$output .= $this->render_html_head();
		} 
		######################################################################
		else if(!empty($RenderData[$data['program_type']][$data['position']]))
		{
			ksort($RenderData[$data['program_type']][$data['position']]);
			foreach($RenderData[$data['program_type']][$data['position']] as $key=>$val)
			{
				$output .= $val;
			}
		}
		return $output;
	}
	
	private function engine_get_program($data=NULL)
	{
		$_AppData = array();
		$_NavData = array();
		$_ProgramData = array();
		$_TemplateData = array();
		################# Get App Data ################
		$_AppData = $this->get_app_sysdata();
		$GLOBALS['_AppData'] = $_AppData;
		#################################################
		################# Get App Nav Data ################
		if(!empty($_AppData))
		{
			$_NavData = $this->get_nav_sysdata();
		}
		$GLOBALS['_NavData'] = $_NavData;
		#################################################
		################# Process Logged ################
		if(!empty($_AppData))
		{
			$RequireLogin = (!empty($_NavData[0]['nav_require_login']))?$_NavData[0]['nav_require_login']:((!empty($_AppData[0]['uprgm_require_login']))?$_AppData[0]['uprgm_require_login']:array());
			$_SysLogged = $this->sessiontools->process(array('_AppData'=>array($_AppData[0]),'action'=>'get','_SysLogged'=>$RequireLogin));
			if(!in_array(true,$_SysLogged))
			{
				$this->httptools->errorprocess(array('error'=>'401'));
				$GLOBALS['_AppData'] = array();
				$GLOBALS['_NavData'] = array();
			}
		}
		else
		{
			$this->httptools->errorprocess(array('error'=>'404'));
		}
		#################################################
		################# Get Program Data ################
		if(!empty($_AppData))
		{
			$_ProgramData = $this->get_program_sysdata();
		}
		$GLOBALS['_ProgramData'] = $_ProgramData;
		#################################################
		$_TemplateData = end($_ProgramData);
		if((!empty($_TemplateData) and $_TemplateData['iprgm_type']!='template') or empty($_TemplateData))
		{
			$this->httptools->errorprocess(array('error'=>'501','message'=>'Sorry your requested application doesn\'t have any template.'));
			$_TemplateData = array();
		}
		$GLOBALS['_TemplateData'][] = $_TemplateData;
		#################################################
		$_GetProgramData = array(
								  '_ProgramData'=>$_ProgramData,
								  '_NavData'=>$_NavData,
								  '_AppData'=>$_AppData,
								  '_TemplateData'=>$_TemplateData
								 );
		
		return $_GetProgramData;		
	}
	
	private function get_app_sysdata($data=NULL)
	{
		$loop=0;
		$AppsUprgmData = array();
		$ArrApps = array();
		if(file_exists(SYSTEM_APPS_DIR))
		{
			$URLIndex = str_replace('.php','',$GLOBALS['_URLDecode']['URLIndex']);
			$ScanAppsIprgmDir = $this->directorytools->scandirectory(SYSTEM_APPS_DIR);
			if(!empty($ScanAppsIprgmDir))
			{
				foreach($ScanAppsIprgmDir as $key=>$val)
				{
					$AppsIprgmDir = SYSTEM_APPS_DIR.'/'.$val.'/_iprgm';
					$AppsIprgmFile = $AppsIprgmDir.'/'.$val.'.php';
					if(file_exists($AppsIprgmFile) and !in_array($AppsIprgmDir,$ArrApps))
					{
						$sysdata = array();
						include_once($AppsIprgmFile);
						if(!empty($sysdata) and $sysdata['iprgm_status']===true and $val==$sysdata['iprgm_name'])
						{
							$AppsIprgmData = $sysdata;
							$AppsUprgmDir = $AppsIprgmDir.'/_uprgm';
							$ScanAppsUprgmFiles = $this->directorytools->scanfiles($AppsUprgmDir.'/'.((!empty($GLOBALS['_URLDecode']['URLApp']))?$GLOBALS['_URLDecode']['URLApp']:((file_exists($AppsUprgmDir.'/_WildCard.php'))?'_WildCard':'*')).'.php');
							if(!empty($ScanAppsUprgmFiles))
							{
								foreach($ScanAppsUprgmFiles as $AppsUprgmFiles)
								{
									if(file_exists($AppsUprgmFiles))
									{
										$sysdata = array();
										include_once($AppsUprgmFiles);
										$v_uprgm = (!empty($GLOBALS['_URLDecode']['URLApp']))?$GLOBALS['_URLDecode']['URLApp']:(($sysdata['uprgm_default']===true)?$sysdata['uprgm_name']:false);
										$uprgm_domain = '';
										if(!empty($sysdata) and (in_array('*',$sysdata['uprgm_loader']) or in_array($URLIndex,$sysdata['uprgm_loader'])) and in_array(true,array_map(function($var){return fnmatch($var,DOMAIN);},$sysdata['uprgm_domain'])) and $sysdata['uprgm_status']===true and ($v_uprgm==$sysdata['uprgm_name'] or $sysdata['uprgm_name']=='_WildCard'))
										{
											$AppsUprgmData[$loop] = array_merge($AppsIprgmData,$sysdata);
											$AppsUprgmData[$loop]['_SystemProgramDir'] = SYSTEM_PROGRAM_DIR.'/'.$AppsUprgmData[$loop]['iprgm_type']."s/".$AppsUprgmData[$loop]['iprgm_name'];
											$AppsUprgmData[$loop]['_URLProgramDir'] = URL_PROGRAM_DIR.'/'.$AppsUprgmData[$loop]['iprgm_type']."s/".$AppsUprgmData[$loop]['iprgm_name'];
											$loop++;
											break 2;
										}
									}
								}
							}
						}
						$ArrApps[] = $AppsIprgmDir;
					}
				}
				unset($sysdata);
			}
		}
		return $AppsUprgmData;
	}
	
	private function get_nav_sysdata($data=NULL)
	{
		$NavDir = array();
		$NavData = array();
		$NavDir = SYSTEM_APPS_DIR.'/'.$GLOBALS['_AppData'][0]['iprgm_name'].'/_iprgm/_uprgm/_nav/'.$GLOBALS['_AppData'][0]['uprgm_name'];
		if(file_exists($NavDir))
		{
			$NavFiles = (file_exists($NavDir.'/'.$GLOBALS['_URLDecode']['URLLink'].'.php'))?$NavDir.'/'.$GLOBALS['_URLDecode']['URLLink'].'.php':((file_exists($NavDir.'/_WildCard.php') and !empty($GLOBALS['_URLDecode']['URLLink']))?$NavDir.'/_WildCard.php':$NavDir.'/*.php');
			$ScanNavFiles = $this->directorytools->scanfiles($NavFiles);
			if(!empty($ScanNavFiles))
			{
				foreach($ScanNavFiles as $NavFile)
				{
					$sysnavdata = array();
					include_once($NavFile);
					if(!empty($sysnavdata) and ((empty($GLOBALS['_URLDecode']['URLLink']))?$sysnavdata['nav_default']===true:($GLOBALS['_URLDecode']['URLLink']==$sysnavdata['nav_name'] or $sysnavdata['nav_name']=='_WildCard')))
					{
						$NavData[] = $sysnavdata;
						break;
					}
				}
			}
			unset($sysnavdata);
		}
		return $NavData;
	}
	
	private function get_program_sysdata($data=NULL)
	{
		$loop=0;
		$ArrAppComModTem = array();
		$ProgramIprgmData = array();
		$ProgramUprgmData = array();
		if(file_exists(SYSTEM_PROGRAM_DIR) and !empty($GLOBALS['_AppData']))
		{
			$URLIndex = str_replace('.php','',$GLOBALS['_URLDecode']['URLIndex']);
			$ScanDir = array(SYSTEM_COMPONENTS_DIR,SYSTEM_MODULES_DIR,SYSTEM_TEMPLATES_DIR);
			foreach($ScanDir as $ProgramDir)
			{
				if(file_exists($ProgramDir))
				{
					$sysdata = array();
					$ScanAppComModDir = $this->directorytools->scandirectory($ProgramDir);
					foreach($ScanAppComModDir as $key=>$val)
					{
						$ProgramIprgmDir = $ProgramDir.'/'.$val.'/_iprgm';
						$ProgramIprgmFile = $ProgramIprgmDir.'/'.$val.'.php';
						if(file_exists($ProgramIprgmFile) and !in_array($ProgramIprgmDir,$ArrAppComModTem))
						{
							$sysdata = array();
							include_once($ProgramIprgmFile);
							if(!empty($sysdata) and $sysdata['iprgm_status'] === true and $val==$sysdata['iprgm_name'])
							{
								$ProgramIprgmData = $sysdata;
								$ProgramUprgmDir = $ProgramIprgmDir.'/_uprgm';
								$ScanProgramUprgmFiles = $this->directorytools->scanfiles($ProgramUprgmDir.'/'.((file_exists($ProgramUprgmDir.'/_WildCard'))?'_WildCard':'*').'.php');
								if(!empty($ScanProgramUprgmFiles))
								{
									foreach($ScanProgramUprgmFiles as $ProgramUprgmFiles)
									{
										if(file_exists($ProgramUprgmFiles))
										{
											$sysdata = array();
											include_once($ProgramUprgmFiles);
											$RequireLogin = (!empty($sysdata['uprgm_require_login']))?$sysdata['uprgm_require_login']:array();
											$_SysLogged = $this->sessiontools->process(array('action'=>'get','_SysLogged'=>$RequireLogin));
											$uprgm_location = ((!empty($sysdata['uprgm_location']))?in_array(true,array_map(function($var,$key){return (((is_array($var))?in_array(true,array_map(function($var,$key){return (((is_array($var))?in_array(true,array_map(function($var,$key){return fnmatch($var,((!empty($GLOBALS['_NavData'][0]['nav_name']))?$GLOBALS['_NavData'][0]['nav_name']:$GLOBALS['_URLDecode']['URLLink']));},$var,array_keys($var))):fnmatch($var,$GLOBALS['_AppData'][0]['uprgm_name']))and ((!is_numeric($key))?fnmatch($key,$GLOBALS['_AppData'][0]['uprgm_name']):true));},$var,array_keys($var))):fnmatch($var,$GLOBALS['_AppData'][0]['iprgm_name']))and((!is_numeric($key))?fnmatch($key,$GLOBALS['_AppData'][0]['iprgm_name']):true));},$sysdata['uprgm_location'],array_keys($sysdata['uprgm_location']))):false);
											if(!empty($sysdata) and in_array(true,array_map(function($var){return fnmatch($var,$GLOBALS['_URLDecode']['URLIndex']);},$sysdata['uprgm_loader'])) and in_array(true,array_map(function($var){return fnmatch($var,DOMAIN);},$sysdata['uprgm_domain'])) and $sysdata['uprgm_status']===true and in_array(true,$_SysLogged) and $uprgm_location)
											{
												$ProgramUprgmData[$loop] = array_merge($ProgramIprgmData,$sysdata);
												$ProgramUprgmData[$loop]['_SystemProgramDir'] = SYSTEM_PROGRAM_DIR.'/'.$ProgramUprgmData[$loop]['iprgm_type']."s/".$ProgramUprgmData[$loop]['iprgm_name'];
												$ProgramUprgmData[$loop]['_URLProgramDir'] = URL_PROGRAM_DIR.'/'.$ProgramUprgmData[$loop]['iprgm_type']."s/".$ProgramUprgmData[$loop]['iprgm_name'];
												$loop++;
											}
										}
									}
								}
							}
							$ArrAppComModTem[] = $ProgramIprgmDir;
						}
					}
				}
			}
			unset($sysdata);
			$ProgramUprgmData = array_merge($GLOBALS['_AppData'],(!empty($ProgramUprgmData))?$ProgramUprgmData:array());//inject AppData
		}
		return $ProgramUprgmData;
	}
	
	private function render_html_head($data=NULL)
	{
		$TitleDelimiter = NULL;
		$TitleResult = array();
		$MetaResult = array();
		$LinkResult = array();
		$ScriptResult = array();
		$NoScriptResult = array();
		$StyleResult = array();
		$loaded_iprgm = array();
		##########################################################################################################################################
		########################################################  Process from source  ###########################################################
		##########################################################################################################################################
		/*
		 * New Method using looping 
		 */
		$HTMLHeadData[] = array('htmlhead'=>$this->staging_html_head);
		$HTMLHeadData[] = array('htmlhead'=>((isset($GLOBALS['_NavData'][0]['nav_html_head']))?$GLOBALS['_NavData'][0]['nav_html_head']:NULL));
		if(!empty($GLOBALS['_ProgramData']))
		{
			foreach($GLOBALS['_ProgramData'] as $_Program)
			{
				$ProgramDir = '/program/'.$_Program['iprgm_type'].'s/'.$_Program['iprgm_name'];
				if(!in_array($_Program['iprgm_name'],$loaded_iprgm))
				{
					$HTMLHeadData[] = array('htmlhead'=>$_Program['iprgm_html_head'],'urlprogramdir'=>URL_ROOT_DIR.$ProgramDir,'systemprogramdir'=>SYSTEM_ROOT_DIR.$ProgramDir);
					$loaded_iprgm[] = $_Program['iprgm_name'];
				}
				$HTMLHeadData[] = array('htmlhead'=>$_Program['uprgm_html_head'],'urlprogramdir'=>URL_ROOT_DIR.$ProgramDir,'systemprogramdir'=>SYSTEM_ROOT_DIR.$ProgramDir);
			}
		}
		$HTMLHeadData[] = array('htmlhead'=>((isset($GLOBALS['_LoaderData']['loader_html_head']))?$GLOBALS['_LoaderData']['loader_html_head']:NULL));
		$_SysConfig = $this->configtools->system_attribute();
		$HTMLHeadData[] = array('htmlhead'=>$_SysConfig['sysconfig_html_head']);
		foreach($HTMLHeadData as $k_htmlhead=>$v_htmlhead)
		{
			$urlprogramdir = ((!empty($v_htmlhead['urlprogramdir']))?$v_htmlhead['urlprogramdir']:URL_ROOT_DIR.'/');
			$systemprogramdir = ((!empty($v_htmlhead['systemprogramdir']))?$v_htmlhead['systemprogramdir']:SYSTEM_ROOT_DIR.'/');
			$htmlhead = (!empty($v_htmlhead['htmlhead']))?$v_htmlhead['htmlhead']:array();
			/*  Title data start */
			$TitleDelimiter = (!empty($TitleDelimiter))?$TitleDelimiter:((!empty($htmlhead['title']['delimiter']))?((is_array($htmlhead['title']['delimiter']))?reset($htmlhead['title']['delimiter']):$htmlhead['title']['delimiter']):NULL);
			unset($htmlhead['title']['delimiter']);
			$TitleData = (!empty($htmlhead['title']))?array($htmlhead['title']):NULL;
			if(!empty($TitleData))
			{
				$TitleResult = $this->render_html_title(array('TitleDelimiter'=>$TitleDelimiter,'TitleData'=>$TitleData,'TitleResult'=>$TitleResult));
			}
			/* Title data end */
			
			/* Meta data start */
			$MetaData = (!empty($htmlhead['meta']))?$htmlhead['meta']:NULL;
			if(!empty($MetaData))
			{
				$MetaResult = $this->render_html_meta(array('MetaData'=>$MetaData,'MetaResult'=>$MetaResult));
			}
			/*  Meta data end */
			
			/* Link data start */
			$LinkData = (!empty($htmlhead['link']))?$htmlhead['link']:NULL;
			if(!empty($LinkData))
			{
				$LinkResult = $this->render_html_link(array('LinkData'=>$LinkData,'LinkResult'=>$LinkResult,'LinkHref'=>$urlprogramdir));
			}
			/* Link data end */
			
			/* Script data start */
			$ScriptData = (!empty($htmlhead['script']))?$htmlhead['script']:NULL;
			if(!empty($ScriptData))
			{
				$ScriptResult = $this->render_html_script(array('ScriptData'=>$ScriptData,'ScriptResult'=>$ScriptResult,'ScriptSrc'=>$urlprogramdir));
			}
			/* Script data end */
			
			/* No Script data start */
			$NoScriptData = (!empty($htmlhead['noscript']))?$htmlhead['noscript']:NULL;
			if(!empty($NoScriptData))
			{
				$NoScriptResult = $this->render_html_noscript(array('NoScriptData'=>$NoScriptData,'NoScriptResult'=>$NoScriptResult,'NoScriptSrc'=>$urlprogramdir));
			}
			/* No Script data end */
			
			/* Style data start */
			$StyleData = (!empty($htmlhead['style']))?$htmlhead['style']:NULL;
			if(!empty($StyleData))
			{
				$StyleResult = $this->render_html_style(array('StyleData'=>$StyleData,'StyleResult'=>$StyleResult,'StyleSrc'=>$urlprogramdir));
			}
			/* Style data end */
		}
		##########################################################################################################################################
		#################################################### Render Title,Meta,Link,etc  ########################################################
		##########################################################################################################################################
		$head  = '';
		if(!empty($MetaResult))
		{
			$head .= implode('',$MetaResult);
		}
		if(!empty($LinkResult))
		{
			$head .= implode('',$LinkResult);
		}
		if(!empty($ScriptResult))
		{
			$head .= implode('',$ScriptResult);
		}
		if(!empty($NoScriptResult))
		{
			$head .= implode('',$NoScriptResult);
		}
		if(!empty($StyleResult))
		{
			$head .= implode('',$StyleResult);
		}
		if(!empty($TitleResult))
		{
			$head .= implode('',$TitleResult);
		}
		return $head;
	}
	
	private function render_html_title($data=NULL)
	{
		$TitleResult = $data['TitleResult'];
		if(!empty($data['TitleData']))
		{
			foreach($data['TitleData'] as $key0=>$val0)
			{
				if(!empty($val0) and !in_array(true,$TitleResult))
				{
					if(is_array($val0))
					{
						$val0 = array_filter($val0);
						$delimiter = ((!empty($data['TitleDelimiter']))?$data['TitleDelimiter']:((!empty($val0['delimiter']))?$val0['delimiter']:DEFAULTTITLEDELIMITER));
						$html  = '<title>';
						$html .= implode((($delimiter!='')?''.$delimiter.'':' '),$val0);
						$html .= '</title>';
					}
					else if(!is_array($val0))
					{
						$html  = '<title>';
						$html .= $val0;
						$html .= '</title>';
					}
					$TitleResult[true] = $html;
				}
			}
		}
		return $TitleResult;
	}
	
	private function render_html_meta($data=NULL)
	{
		$MetaResult = $data['MetaResult'];
		if(!empty($data['MetaData']))
		{
			foreach($data['MetaData'] as $key0=>$val0)
			{
				if(!empty($val0))
				{
					if(is_array($val0))
					{
						ksort($val0);
						$hash = hash('md5',((array_key_exists('name',$val0) and empty(array_intersect(array('og','gr'),explode(':',$val0['name']))))?$val0['name']:implode('',$val0)));
						if(!array_key_exists($hash, $MetaResult))
						{
							$html  = '<meta';
							foreach($val0 as $key1=>$val1)
							{
								$html .= ' '.$key1.'="'.$val1.'"';
							}
							$html .= '/>';
							$MetaResult[$hash] = $html;
						}
					}
					else
					{
						$hash = hash('md5',$val0);
						if(!array_key_exists($hash, $MetaResult))
						{
							$html  = '<meta';
							$html .= ' '.$val0;
							$html .= '/>';
							$MetaResult[$hash] = $html;
						}
					}
				}
			}
		}
		return $MetaResult;
	}
	
	private function render_html_link($data=NULL)
	{
		$LinkResult = $data['LinkResult'];
		if(!empty($data['LinkData']))
		{
			foreach($data['LinkData'] as $key0=>$val0)
			{
				if(!empty($val0))
				{
					if(is_array($val0))
					{
						krsort($val0);
						
						$hash = hash('md5',implode('',$val0).((strpos($val0['href'],'http://')===false)?$data['LinkHref']:''));
						if(!array_key_exists($hash,$LinkResult))
						{
							$val0['href'] = str_replace((($GLOBALS['_URLDecode']['URLProtocol']==='https')?'http://':''),(($GLOBALS['_URLDecode']['URLProtocol']==='https')?'https://':''),(((strpos($val0['href'],'http://')===false)?$data['LinkHref'].'/':'').$val0['href']));
							$html  = '<link';
							foreach($val0 as $key1=>$val1)
							{
								$html .= ' '.$key1.'="'.$val1.'"';
							}
							$html .= '>';
							$LinkResult[$hash] = $html;
						}
					}
					else
					{
						$hash = hash('md5',$val0);
						if(!array_key_exists($hash,$LinkResult))
						{
							$html  = '<link';
							$html .= ' '.$val0;
							$html .= '>';
							$LinkResult[$hash] = $html;
						}
					}
				}
			}
		}
		return $LinkResult;
	}
	
	private function render_html_script($data=NULL)
	{
		$ScriptResult = $data['ScriptResult'];
		if(!empty($data['ScriptData']))
		{
			foreach($data['ScriptData'] as $key0=>$val0)
			{
				if(!empty($val0))
				{
					if(is_array($val0))
					{
						krsort($val0);
						$hash = hash('md5',implode('',$val0).((strpos($val0['src'],'http://')===false)?$data['ScriptSrc']:''));
						if(!array_key_exists($hash,$ScriptResult))
						{
							$val0['src'] = str_replace((($GLOBALS['_URLDecode']['URLProtocol']==='https')?'http://':''),(($GLOBALS['_URLDecode']['URLProtocol']==='https')?'https://':''),((strpos($val0['src'],'http://')===false)?$data['ScriptSrc'].'/':'').$val0['src']);
							$html  = '<script';
							foreach($val0 as $key1=>$val1)
							{
								$html .= (!is_numeric($key1))?' '.$key1.'="'.$val1.'"':' '.$val1.'';
							}
							$html .= '></script>';
							$ScriptResult[$hash] = $html;
						}
					}
					else 
					{
						$hash = hash('md5',$val0);
						if(!array_key_exists($hash,$ScriptResult))
						{
							$html  = '<script';
							if(!is_numeric($key0))
							{
								$key0 = json_decode($key0,true);
								if(!empty($key0))
								{
									foreach($JSONKey0 as $key=>$val)
									{
										$html .= ' '.$key.'="'.$val.'"';
									}
								}
							}
							else 
							{
								$html .= ' type="text/javascript"';
							}
							$html .= '>';
							$html .= $val0;
							$html .= '</script>';
							$ScriptResult[$hash] = $html;
						}
					}
				}
			}
		}
		return $ScriptResult;
	}
	
	private function render_html_noscript($data=NULL)
	{
		$NoScriptResult = $data['NoScriptResult'];
		if(!empty($data['NoScriptData']))
		{
			foreach($data['NoScriptData'] as $key0=>$val0)
			{
				if(!empty($val0))
				{
					if(is_array($val0))
					{
						krsort($val0);
						$hash = hash('md5',implode('',$val0).((strpos($val0['src'],'http://')===false)?$data['NoScriptSrc']:''));
						if(!array_key_exists($hash,$NoScriptResult))
						{
							$val0['src'] = str_replace((($GLOBALS['_URLDecode']['URLProtocol']==='https')?'http://':''),(($GLOBALS['_URLDecode']['URLProtocol']==='https')?'https://':''),((strpos($val0['src'],'http://')===false)?$data['NoScriptSrc'].'/':'').$val0['src']);
							$html  = '<noscript';
							foreach($val0 as $key1=>$val1)
							{
								$html .= ' '.$key1.'="'.$val1.'"';
							}
							$html .= '></noscript>';
							$NoScriptResult[$hash] = $html;
						}
					}
					else 
					{
						$hash = hash('md5',$val0);
						if(!array_key_exists($hash,$NoScriptResult))
						{
							$html  = '<noscript>';
							$html .= $val0;
							$html .= '</noscript>';
							$NoScriptResult[$hash] = $html;
						}
					}
				}
			}
		}
		return $NoScriptResult;
	}
	
	private function render_html_style($data=NULL)
	{
		$StyleResult = $data['StyleResult'];
		if(!empty($data['StyleData']))
		{
			foreach($data['StyleData'] as $key0=>$val0)
			{	
				$html  = '<style';
				if(!is_numeric($key0))
				{
					$key0 = json_decode($key0,true);
					if(!empty($key0))
					{
						foreach($JSONKey0 as $key=>$val)
						{
							$html .= ' '.$key.'="'.$val.'"';
						}
					}
				}
				else 
				{
					$html .= ' type="text/css"';
				}
				$html .= '>';
				$html .= $val0;
				$html .= '</style>';
				$StyleResult[] = $html;
			}
		}
		return $StyleResult;
	}
	
	private function template_path($data=NULL)
	{
		$_TemplateData = (!empty($GLOBALS['_TemplateData']))?$GLOBALS['_TemplateData'][0]:array();
		$_CurProgramData = (!empty($GLOBALS['_CurProgramData']))?$GLOBALS['_CurProgramData']:array();
		$_NavData  = (!empty($GLOBALS['_NavData'][0]))?$GLOBALS['_NavData'][0]:array();
		$templatedir = '';
		$templatefile = '';
		
		$tmpl_templaterootdir = (!empty($_TemplateData))?SYSTEM_TEMPLATES_DIR.'/'.$_TemplateData['iprgm_name'].'':'';
		
		$tmpl_templatedir = (!empty($_CurProgramData) and file_exists($tmpl_templaterootdir.'/template/'.$_TemplateData['uprgm_name'].'/program/'.$_CurProgramData['iprgm_type'].'s'))?$tmpl_templaterootdir.'/template/'.$_TemplateData['uprgm_name'].'/program/'.$_CurProgramData['iprgm_type'].'s':((!empty($_CurProgramData) and file_exists($tmpl_templaterootdir.'/template/program/'.$_CurProgramData['iprgm_type'].'s'))?$tmpl_templaterootdir.'/template/program/'.$_CurProgramData['iprgm_type'].'s':'');
		$tmpl_templatefile = (file_exists($tmpl_templatedir.'/index.php'))?$tmpl_templatedir.'/index.php':'';
		
		$tmpl_programdir = (!empty($_CurProgramData) and file_exists($tmpl_templatedir.'/'.$_CurProgramData['iprgm_name']))?$tmpl_templatedir.'/'.$_CurProgramData['iprgm_name'].'':'';
		$tmpl_programfile = (!empty($_NavData) and file_exists($tmpl_programdir.'/'.$_CurProgramData['uprgm_name'].'/'.$_NavData['nav_name'].'.php'))?$tmpl_programdir.'/'.$_CurProgramData['uprgm_name'].'/'.$_NavData['nav_name'].'.php':((file_exists($tmpl_programdir.'/'.$_CurProgramData['uprgm_name'].'.php'))?$tmpl_programdir.'/'.$_CurProgramData['uprgm_name'].'.php':((file_exists($tmpl_programdir.'/index.php'))?$tmpl_programdir.'/index.php':''));
		
		$tmpl_positiondir = (!empty($_CurProgramData) and file_exists($tmpl_templaterootdir.'/template/'.$_TemplateData['uprgm_name'].'/position/'))?$tmpl_templaterootdir.'/template/'.$_TemplateData['uprgm_name'].'/position/':((!empty($_CurProgramData) and file_exists($tmpl_templaterootdir.'/template/position/'))?$tmpl_templaterootdir.'/template/position/':'');
		$tmpl_positionfile = (file_exists($tmpl_positiondir.'/'.$_CurProgramData['uprgm_position'].'.php'))?$tmpl_positiondir.'/'.$_CurProgramData['uprgm_position'].'.php':'';
		
		$tmpl_defaultdir = (!empty($_CurProgramData) and file_exists(SYSTEM_PROGRAM_DIR.'/'.$_CurProgramData['iprgm_type'].'s/'.$_CurProgramData['iprgm_name'].'/template'))?SYSTEM_PROGRAM_DIR.'/'.$_CurProgramData['iprgm_type'].'s/'.$_CurProgramData['iprgm_name'].'/template':'';
		$tmpl_defaultfile = (file_exists($tmpl_defaultdir.'/'.$_CurProgramData['uprgm_name'].'.php'))?$tmpl_defaultdir.'/'.$_CurProgramData['uprgm_name'].'.php':((file_exists($tmpl_defaultdir.'/'.$_CurProgramData['iprgm_name'].'.php'))?$tmpl_defaultdir.'/'.$_CurProgramData['iprgm_name'].'.php':((file_exists($tmpl_defaultdir.'/index.php'))?$tmpl_defaultdir.'/index.php':''));
		
		if(($_CurProgramData['uprgm_template']=='auto' or $_CurProgramData['uprgm_template']=='custom') and $tmpl_programfile!='')
		{
			$templatedir = $tmpl_programdir;
			$templatefile = $tmpl_programfile;
		}
		else if(($_CurProgramData['uprgm_template']=='auto' or $_CurProgramData['uprgm_template']=='position') and $tmpl_positionfile!='')
		{
			$templatedir = $tmpl_positiondir;
			$templatefile = $tmpl_positionfile;
		}
		else if(($_CurProgramData['uprgm_template']=='auto' or $_CurProgramData['uprgm_template']=='template') and $tmpl_templatefile!='')
		{
			$templatedir = $tmpl_templatedir;
			$templatefile = $tmpl_templatefile;
		}
		else if(($_CurProgramData['uprgm_template']=='auto' or $_CurProgramData['uprgm_template']=='default') and $tmpl_defaultfile!='')
		{
			$templatedir = $tmpl_defaultdir;
			$templatefile = $tmpl_defaultfile;
		}
		return array('templatefile'=>$templatefile,'templatedir'=>$templatedir);
	}
	
	public function addhtmlhead($data=NULL)
	{
		$html_head = (isset($data['html_head']))?$data['html_head']:$data;
		$this->staging_html_head = array_merge_recursive($this->staging_html_head,$html_head);
	}
}
?>