<?php
/* بسم الله الرحمن الرحيم
 * File name		: system.class.php
 * Author			: Tri Hartanto
 * Site				: trihartanto.com
 * Framework		: thtech
 * Engine type		: Engine System
 * Version			: 2
 * License			: GPL
 * Create Date		: 23 February 2011
 * Modified Date	: 04 Apr 2014
 * File Description	: This file contains system class to be used by the framework and CMS system.
 *
 * For more license information please kindly open and read LICENSE.txt file
 */
class Systems
{	
	public function __call($method,$arguments)
	{
		if(method_exists($GLOBALS['_SysEngine'],$method))
		{
			return call_user_func_array(array($GLOBALS['_SysEngine'],$method),$arguments);
		}
		return false;
	}
	
	public function __get($var=NULL)
	{
		if(array_key_exists($var,$GLOBALS))
		{
			return $GLOBALS[$var];
		}
		else if(array_key_exists($var,$GLOBALS['_SysEngine']->dynamic_loaded_class))
		{
			return $GLOBALS['_SysEngine']->dynamic_loaded_class[$var];
		}
		else if(array_key_exists($var,$GLOBALS['_SysEngine']->static_loaded_class))
		{
			return $GLOBALS['_SysEngine']->static_loaded_class[$var];
		}
		return false;
	}
}
?>