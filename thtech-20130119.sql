-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-01-19 19:24:38
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for thtech
CREATE DATABASE IF NOT EXISTS `thtech` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `thtech`;


-- Dumping structure for table thtech.sys_installed_program
CREATE TABLE IF NOT EXISTS `sys_installed_program` (
  `iprgm_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_name` varchar(200) DEFAULT NULL,
  `iprgm_type` varchar(200) DEFAULT NULL,
  `iprgm_order` int(100) DEFAULT NULL,
  `iprgm_nav_table` varchar(250) DEFAULT NULL,
  `iprgm_custom_head` longtext,
  `iprgm_template_attribute` text COMMENT 'off;template;default;auto',
  `iprgm_params` longtext,
  `iprgm_updatedate` int(20) DEFAULT NULL,
  `iprgm_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`iprgm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_installed_program: 1 rows
/*!40000 ALTER TABLE `sys_installed_program` DISABLE KEYS */;
INSERT INTO `sys_installed_program` (`iprgm_id`, `iprgm_name`, `iprgm_type`, `iprgm_order`, `iprgm_nav_table`, `iprgm_custom_head`, `iprgm_template_attribute`, `iprgm_params`, `iprgm_updatedate`, `iprgm_adddate`) VALUES
	(1, 'app_cnt', 'app', 0, '', '', '', '', 0, 0);
/*!40000 ALTER TABLE `sys_installed_program` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_installed_program_type
CREATE TABLE IF NOT EXISTS `sys_installed_program_type` (
  `iprgm_type_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_loader_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `iprgm_type_date` int(20) DEFAULT NULL,
  PRIMARY KEY (`iprgm_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_installed_program_type: 0 rows
/*!40000 ALTER TABLE `sys_installed_program_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_installed_program_type` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_program_view
CREATE TABLE IF NOT EXISTS `sys_program_view` (
  `uprgm_id` int(255) NOT NULL DEFAULT '0',
  `iprgm_id` int(255) DEFAULT '0',
  `type_id` int(255) DEFAULT '0',
  `tmpl_id` int(255) DEFAULT NULL,
  `uprgm_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_position` varchar(50) CHARACTER SET utf8 NOT NULL,
  `uprgm_location` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_iprgm_location` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_nav_table` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_params` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_custom_head` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_status` tinyint(1) DEFAULT NULL,
  `uprgm_default` tinyint(1) DEFAULT NULL,
  `uprgm_order` int(100) DEFAULT NULL,
  `uprgm_template` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT 'off;template;default;auto',
  `uprgm_template_attribute` text CHARACTER SET utf8 NOT NULL,
  `uprgm_require_login` varchar(22) CHARACTER SET utf8 NOT NULL,
  `iprgm_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_params` longtext CHARACTER SET utf8,
  `iprgm_type` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_order` int(100) DEFAULT NULL,
  `iprgm_nav_table` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_custom_head` longtext CHARACTER SET utf8,
  `iprgm_template_attribute` text CHARACTER SET utf8 COMMENT 'off;template;default;auto',
  `loader_name` varchar(255) DEFAULT NULL,
  `loader_index` varchar(255) DEFAULT NULL,
  `uprgm_updatedate` int(20) NOT NULL,
  `uprgm_adddate` int(20) NOT NULL,
  `iprgm_updatedate` int(20) DEFAULT NULL,
  `iprgm_adddate` int(20) DEFAULT NULL,
  `type_adddate` int(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table thtech.sys_program_view: 0 rows
/*!40000 ALTER TABLE `sys_program_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_program_view` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_template
CREATE TABLE IF NOT EXISTS `sys_template` (
  `tmpl_id` int(255) NOT NULL AUTO_INCREMENT,
  `type_id` int(255) NOT NULL DEFAULT '0',
  `tmpl_name` varchar(250) DEFAULT NULL,
  `tmpl_title` varchar(250) DEFAULT NULL,
  `tmpl_attribute` longtext,
  `tmpl_params` longtext,
  `tmpl_custom_head` longtext,
  `tmpl_status` int(1) DEFAULT NULL,
  `tmpl_default` int(1) DEFAULT NULL,
  `tmpl_date_insert` int(20) DEFAULT NULL,
  PRIMARY KEY (`tmpl_id`),
  KEY `type_id` (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_template: 1 rows
/*!40000 ALTER TABLE `sys_template` DISABLE KEYS */;
INSERT INTO `sys_template` (`tmpl_id`, `type_id`, `tmpl_name`, `tmpl_title`, `tmpl_attribute`, `tmpl_params`, `tmpl_custom_head`, `tmpl_status`, `tmpl_default`, `tmpl_date_insert`) VALUES
	(1, 1, 'clickntrades', '', NULL, NULL, NULL, 1, 1, 0);
/*!40000 ALTER TABLE `sys_template` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_type
CREATE TABLE IF NOT EXISTS `sys_type` (
  `type_id` int(255) NOT NULL AUTO_INCREMENT,
  `loader_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `loader_index` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `type_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_type: 1 rows
/*!40000 ALTER TABLE `sys_type` DISABLE KEYS */;
INSERT INTO `sys_type` (`type_id`, `loader_name`, `loader_index`, `type_adddate`) VALUES
	(1, 'frontend', 'index.php', NULL);
/*!40000 ALTER TABLE `sys_type` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_used_program
CREATE TABLE IF NOT EXISTS `sys_used_program` (
  `uprgm_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_id` int(255) DEFAULT NULL,
  `type_id` int(255) DEFAULT NULL,
  `tmpl_id` int(255) DEFAULT NULL,
  `uprgm_name` varchar(250) DEFAULT NULL,
  `uprgm_title` varchar(250) DEFAULT NULL,
  `uprgm_position` varchar(50) DEFAULT NULL,
  `uprgm_location` longtext,
  `uprgm_iprgm_location` longtext,
  `uprgm_nav_table` varchar(250) DEFAULT NULL,
  `uprgm_params` longtext,
  `uprgm_custom_head` longtext,
  `uprgm_status` tinyint(1) DEFAULT NULL,
  `uprgm_default` tinyint(1) DEFAULT NULL,
  `uprgm_order` int(100) DEFAULT NULL,
  `uprgm_template` varchar(10) DEFAULT NULL COMMENT 'off;template;default;auto',
  `uprgm_template_attribute` text,
  `uprgm_mvc_class` varchar(250) DEFAULT NULL,
  `uprgm_mvc_method` varchar(250) DEFAULT NULL,
  `uprgm_require_login` varchar(22) DEFAULT NULL,
  `uprgm_updatedate` int(20) DEFAULT NULL,
  `uprgm_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`uprgm_id`),
  KEY `iprgm_id` (`iprgm_id`),
  KEY `type_id` (`type_id`),
  KEY `tmpl_id` (`tmpl_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_used_program: 2 rows
/*!40000 ALTER TABLE `sys_used_program` DISABLE KEYS */;
INSERT INTO `sys_used_program` (`uprgm_id`, `iprgm_id`, `type_id`, `tmpl_id`, `uprgm_name`, `uprgm_title`, `uprgm_position`, `uprgm_location`, `uprgm_iprgm_location`, `uprgm_nav_table`, `uprgm_params`, `uprgm_custom_head`, `uprgm_status`, `uprgm_default`, `uprgm_order`, `uprgm_template`, `uprgm_template_attribute`, `uprgm_mvc_class`, `uprgm_mvc_method`, `uprgm_require_login`, `uprgm_updatedate`, `uprgm_adddate`) VALUES
	(1, 1, 1, NULL, 'app_cnt', 'Click N Trades', 'app', '', '', '', '', '', 1, 1, 0, '', '', 'clickntrades', '', '["logged","guest"]', 0, 0),
	(2, 1, 1, NULL, 'member_administrator', 'Click N Trades', 'app', '', '', '', '', '', 1, 0, 0, 'template', '', '', '', '["logged","guest"]', 0, 0);
/*!40000 ALTER TABLE `sys_used_program` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
