-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-03-09 12:42:42
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for thtech
CREATE DATABASE IF NOT EXISTS `thtech` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `thtech`;


-- Dumping structure for table thtech.sys_installed_program
CREATE TABLE IF NOT EXISTS `sys_installed_program` (
  `iprgm_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_name` varchar(200) DEFAULT NULL,
  `iprgm_type` varchar(200) DEFAULT NULL,
  `iprgm_order` int(100) DEFAULT NULL,
  `iprgm_nav_table` varchar(250) DEFAULT NULL,
  `iprgm_html_head` longtext,
  `iprgm_template_attribute` text COMMENT 'off;template;default;auto',
  `iprgm_params` longtext,
  `iprgm_updatedate` int(20) DEFAULT NULL,
  `iprgm_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`iprgm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table thtech.sys_installed_program_type
CREATE TABLE IF NOT EXISTS `sys_installed_program_type` (
  `iprgm_type_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_loader_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `iprgm_type_date` int(20) DEFAULT NULL,
  PRIMARY KEY (`iprgm_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table thtech.sys_program_view
CREATE TABLE IF NOT EXISTS `sys_program_view` (
  `uprgm_id` int(255) NOT NULL DEFAULT '0',
  `iprgm_id` int(255) DEFAULT '0',
  `type_id` int(255) DEFAULT '0',
  `tmpl_id` int(255) DEFAULT NULL,
  `uprgm_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_position` varchar(50) CHARACTER SET utf8 NOT NULL,
  `uprgm_location` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_iprgm_location` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_nav_table` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_params` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_custom_head` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_status` tinyint(1) DEFAULT NULL,
  `uprgm_default` tinyint(1) DEFAULT NULL,
  `uprgm_order` int(100) DEFAULT NULL,
  `uprgm_template` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT 'off;template;default;auto',
  `uprgm_template_attribute` text CHARACTER SET utf8 NOT NULL,
  `uprgm_require_login` varchar(22) CHARACTER SET utf8 NOT NULL,
  `iprgm_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_params` longtext CHARACTER SET utf8,
  `iprgm_type` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_order` int(100) DEFAULT NULL,
  `iprgm_nav_table` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_custom_head` longtext CHARACTER SET utf8,
  `iprgm_template_attribute` text CHARACTER SET utf8 COMMENT 'off;template;default;auto',
  `loader_name` varchar(255) DEFAULT NULL,
  `loader_index` varchar(255) DEFAULT NULL,
  `uprgm_updatedate` int(20) NOT NULL,
  `uprgm_adddate` int(20) NOT NULL,
  `iprgm_updatedate` int(20) DEFAULT NULL,
  `iprgm_adddate` int(20) DEFAULT NULL,
  `type_adddate` int(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table thtech.sys_template
CREATE TABLE IF NOT EXISTS `sys_template` (
  `tmpl_id` int(255) NOT NULL AUTO_INCREMENT,
  `type_id` int(255) NOT NULL DEFAULT '0',
  `tmpl_name` varchar(250) DEFAULT NULL,
  `tmpl_title` varchar(250) DEFAULT NULL,
  `tmpl_attribute` longtext,
  `tmpl_params` longtext,
  `tmpl_html_head` longtext,
  `tmpl_status` int(1) DEFAULT NULL,
  `tmpl_default` int(1) DEFAULT NULL,
  `tmpl_date_insert` int(20) DEFAULT NULL,
  PRIMARY KEY (`tmpl_id`),
  KEY `type_id` (`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table thtech.sys_type
CREATE TABLE IF NOT EXISTS `sys_type` (
  `type_id` int(255) NOT NULL AUTO_INCREMENT,
  `loader_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `loader_index` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `type_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table thtech.sys_used_program
CREATE TABLE IF NOT EXISTS `sys_used_program` (
  `uprgm_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_id` int(255) DEFAULT NULL,
  `type_id` int(255) DEFAULT NULL,
  `tmpl_id` int(255) DEFAULT NULL,
  `uprgm_name` varchar(250) DEFAULT NULL,
  `uprgm_title` varchar(250) DEFAULT NULL,
  `uprgm_position` varchar(50) DEFAULT NULL,
  `uprgm_location` longtext,
  `uprgm_iprgm_location` longtext,
  `uprgm_nav_table` varchar(250) DEFAULT NULL,
  `uprgm_params` longtext,
  `uprgm_html_head` longtext,
  `uprgm_status` tinyint(1) DEFAULT NULL,
  `uprgm_default` tinyint(1) DEFAULT NULL,
  `uprgm_order` int(100) DEFAULT NULL,
  `uprgm_template` varchar(10) DEFAULT NULL COMMENT 'off;template;default;auto',
  `uprgm_template_attribute` text,
  `uprgm_mvc_class` varchar(250) DEFAULT NULL,
  `uprgm_mvc_method` varchar(250) DEFAULT NULL,
  `uprgm_require_login` varchar(22) DEFAULT NULL,
  `uprgm_updatedate` int(20) DEFAULT NULL,
  `uprgm_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`uprgm_id`),
  KEY `iprgm_id` (`iprgm_id`),
  KEY `type_id` (`type_id`),
  KEY `tmpl_id` (`tmpl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
