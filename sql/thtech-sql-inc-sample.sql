-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-06-23 22:51:36
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for thtech
CREATE DATABASE IF NOT EXISTS `thtech` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `thtech`;


-- Dumping structure for table thtech.sys_installed_program
CREATE TABLE IF NOT EXISTS `sys_installed_program` (
  `iprgm_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_name` varchar(200) DEFAULT NULL,
  `iprgm_type` varchar(200) DEFAULT NULL,
  `iprgm_order` int(100) DEFAULT NULL,
  `iprgm_nav_table` varchar(250) DEFAULT NULL,
  `iprgm_html_head` longtext,
  `iprgm_template_attribute` text COMMENT 'off;template;default;auto',
  `iprgm_params` longtext,
  `iprgm_configuration` longtext,
  `iprgm_updatedate` int(20) DEFAULT NULL,
  `iprgm_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`iprgm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_installed_program: 3 rows
/*!40000 ALTER TABLE `sys_installed_program` DISABLE KEYS */;
INSERT INTO `sys_installed_program` (`iprgm_id`, `iprgm_name`, `iprgm_type`, `iprgm_order`, `iprgm_nav_table`, `iprgm_html_head`, `iprgm_template_attribute`, `iprgm_params`, `iprgm_configuration`, `iprgm_updatedate`, `iprgm_adddate`) VALUES
	(1, 'app_cnt', 'app', 0, 'acnt_navigation', '', '', '', '{"DBConfig":{"DBUser":"root","DBPassword":"","DBHost":"localhost","DBName":"acnt","DBType":"mysql"},"autoload":[{"file":"model\\/member_process.php","class":null,"alias":"CNTMemberProcess","sourcedir":null},{"file":"model\\/country_province_city_process.php","class":null,"alias":"CNTCountryProvinceCityProcess","sourcedir":null},{"file":"model\\/common_process.php","class":null,"alias":"CNTCommonProcess","sourcedir":null}],"email":{"emailUsername":"cs@clickntrades.com","emailPassword":"cntcnt123","emailHost":"ssl:\\/\\/smtp.gmail.com","emailSentFrom":{"address":"cs@clickntrades.com","title":"Click N Trades"},"emailReplyTo":{"address":"cs@clickntrades.com","title":"Click N Trades"},"emailSentTo":{"address":"cs@clickntrades.com","title":"Click N Trades"},"emailBCC":{"address":"cs@clickntrades.com","title":"Click N Trades"},"emailSubject":"Click and Trades","emailBody":"Click and Trades Daemon NULL email"}}', 0, 0),
	(2, 'mod_navigation', 'module', 0, NULL, NULL, NULL, NULL, '{"autoload":[{"file":"model\\/navigation_process.php","class":null,"alias":"ModCNTNavProcess","sourcedir":null}]} ', NULL, NULL),
	(3, 'tpl_clickntrades', 'template', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `sys_installed_program` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_installed_program_type
CREATE TABLE IF NOT EXISTS `sys_installed_program_type` (
  `iprgm_type_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_type_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `iprgm_type_date` int(20) DEFAULT NULL,
  PRIMARY KEY (`iprgm_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_installed_program_type: 0 rows
/*!40000 ALTER TABLE `sys_installed_program_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_installed_program_type` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_program_view
CREATE TABLE IF NOT EXISTS `sys_program_view` (
  `uprgm_id` int(255) NOT NULL DEFAULT '0',
  `iprgm_id` int(255) DEFAULT '0',
  `loader_id` int(255) DEFAULT '0',
  `tmpl_id` int(255) DEFAULT NULL,
  `uprgm_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_position` varchar(50) CHARACTER SET utf8 NOT NULL,
  `uprgm_location` longtext CHARACTER SET utf8 NOT NULL,
  `iprgm_location` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_nav_table` varchar(250) CHARACTER SET utf8 NOT NULL,
  `uprgm_params` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_custom_head` longtext CHARACTER SET utf8 NOT NULL,
  `uprgm_status` tinyint(1) DEFAULT NULL,
  `uprgm_default` tinyint(1) DEFAULT NULL,
  `uprgm_order` int(100) DEFAULT NULL,
  `uprgm_template` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT 'off;template;default;auto',
  `uprgm_template_attribute` text CHARACTER SET utf8 NOT NULL,
  `uprgm_require_login` varchar(22) CHARACTER SET utf8 NOT NULL,
  `iprgm_name` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_params` longtext CHARACTER SET utf8,
  `iprgm_type` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_order` int(100) DEFAULT NULL,
  `iprgm_nav_table` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `iprgm_custom_head` longtext CHARACTER SET utf8,
  `iprgm_template_attribute` text CHARACTER SET utf8 COMMENT 'off;template;default;auto',
  `loader_name` varchar(255) DEFAULT NULL,
  `loader_index` varchar(255) DEFAULT NULL,
  `uprgm_updatedate` int(20) NOT NULL,
  `uprgm_adddate` int(20) NOT NULL,
  `iprgm_updatedate` int(20) DEFAULT NULL,
  `iprgm_adddate` int(20) DEFAULT NULL,
  `loader_adddate` int(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table thtech.sys_program_view: 0 rows
/*!40000 ALTER TABLE `sys_program_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_program_view` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_template
CREATE TABLE IF NOT EXISTS `sys_template` (
  `tmpl_id` int(255) NOT NULL AUTO_INCREMENT,
  `loader_id` int(255) NOT NULL DEFAULT '0',
  `tmpl_name` varchar(250) DEFAULT NULL,
  `tmpl_title` varchar(250) DEFAULT NULL,
  `tmpl_attribute` longtext,
  `tmpl_params` longtext,
  `tmpl_html_head` longtext,
  `tmpl_status` int(1) DEFAULT NULL,
  `tmpl_configuration` longtext,
  `tmpl_default` int(1) DEFAULT NULL,
  `tmpl_date_insert` int(20) DEFAULT NULL,
  PRIMARY KEY (`tmpl_id`),
  KEY `loader_id` (`loader_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_template: 1 rows
/*!40000 ALTER TABLE `sys_template` DISABLE KEYS */;
INSERT INTO `sys_template` (`tmpl_id`, `loader_id`, `tmpl_name`, `tmpl_title`, `tmpl_attribute`, `tmpl_params`, `tmpl_html_head`, `tmpl_status`, `tmpl_configuration`, `tmpl_default`, `tmpl_date_insert`) VALUES
	(1, 1, 'clickntrades', '', NULL, NULL, '{"link":[{"href":"style\\/main.css","rel":"stylesheet","type":"text\\/css"}]} ', 1, NULL, 1, 0);
/*!40000 ALTER TABLE `sys_template` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_loader
CREATE TABLE IF NOT EXISTS `sys_type` (
  `loader_id` int(255) NOT NULL AUTO_INCREMENT,
  `loader_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `loader_index` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `loader_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`loader_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_type: 2 rows
/*!40000 ALTER TABLE `sys_type` DISABLE KEYS */;
INSERT INTO `sys_type` (`loader_id`, `loader_name`, `loader_index`, `loader_adddate`) VALUES
	(1, 'frontend', 'index.php', NULL),
	(2, 'backend', 'controlpanel.php', NULL);
/*!40000 ALTER TABLE `sys_type` ENABLE KEYS */;


-- Dumping structure for table thtech.sys_used_program
CREATE TABLE IF NOT EXISTS `sys_used_program` (
  `uprgm_id` int(255) NOT NULL AUTO_INCREMENT,
  `iprgm_id` int(255) DEFAULT NULL,
  `loader_id` int(255) DEFAULT NULL,
  `tmpl_id` int(255) DEFAULT NULL,
  `uprgm_name` varchar(250) DEFAULT NULL,
  `uprgm_title` varchar(250) DEFAULT NULL,
  `uprgm_position` varchar(50) DEFAULT NULL,
  `uprgm_location` longtext,
  `uprgm_iprgm_location` longtext,
  `uprgm_nav_table` varchar(250) DEFAULT NULL,
  `uprgm_params` longtext,
  `uprgm_html_head` longtext,
  `uprgm_status` tinyint(1) DEFAULT NULL,
  `uprgm_default` tinyint(1) DEFAULT NULL,
  `uprgm_order` int(100) DEFAULT NULL,
  `uprgm_template` varchar(10) DEFAULT NULL COMMENT 'off;template;default;auto',
  `uprgm_template_attribute` text,
  `uprgm_mvc_class` varchar(250) DEFAULT NULL,
  `uprgm_mvc_method` varchar(250) DEFAULT NULL,
  `uprgm_require_login` varchar(22) DEFAULT NULL,
  `uprgm_configuration` longtext,
  `uprgm_updatedate` int(20) DEFAULT NULL,
  `uprgm_adddate` int(20) DEFAULT NULL,
  PRIMARY KEY (`uprgm_id`),
  KEY `iprgm_id` (`iprgm_id`),
  KEY `loader_id` (`loader_id`),
  KEY `tmpl_id` (`tmpl_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table thtech.sys_used_program: 3 rows
/*!40000 ALTER TABLE `sys_used_program` DISABLE KEYS */;
INSERT INTO `sys_used_program` (`uprgm_id`, `iprgm_id`, `loader_id`, `tmpl_id`, `uprgm_name`, `uprgm_title`, `uprgm_position`, `uprgm_location`, `uprgm_iprgm_location`, `uprgm_nav_table`, `uprgm_params`, `uprgm_html_head`, `uprgm_status`, `uprgm_default`, `uprgm_order`, `uprgm_template`, `uprgm_template_attribute`, `uprgm_mvc_class`, `uprgm_mvc_method`, `uprgm_require_login`, `uprgm_configuration`, `uprgm_updatedate`, `uprgm_adddate`) VALUES
	(1, 1, 1, NULL, 'app_cnt', 'Click N Trades', 'app', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 'template', '', 'clickntrades', '', '["logged","guest"]', '', 0, 0),
	(2, 1, 1, NULL, 'member_administrator', 'Click N Trades', 'app', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 'template', '', '', '', '["logged","guest"]', NULL, 0, 0),
	(3, 2, 1, NULL, 'top_navigation', 'Top Navigation', 'top', '["1"]', NULL, NULL, NULL, NULL, 1, 0, 0, 'default', NULL, NULL, NULL, '["logged","guest"]', NULL, NULL, NULL);
/*!40000 ALTER TABLE `sys_used_program` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
